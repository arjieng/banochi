﻿using System;
using Xamarin.Forms;

namespace banochixamarin
{
    public class ListViewNoScroll : ListView
    {
		public static readonly BindableProperty ScrollProperty = BindableProperty.Create(nameof(Scroll), typeof(bool), typeof(ListViewNoScroll), false, BindingMode.Default, null);
		public bool Scroll
		{
			get { return (bool)GetValue(ScrollProperty); }
			set { SetValue(ScrollProperty, value); }
		}
    }
}
