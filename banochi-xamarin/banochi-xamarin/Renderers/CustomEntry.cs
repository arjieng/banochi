﻿using System;
using Xamarin.Forms;

namespace banochixamarin
{
    public class CustomEntry : Entry
    {

        public void InvalidateLayout()
        {
            InvalidateMeasure();
        }

        public static readonly BindableProperty ReturnKeyProperty = BindableProperty.Create("ReturnKey", typeof(ReturnKeyButton), typeof(CustomEntry), ReturnKeyButton.Done);

        public ReturnKeyButton ReturnKey
        {
            get { return (ReturnKeyButton)GetValue(ReturnKeyProperty); }
            set { SetValue(ReturnKeyProperty, value); }
        }

        public static readonly BindableProperty AutoCapitalizationProperty = BindableProperty.Create("AutoCapitalization", typeof(TextAutoCapitalization), typeof(CustomEntry), TextAutoCapitalization.None);

        public TextAutoCapitalization AutoCapitalization
        {
            get { return (TextAutoCapitalization)GetValue(AutoCapitalizationProperty); }
            set { SetValue(AutoCapitalizationProperty, value); }
        }

        public static readonly BindableProperty ButtonModeProperty = BindableProperty.Create("ButtonMode", typeof(bool), typeof(CustomEntry), true);

        public bool ButtonMode
        {
            get { return (bool)GetValue(ButtonModeProperty); }
            set { SetValue(ButtonModeProperty, value); }
        }

        /// <summary>
        /// Text alignment values:
        /// 0 - left
        /// 1 - right
        /// 2 - center
        /// 3 - justified
        /// </summary>
        public static readonly BindableProperty TextAlignmentProperty = BindableProperty.Create("TextAlignment", typeof(int), typeof(CustomEntry), 0);

        public int TextAlignment
        {
            get { return (int)GetValue(TextAlignmentProperty); }
            set { SetValue(TextAlignmentProperty, value); }
        }

        public Entry NextEntry { get; set; }

        public static readonly BindableProperty PlaceholderOriginalColorProperty = BindableProperty.Create("PlaceholderOriginalColor", typeof(Color), typeof(CustomEntry), Constants.LABEL_GRAY);

        public Color PlaceholderOriginalColor
        {
            get { return (Color)GetValue(PlaceholderOriginalColorProperty); }
            set { SetValue(PlaceholderOriginalColorProperty, value); }
        }

        public static readonly BindableProperty MoveUpProperty = BindableProperty.Create("MoveUp", typeof(bool), typeof(CustomEntry), true);

        public bool MoveUp
        {
            get { return (bool)GetValue(MoveUpProperty); }
            set { SetValue(MoveUpProperty, value); }
        }
    }

    public enum TextAutoCapitalization
    {
        None,
        Word,
        Sentence
    }

	public enum ReturnKeyButton
	{
		Next,
		Done,
		Return
	}
}
