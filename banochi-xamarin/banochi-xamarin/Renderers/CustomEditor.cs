﻿using System;
using Xamarin.Forms;

namespace banochixamarin
{
	public enum EditorTextAlignment
	{
		Start,
		Center,
		End
	}

	public class CustomEditor : Editor
	{
		public void InvalidateLayout()
		{
			InvalidateMeasure();
		}
		public static readonly BindableProperty ReturnKeyProperty = BindableProperty.Create("ReturnKey", typeof(ReturnKeyButton), typeof(CustomEditor), ReturnKeyButton.Done);

		public ReturnKeyButton ReturnKey
		{
			get { return (ReturnKeyButton)GetValue(ReturnKeyProperty); }
			set { SetValue(ReturnKeyProperty, value); }
		}

		public static readonly BindableProperty AutoCapitalizationProperty = BindableProperty.Create("AutoCapitalization", typeof(TextAutoCapitalization), typeof(CustomEditor), TextAutoCapitalization.None);

		public TextAutoCapitalization AutoCapitalization
		{
			get { return (TextAutoCapitalization)GetValue(AutoCapitalizationProperty); }
			set { SetValue(AutoCapitalizationProperty, value); }
		}

		public static readonly BindableProperty PlaceholderProperty = BindableProperty.Create("PlaceHolder", typeof(string), typeof(CustomEditor), "");

		public string Placeholder
		{
			get { return (string)GetValue(PlaceholderProperty); }
			set { SetValue(PlaceholderProperty, value); }
		}

		public static readonly BindableProperty PlaceholderColorProperty = BindableProperty.Create("PlaceholderColor", typeof(Color), typeof(CustomEditor), Constants.LABEL_GRAY);

		public Color PlaceholderColor
		{
			get { return (Color)GetValue(PlaceholderColorProperty); }
			set { SetValue(PlaceholderColorProperty, value); }
		}

		public static readonly BindableProperty BorderRadiusProperty = BindableProperty.Create("BorderRadius", typeof(int), typeof(CustomEditor), 0);

		public int BorderRadius
		{
			get { return (int)GetValue(BorderRadiusProperty); }
			set { SetValue(BorderRadiusProperty, value); }
		}

		public static readonly BindableProperty ButtonModeProperty = BindableProperty.Create("ButtonMode", typeof(string), typeof(CustomEditor), null);

		public string ButtonMode
		{
			get { return (string)GetValue(ButtonModeProperty); }
			set { SetValue(ButtonModeProperty, value); }
		}

		public static readonly BindableProperty VerticalTextAlignmentProperty = BindableProperty.Create("VerticalTextAlignment", typeof(EditorTextAlignment), typeof(CustomEditor), EditorTextAlignment.Start);

		public EditorTextAlignment VerticalTextAlignment
		{
			get { return (EditorTextAlignment)GetValue(VerticalTextAlignmentProperty); }
			set { SetValue(VerticalTextAlignmentProperty, value); }
		}

		public static readonly BindableProperty HorizontalTextAlignmentProperty = BindableProperty.Create("HorizontalTextAlignment", typeof(EditorTextAlignment), typeof(CustomEditor), EditorTextAlignment.Start);

		public EditorTextAlignment HorizontalTextAlignment
		{
			get { return (EditorTextAlignment)GetValue(HorizontalTextAlignmentProperty); }
			set { SetValue(HorizontalTextAlignmentProperty, value); }
		}

		public static readonly BindableProperty CharacterLimitProperty = BindableProperty.Create("CharacterLimit", typeof(int), typeof(CustomEditor), -1);

		public int CharacterLimit
		{
			get { return (int)GetValue(CharacterLimitProperty); }
			set { SetValue(CharacterLimitProperty, value); }
		}

		public static readonly BindableProperty LabelSpacingProperty = BindableProperty.Create("LabelSpacing", typeof(double), typeof(CustomEditor), 1.0);

		public double LabelSpacing
		{
			get { return (double)GetValue(LabelSpacingProperty); }
			set { SetValue(LabelSpacingProperty, value); }
		}

		public static readonly BindableProperty ViewToSwiftUpProperty = BindableProperty.Create("ViewToSwiftUp", typeof(View), typeof(CustomEditor), null);

		public View ViewToSwiftUp
		{
			get { return (View)GetValue(ViewToSwiftUpProperty); }
			set { SetValue(ViewToSwiftUpProperty, value); }
		}

		public static readonly BindableProperty ScrollProperty = BindableProperty.Create("Scroll", typeof(bool), typeof(CustomEditor), false);

		public bool Scroll
		{
			get { return (bool)GetValue(ScrollProperty); }
			set { SetValue(ScrollProperty, value); }
		}

		public static readonly BindableProperty MoveUpProperty = BindableProperty.Create("MoveUp", typeof(bool), typeof(CustomEditor), true);

		public bool MoveUp
		{
			get { return (bool)GetValue(MoveUpProperty); }
			set { SetValue(MoveUpProperty, value); }
		}

		public static readonly BindableProperty EditorTypeProperty = BindableProperty.Create("EditorType", typeof(int), typeof(CustomEditor), 0);

		public int EditorType
		{
			get { return (int)GetValue(EditorTypeProperty); }
			set { SetValue(EditorTypeProperty, value); }
		}

		public Editor NextEntry { get; set; }

	}
}
