﻿using System;
using Xamarin.Forms;

namespace banochixamarin
{
	public class GestureStackLayout : StackLayout
	{
		public event EventHandler<object> Tapped;
		public void OnTappedEvent(object e)
		{
			if (Tapped != null)  // make sure at least one subscriber
								 // note the event is returning a string
				Tapped.Invoke(this, e);
		}

		public static readonly BindableProperty IsSwipeableProperty = BindableProperty.Create("IsSwipeable", typeof(bool), typeof(StackLayout), true);
		public bool IsSwipeable
		{
			get { return (bool)GetValue(IsSwipeableProperty); }
			set { SetValue(IsSwipeableProperty, value); }
		}

		/* CollectionType
		 * 0 - Decision Options
         * 1 - In-Progress
         * 2 - Saved Results
         */
		public static readonly BindableProperty CollectionTypeProperty = BindableProperty.Create("CollectionType", typeof(int), typeof(StackLayout), 0);
		public int CollectionType
		{
			get { return (int)GetValue(CollectionTypeProperty); }
			set { SetValue(CollectionTypeProperty, value); }
		}

		public GestureStackLayout() { }
	}
}
