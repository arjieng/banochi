﻿using System;
using Xamarin.Forms;

namespace banochixamarin
{
	public class CustomScrollView : ScrollView
	{
		public static readonly BindableProperty ScrollProperty = BindableProperty.Create(nameof(Scroll), typeof(bool), typeof(CustomScrollView), false, BindingMode.Default, null);
		public bool Scroll
		{
			get { return (bool)GetValue(ScrollProperty); }
			set { SetValue(ScrollProperty, value); }
		}
	}
}
