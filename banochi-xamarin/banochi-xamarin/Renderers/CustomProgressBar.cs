﻿using System;
using Xamarin.Forms;

namespace banochixamarin
{
	public class CustomProgressBar : ProgressBar
	{
		public static readonly BindableProperty MaxColorProperty = BindableProperty.Create(nameof(MaxColor), typeof(Color), typeof(CustomProgressBar), Color.Default);

		public Color MaxColor
		{
			get { return (Color)GetValue(MaxColorProperty); }
			set { SetValue(MaxColorProperty, value); }
		}

		public static readonly BindableProperty MinColorProperty = BindableProperty.Create(nameof(MinColor), typeof(Color), typeof(CustomProgressBar), Color.Default);

		public Color MinColor
		{
			get { return (Color)GetValue(MinColorProperty); }
			set { SetValue(MinColorProperty, value); }
		}
	}
}
