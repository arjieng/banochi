﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace banochixamarin
{
	public class MasterPageItem : INotifyPropertyChanged
	{
		public string Title { get; set; }
		public Type TargetType { get; set; }
        public int _IsSelected;
		public int IsSelected { get { return _IsSelected; } set { _IsSelected = value; OnPropertyChanged(); } }

		public event PropertyChangedEventHandler PropertyChanged;
		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = "")
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}
