﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace banochixamarin
{
	public class IsZeroDouble : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (value != null)
			{
				if (value is string)
				{
					double tempValue = double.Parse(value.ToString());
					if (tempValue.Equals(0.0))
					{
						return true;
					}
				}
			}

			return false;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
