﻿using System;
using Xamarin.Forms;

namespace banochixamarin
{
    public class Constants
    {
        public Constants()
        {
        }

        // New Colors
        public static readonly Color BUTTON_BACKGROUND_GREEN = Color.FromHex("#00B050");
        public static readonly Color NAVIGATION_BACKGROUND_BLUE = Color.FromHex("#4472C4");
        //navigation color but with lighter alpha
        public static readonly Color NAVIGATION_BACKGROUND_BLUE_LIGHT = Color.FromHex("#527BCD");

        public static readonly Color BUTTON_TEXTCOLOR_WHITE = Color.FromHex("#FFFFFF");
        public static readonly Color BUTTON_TEXTCOLOR = Color.FromHex("#FFFFFF");

        // navigation
        public static readonly double NAVIGATION_HEIGHT = Device.RuntimePlatform == Device.iOS ? 64.ScaleHeight() : 56.ScaleHeight();
        public static readonly Thickness NAVIGATION_MARGIN = Device.RuntimePlatform == Device.iOS ? new Thickness(0, -64.ScaleHeight(), 0, 0) : new Thickness(0, -56.ScaleHeight(), 0, 0);
        public static readonly Thickness PICKER_MARGIN = Device.RuntimePlatform == Device.iOS ? new Thickness(15.ScaleWidth(), 112.ScaleHeight(), 15.ScaleWidth(), 0) : new Thickness(15.ScaleWidth(), 104.ScaleHeight(), 15.ScaleWidth(), 0);

        //custom switch
        public static readonly Rectangle SWITCH_BACKGROUND = new Rectangle(13.ScaleWidth(), 3.ScaleHeight(), 34.ScaleWidth(), 12.ScaleHeight());
        public static readonly double BACKGROUND_RADIUS = (SWITCH_BACKGROUND.Height / 2.0);
        public static readonly Rectangle SWITCH_BUTTON = new Rectangle(13.ScaleWidth(), 0, 18.ScaleWidth(), 18.ScaleHeight());
        public static readonly double SWITCHBUTTON_RADIUS = (SWITCH_BUTTON.Width / 2.0);
        public static readonly Color SWITCH_OFF_BACKGROUND_COLOR = Color.FromHex("#B1B1B1");
        public static readonly Color SWITCH_OFF_COLOR = Color.FromHex("#bbbbbb");
        public static readonly Rectangle SWITCH_LABEL = new Rectangle(0, 26.ScaleHeight(), 60.ScaleWidth(), 6.ScaleHeight());
        public static readonly Color SWITCH_ON_BACKGROUND_COLOR = Color.FromHex("#4BC684");
        public static readonly Color SWITCH_ON_COLOR = Color.FromHex("#00B050");

        // label
        public static readonly Color LABEL_BLACK = Color.FromHex("#1D1D26");
        public static readonly Color LABEL_GRAY = Color.FromHex("#C4C3C8");
        public static readonly Color LABEL_RED = Color.Red;

        // boxview divider
        public static readonly Color BOXVIEW_DIVIDERCOLOR = Color.FromHex("#C4C3C8");

        // background
        public static readonly Color BACKGROUND_WHITE = Color.FromHex("#FFFFFF");
        public static readonly Color BACKGROUND_GRAY = Color.FromHex("#C4C3C8");

        // listview
        public static readonly Thickness AB_PADDING = new Thickness(7, 0, 7, Device.RuntimePlatform == Device.iOS ? 3 : 6.5);
        public static readonly Color DRAWER_COLOR = Color.FromHex("#50d2c2");
        public static readonly Color DRAWER_DELETE = Color.FromHex("#ff3366");
        public static readonly Rectangle LISTVIEW_CELL_RECT = new Rectangle(0, 0, 1, 63.ScaleHeight());
        public static readonly Color GRAY_BACKGROUNDCOLOR = Color.FromHex("#f8f8f8");

        // bar graph
        public static readonly double BAR_FULL_WIDTH = 200.ScaleWidth();
        public static readonly Color LINE_BLACK = Color.FromHex("#000000");

        // progress label
        public static readonly double PROGRESS_FULL_WIDTH = 290.ScaleWidth();

        // carousel view
        public static readonly Color CURRENT_INDICATOR_COLOR = Color.FromHex("#50D2C2");
        public static readonly Color INDICATOR_TINT_COLOR = Color.FromHex("#FFFFFF");

        // FONTS
        public static readonly string SF_BOLD = Device.RuntimePlatform == Device.iOS ? "SanFranciscoText-Bold" : "Fonts/SanFranciscoText-Bold.otf#SanFranciscoText-Bold";
        public static readonly string SF_HEAVY = Device.RuntimePlatform == Device.iOS ? "SanFranciscoText-Heavy" : "Fonts/SanFranciscoText-Heavy.otf#SanFranciscoText-Heavy";
        public static readonly string SF_LIGHT = Device.RuntimePlatform == Device.iOS ? "SanFranciscoText-Light" : "Fonts/SanFranciscoText-Light.otf#SanFranciscoText-Light";
        public static readonly string SF_MEDIUM = Device.RuntimePlatform == Device.iOS ? "SanFranciscoText-Medium" : "Fonts/SanFranciscoText-Medium.otf#SanFranciscoText-Medium";
        public static readonly string SF_REGULAR = Device.RuntimePlatform == Device.iOS ? "SanFranciscoText-Regular" : "Fonts/SanFranciscoText-Regular.otf#SanFranciscoText-Regular";
        public static readonly string SF_SEMIBOLD = Device.RuntimePlatform == Device.iOS ? "SanFranciscoText-Semibold.otf" : "Fonts/SanFranciscoText-Semibold.otf#SanFranciscoText-Semibold";

        public static string ChangeValueTo(string iOSValue, string androidValue)
        {
            return Device.RuntimePlatform == Device.iOS ? iOSValue : androidValue;
        }
        public static float ChangeValueTo(float iOSValue, float androidValue)
        {
            return Device.RuntimePlatform == Device.iOS ? iOSValue : androidValue;
        }
        public static double ChangeValueTo(double iOSValue, double androidValue)
        {
            return Device.RuntimePlatform == Device.iOS ? iOSValue : androidValue;
        }
        public static int ChangeValueTo(int iOSValue, int androidValue)
        {
            return Device.RuntimePlatform == Device.iOS ? iOSValue : androidValue;
        }
        public static Thickness ChangeValueTo(Thickness iOSValue, Thickness androidValue)
        {
            return Device.RuntimePlatform == Device.iOS ? iOSValue : androidValue;
        }
        public static Rectangle ChangeValueTo(Rectangle iOSValue, Rectangle androidValue)
        {
            return Device.RuntimePlatform == Device.iOS ? iOSValue : androidValue;
        }

        // SimplyRets
        public static readonly string SIMPLYRETS_USERNAME = "simplyrets";
        public static readonly string SIMPLYRETS_PASSWORD = "simplyrets";
        public static readonly string SIMPLYRETS_URL = "https://api.simplyrets.com/properties";

#if DEBUG
        public static readonly string URL = "http://192.168.1.22";
        public static readonly string PORT = "8080";
        public static readonly string ROOT = URL + ":" + PORT;
        public static readonly string ROOT_URL = ROOT + "/v1";

#elif STAGING
        //public static readonly string URL = "";
        //public static readonly string PORT = "4000";
        //public static readonly string ROOT = URL +":"+ PORT;
        //public static readonly string ROOT_URL = ROOT + "/v1";



#elif PRODUCTION
        public static readonly string URL = "";
        public static readonly string PORT = "443";
        public static readonly string ROOT = URL;
        public static readonly string ROOT_URL = ROOT + "/v1";
#endif


#if DEBUG == false

        public static readonly string URL = "http://45.33.6.29";

        //public static readonly string PORT = "3000";
        public static readonly string PORT = "8080";

        //USE THIS FOR LOCAL
        public static readonly string ROOT = URL + ":" + PORT;

        //USE THIS FOR SERVER
        //public static readonly string ROOT = URL;


        public static readonly string ROOT_URL = ROOT + "/v1";

        public static readonly string SIGNIN = ROOT_URL + "/sign-in";
        public static readonly string SOCIAL_SIGNIN = ROOT_URL + "/social-signin";
        public static readonly string REGISTER = ROOT_URL + "/register";
        public static readonly string SOCIAL_REGISTER = ROOT_URL + "/social-signup";
        public static readonly string ADD_DECISION = ROOT_URL + "/add-decisions";
        public static readonly string ADD_OPTION = ROOT_URL + "/add-option";
        public static readonly string UPDATE_DECISION = ROOT_URL + "/update-decision";
        public static readonly string UPDATE_OPTION = ROOT_URL + "/update-option";
        public static readonly string DELETE_DECISION = ROOT_URL + "/delete-decision";
        public static readonly string DELETE_OPTION = ROOT_URL + "/delete-option";
        public static readonly string RELOAD_DECISIONS = ROOT_URL + "/reload-decision";
        public static readonly string FORGOT_PASSWORD = ROOT_URL + "/forgot-password";
        public static readonly string CHANGE_STATUS = ROOT_URL + "/change-status";
        public static readonly string RELOAD_OPTIONS = ROOT_URL + "/reload-option";
        public static readonly string SEND_GRAPH = ROOT_URL + "/send-graph";
        public static readonly string IMAGE_UPLOAD = ROOT_URL + "/image-upload";
#endif
    }
}
