﻿using System;
using System.Linq;
using Xamarin.Forms;

namespace banochixamarin
{
	public class EntryBehavior : Behavior<CustomEntry>
	{
		string entryPlaceholder;

		protected override void OnAttachedTo(CustomEntry bindable)
		{
			base.OnAttachedTo(bindable);

			bindable.Focused += Bindable_Focused;
			bindable.Unfocused += Bindable_Unfocused;
			//bindable.TextChanged += Bindable_TextChanged;
		}

		protected override void OnDetachingFrom(CustomEntry bindable)
		{
			base.OnDetachingFrom(bindable);

			bindable.Focused -= Bindable_Focused;
			bindable.Unfocused -= Bindable_Unfocused;
			//bindable.TextChanged -= Bindable_TextChanged;
		}

		protected void Bindable_Focused(object sender, FocusEventArgs e)
		{
			CustomEntry entryField = (CustomEntry)sender;
            OnEntry_Behavior(entryField, 1, int.Parse(entryField.ClassId));
		}

		void Bindable_Unfocused(object sender, FocusEventArgs e)
		{
			CustomEntry entryField = (CustomEntry)sender;
			entryField.PlaceholderColor = entryField.PlaceholderOriginalColor;

			if (!String.IsNullOrEmpty(entryPlaceholder))
			{
				entryField.Placeholder = entryPlaceholder;
			}

            if (String.IsNullOrEmpty(entryField.Text))
            {
                OnEntry_Behavior(entryField, 0, int.Parse(entryField.ClassId));
            }
		}

        void OnEntry_Behavior(CustomEntry entry, int isFocused, int entryId)
        {
			Label label = new Label();
			var parent = entry.Parent as Grid;

			switch (entryId)
			{
				case 1:
					label = parent.FindByName<Label>("nameLabel");
					break;
				case 2:
					label = parent.FindByName<Label>("emailLabel");
					break;
				case 3:
					label = parent.FindByName<Label>("passwordLabel");
					break;
				case 4:
					label = parent.FindByName<Label>("confirmLabel");
					break;
			}

			label.Opacity = isFocused;

            if(isFocused == 1)
            {
				entryPlaceholder = label.Text;
				entry.Focus();
				entry.Placeholder = String.Empty;
				//entry.FontSize = 12.ScaleFont();
            }
		}

		//void Bindable_TextChanged(object sender, TextChangedEventArgs e)
		//{
		//  CustomEntry entryField = (CustomEntry)sender;
		//  double fontSize = entryField.FontSize;
		//  if (e.NewTextValue.Length == 0)
		//  {
		//      entryField.FontSize = 14.ScaleFont();
		//  }

		//  if (e.NewTextValue.Length >= 26 && e.NewTextValue.Length < 29)
		//  {
		//      if (e.NewTextValue.Length > e.OldTextValue.Length)
		//      {
		//          entryField.FontSize = fontSize - 1;
		//      }
		//      else
		//      {
		//          entryField.FontSize = fontSize + 1;
		//      }
		//  }
		//  else if (e.NewTextValue.Length == 25)
		//  {
		//      if (e.NewTextValue != null && e.OldTextValue != null)
		//      {
		//          if (e.NewTextValue.Length < e.OldTextValue.Length)
		//          {
		//              entryField.FontSize = fontSize + 1;
		//          }
		//      }
		//  }
		//}
	}
}
