﻿using System;
using System.Diagnostics;
using Xamarin.Forms;

namespace banochixamarin
{
	public class EditorBehavior : Behavior<CustomEditor>
	{
		double layoutHeight;
		double newLayoutHeight = -1;
		bool isloaded = false;
		CustomEditor customEditor;
		protected override void OnAttachedTo(CustomEditor bindable)
		{
			base.OnAttachedTo(bindable);
			customEditor = bindable;
			bindable.Focused += Bindable_Focused;
			bindable.Unfocused += Bindable_Unfocused;
			bindable.TextChanged += Bindable_TextChanged;
			bindable.PropertyChanged += Bindable_PropertyChanged;
		}

		protected override void OnDetachingFrom(CustomEditor bindable)
		{
			base.OnDetachingFrom(bindable);

			bindable.Focused -= Bindable_Focused;
			bindable.TextChanged -= Bindable_TextChanged;
			bindable.Unfocused -= Bindable_Unfocused;
		}

		void Bindable_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			if (customEditor.IsFocused == true)
			{
				newLayoutHeight = customEditor.Height;
			}
			else
			{
				layoutHeight = customEditor.MinimumHeightRequest;
			}
		}

		protected void Bindable_Focused(object sender, FocusEventArgs e)
		{
			var editor = ((CustomEditor)sender);
			editor.HeightRequest = -1;
		}

		void Bindable_Unfocused(object sender, FocusEventArgs e)
		{
			var editor = ((CustomEditor)sender);
			Animation anims = new Animation((obj) => editor.HeightRequest = obj, editor.Height, 29.ScaleHeight(), Easing.CubicInOut);
			editor.Animate("editorAnimate", anims, 16, 500, Easing.CubicInOut, null, null);
		}

		void Bindable_TextChanged(object sender, TextChangedEventArgs e)
		{
			if (isloaded == false)
			{
				layoutHeight = ((CustomEditor)sender).Height;
				isloaded = true;
			}
		}
	}
}
