﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;

namespace banochixamarin
{
    public class DataClass : INotifyPropertyChanged
    {
		static DataClass dataClass;
		public static DataClass GetInstance
		{
			get
			{
				if (dataClass == null)
				{
					dataClass = new DataClass();
				}
				return dataClass;
			}
		}

		User _user;
		public User user
		{
			set
			{
				_user = value;
				Application.Current.Properties["user"] = JsonConvert.SerializeObject(_user);
				Application.Current.SavePropertiesAsync();
				OnPropertyChanged(nameof(user));
			}
			get
			{
				if (Application.Current.Properties.ContainsKey("user") && _user == null)
				{
					JObject json = JObject.Parse(Application.Current.Properties["user"].ToString());
					_user = JsonConvert.DeserializeObject<User>(json.ToString());
				}
				return _user;
			}
		}

		string _token;
		public string token
		{
			set
			{
				_token = value;
				if (!string.IsNullOrEmpty(_token))
				{
					Application.Current.Properties["token"] = _token;
					Application.Current.SavePropertiesAsync();
				}
				OnPropertyChanged(nameof(token));
			}
			get
			{
				OnPropertyChanged(nameof(token));
				if (Application.Current.Properties.ContainsKey("token") && string.IsNullOrEmpty(_token))
				{
					_token = Application.Current.Properties["token"]?.ToString();
				}
				return _token;
			}
		}

		public async Task Logout()
		{
			Application.Current.Properties.Remove("token");
            Application.Current.Properties.Remove("user_inprogress_decision");
            Application.Current.Properties.Remove("user_saved_results");
			await Application.Current.SavePropertiesAsync();
			token = null;
		}

		public async Task Save()
		{
			await Application.Current.SavePropertiesAsync();
		}

		public ObservableCollection<DecisionOption> decisionOptions { get; set; }
		public ObservableCollection<UserDecision> inProgressDecisions { get; set; }
		public ObservableCollection<UserDecision> savedResults { get; set; }

		public void Update(bool IsAppStart)
		{
			if (Application.Current.Properties.ContainsKey("token"))
			{
				if (IsAppStart)
				{
                    if (Application.Current.Properties.ContainsKey("user_inprogress_decision"))
                    {
						inProgressDecisions.Clear();
						var inProgressCollection = JsonConvert.DeserializeObject<ObservableCollection<UserDecision>>(Application.Current.Properties["user_inprogress_decision"].ToString());
						if (inProgressCollection != null)
						{
							foreach (var inProgress in inProgressCollection)
							{
								inProgressDecisions.Add(inProgress);
							}
						}
                    }

                    if (Application.Current.Properties.ContainsKey("user_saved_results"))
                    {
						savedResults.Clear();
						var savedResultCollection = JsonConvert.DeserializeObject<ObservableCollection<UserDecision>>(Application.Current.Properties["user_saved_results"].ToString());
						if (savedResultCollection != null)
						{
							foreach (var savedResult in savedResultCollection)
							{
								savedResults.Add(savedResult);
							}
						}
                    }
				}
				else
				{
					Application.Current.Properties["user_inprogress_decision"] = JsonConvert.SerializeObject(inProgressDecisions);
					Application.Current.Properties["user_saved_results"] = JsonConvert.SerializeObject(savedResults);
					Application.Current.SavePropertiesAsync();
				}
			}
		}

		public event PropertyChangedEventHandler PropertyChanged;
		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = "")
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
    }
}
