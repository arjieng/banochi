﻿using System;
using Xamarin.Forms;

namespace banochixamarin
{
	public static class ScaleCodedHelper
	{
		public static float ScaleHeight(this int number)
		{
			return (float)(number * (App.screenHeight / 568.0));
		}

		public static float ScaleHeight(this double number)
		{
			return (float)(number * (App.screenHeight / 568.0));
		}

		public static float ScaleHeight(this float number)
		{
			return (float)(number * (App.screenHeight / 568.0));
		}

		public static float ScaleWidth(this int number)
		{
			return (float)(number * (App.screenWidth / 320.0));
		}

		public static float ScaleWidth(this double number)
		{
			return (float)(number * (App.screenWidth / 320.0));
		}

		public static float ScaleWidth(this float number)
		{
			return (float)(number * (App.screenWidth / 320.0));
		}

		public static double ScaleFont(this int number)
		{
			return (number * (App.screenHeight / 568.0) - (Device.RuntimePlatform == Device.iOS ? 0.5 : 0));
		}

		public static double scale(this double number)
		{
			return number * App.screenScale;
		}

		public static float scale(this float number)
		{
			return (float)(number * App.screenScale);
		}

		public static int scale(this int number)
		{
			return (int)(number * App.screenScale);
		}
	}
}
