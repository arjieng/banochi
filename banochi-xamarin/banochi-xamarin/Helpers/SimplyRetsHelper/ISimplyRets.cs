﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace banochixamarin
{
    public interface ISimplyRets
    {
        Task GetRequest(string url, CancellationToken ct, int type);
    }
}
