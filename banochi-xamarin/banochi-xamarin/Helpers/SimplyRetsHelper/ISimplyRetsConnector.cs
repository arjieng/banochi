﻿using System;
using System.Threading;
using Newtonsoft.Json.Linq;

namespace banochixamarin
{
    public interface ISimplyRetsConnector
    {
		void ReceiveSimplyRetsJSONData(JObject jsonData, CancellationToken ct);
        void ReceiveSimplyRetsJSONData(JArray jsonData, CancellationToken ct);
		void ReceiveSimplyRetsTimeoutError(string title, string error);
    }
}
