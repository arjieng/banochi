﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;

namespace banochixamarin
{
    public class SimplyRetsHelper : ISimplyRets
    {
		NetworkHelper networkHelper = NetworkHelper.GetInstance;
		HttpClient client;

		static SimplyRetsHelper _restService;
		WeakReference<ISimplyRetsConnector> _webServiceDelegate;
		public ISimplyRetsConnector WebServiceDelegate
		{
			get
			{
				ISimplyRetsConnector webServiceDelegate;
				return _webServiceDelegate.TryGetTarget(out webServiceDelegate) ? webServiceDelegate : null;
			}

			set
			{
				_webServiceDelegate = new WeakReference<ISimplyRetsConnector>(value);
			}
		}

		public static SimplyRetsHelper GetInstance
		{
			get { if (_restService == null) _restService = new SimplyRetsHelper(); return _restService; }
		}

        public SimplyRetsHelper()
        {
			var authData = string.Format("{0}:{1}", Constants.SIMPLYRETS_USERNAME, Constants.SIMPLYRETS_PASSWORD);
			var authHeaderValue = Convert.ToBase64String(Encoding.UTF8.GetBytes(authData));

			client = new HttpClient();
			client.MaxResponseContentBufferSize = 256000;
			client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authHeaderValue);
        }

        public async Task GetRequest(string url, CancellationToken ct, int type = 0)
        {
        	if (networkHelper.HasInternet())
        	{
        		if (await networkHelper.IsHostReachable() == true)
        		{
        			var uri = new Uri(url);

        			HttpResponseMessage response = await client.GetAsync(uri, ct);

        			if (response.IsSuccessStatusCode)
        			{
        				var result = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                        switch(type)
                        {
                            case 0: WebServiceDelegate?.ReceiveSimplyRetsJSONData(JObject.Parse(result), ct); break;
                            case 1: WebServiceDelegate?.ReceiveSimplyRetsJSONData(JArray.Parse(result), ct); break;
                        }
        			}
        			else
        			{
                        //300, 404, 500
        				WebServiceDelegate?.ReceiveSimplyRetsTimeoutError("MLS Error", response.StatusCode.ToString());
        			}
        		}
        		else
        		{
        			WebServiceDelegate?.ReceiveSimplyRetsTimeoutError("Host Unreachable!", "The URL host for Banochi cannot be reached and seems to be unavailable. Please try again later!");
        		}
        	}
        	else
        	{
        		WebServiceDelegate?.ReceiveSimplyRetsTimeoutError("No Internet Connection!", "Please check your internet connection, and try again!");
        	}
        }
    }
}
