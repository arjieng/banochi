﻿using System;
using System.Threading;
using Newtonsoft.Json.Linq;

namespace banochixamarin
{
	public interface IRestConnector
	{
		void ReceiveJSONData(JObject jsonData, CancellationToken ct);
		void ReceiveTimeoutError(string title, string error);
	}
}
