﻿using System;
using System.Diagnostics;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace banochixamarin
{
	public class RestServices : IRestService
	{
		NetworkHelper networkHelper = NetworkHelper.GetInstance;
		HttpClient client;

		static RestServices _restService;
		WeakReference<IRestConnector> _webServiceDelegate;
		public IRestConnector WebServiceDelegate
		{
			get
			{
				IRestConnector webServiceDelegate;
				return _webServiceDelegate.TryGetTarget(out webServiceDelegate) ? webServiceDelegate : null;
			}

			set
			{
				_webServiceDelegate = new WeakReference<IRestConnector>(value);
			}
		}

		public static RestServices GetInstance
		{
			get { if (_restService == null) _restService = new RestServices(); return _restService; }
		}

		public RestServices()
		{
			client = new HttpClient();
			client.DefaultRequestHeaders.Accept.Clear();
			client.MaxResponseContentBufferSize = 256000;
		}

		public async Task GetRequest(string url, CancellationToken ct)
		{
			if (networkHelper.HasInternet())
			{
				if (await networkHelper.IsHostReachable() == true)
				{
					var uri = new Uri(url);

					HttpResponseMessage response = await client.GetAsync(uri, ct);

					await RequestAsync(response, ct);
				}
				else
				{
					WebServiceDelegate?.ReceiveTimeoutError("Host Unreachable!", "The URL host for Banochi cannot be reached and seems to be unavailable. Please try again later!");
				}
			}
			else
			{
				WebServiceDelegate?.ReceiveTimeoutError("No Internet Connection!", "Please check your internet connection, and try again!");
			}
		}

		public async Task PostRequestAsync(string url, string dictionary, CancellationToken ct)
		{
			if (networkHelper.HasInternet())
			{
				if (await networkHelper.IsHostReachable() == true)
				{
					var uri = new Uri(url);
					var content = new StringContent(dictionary, Encoding.UTF8, "application/json");

					HttpResponseMessage response = await client.PostAsync(uri, content, ct);

					await RequestAsync(response, ct);
				}
				else
				{
					WebServiceDelegate?.ReceiveTimeoutError("Host Unreachable!", "The URL host for Banochi cannot be reached and seems to be unavailable. Please try again later!");
				}
			}
			else
			{
				WebServiceDelegate?.ReceiveTimeoutError("No Internet Connection!", "Please check your internet connection, and try again!");
			}
		}

		async Task RequestAsync(HttpResponseMessage response, CancellationToken ct)
		{
            Debug.WriteLine(response);
            ct.ThrowIfCancellationRequested();
			if (response.IsSuccessStatusCode)
			{
				var result = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
				WebServiceDelegate?.ReceiveJSONData(JObject.Parse(result), ct);
			}
			else
			{
				//300, 404, 500
				WebServiceDelegate?.ReceiveTimeoutError("Error!", response.StatusCode.ToString());
			}

		}

		public async Task PutRequestAsync(string url, string dictionary, CancellationToken ct)
		{
			if (networkHelper.HasInternet())
			{
				if (await networkHelper.IsHostReachable() == true)
				{
					var uri = new Uri(url);
					var content = new StringContent(dictionary, Encoding.UTF8, "application/json");

					HttpResponseMessage response = await client.PutAsync(uri, content, ct);

					await RequestAsync(response, ct);
				}
				else
				{
					WebServiceDelegate?.ReceiveTimeoutError("Host Unreachable!", "The URL host for Banochi cannot be reached and seems to be unavailable. Please try again later!");
				}
			}
			else
			{
				WebServiceDelegate?.ReceiveTimeoutError("No Internet Connection!", "Please check your internet connection, and try again!");
			}
		}

		public async Task DeleteRequestAsync(string url, CancellationToken ct)
		{
			if (networkHelper.HasInternet())
			{
				if (await networkHelper.IsHostReachable() == true)
				{
					var uri = new Uri(url);

					HttpResponseMessage response = await client.DeleteAsync(uri, ct);

					await RequestAsync(response, ct);
				}
				else
				{
					WebServiceDelegate?.ReceiveTimeoutError("Host Unreachable!", "The URL host for Banochi cannot be reached and seems to be unavailable. Please try again later!");
				}
			}
			else
			{
				WebServiceDelegate?.ReceiveTimeoutError("No Internet Connection!", "Please check your internet connection, and try again!");
			}
		}

        public async Task MultiPartDataContentAsync(string url, string primaryKey, string dictionary, System.IO.Stream image = null, CancellationToken ct = new CancellationToken(), RestMethod restMethod = RestMethod.Put, string secondaryKey = "token" , string keyParameter = "image")
		{
			if (networkHelper.HasInternet())
			{
				if (await networkHelper.IsHostReachable() == true)
				{
					var uri = new Uri(url);
					var json = JObject.Parse(dictionary)[primaryKey];
					var tokenJson = JObject.Parse(dictionary)[secondaryKey];
					var multipartFormData = new MultipartFormDataContent();

					foreach (var obj in json)
					{
						string[] keys = obj.Path.Split('.');
						if (keys[1].ToString() == keyParameter && image != null)
						{
                            
							string name = "\"" + keys[0] + "[" + keys[1] + "]" + "\"";
							//StreamContent content = new StreamContent(file.GetStream());
							StreamContent content = new StreamContent(image);
							content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("form-data") { FileName = "\"upload-image.jpeg\"", Name = name };
							multipartFormData.Add(content);

						}
						else
						{
							if (!string.IsNullOrEmpty(obj.First.ToString()))
							{
								string keyName = "\"" + keys[0] + "[" + keys[1] + "]" + "\"";
								StringContent content = new StringContent(obj.First.ToString(), System.Text.Encoding.UTF8);
								content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("form-data") { Name = keyName };
								multipartFormData.Add(content);
							}
						}
					}

					if (secondaryKey.Equals("token"))
					{
						string tokenName = '"' + secondaryKey + '"';
						StringContent contentTokens = new StringContent(tokenJson.ToString(), System.Text.Encoding.UTF8);
						contentTokens.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("form-data") { Name = tokenName };
						multipartFormData.Add(contentTokens);
					}
					else
					{
						foreach (var obj in tokenJson)
						{
							string[] keys = obj.Path.Split('.');
							if (!string.IsNullOrEmpty(obj.First.ToString()))
							{
								string secondaryKeyName = "\"" + keys[0] + "[" + keys[1] + "]" + "\"";
								StringContent content = new StringContent(obj.First.ToString(), System.Text.Encoding.UTF8);
								content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("form-data") { Name = secondaryKeyName };
								multipartFormData.Add(content);
							}
						}
					}

                    HttpResponseMessage response = restMethod == RestMethod.Put ? await client.PutAsync(uri, multipartFormData, ct) : await client.PostAsync(uri, multipartFormData, ct);

					await RequestAsync(response, ct);
				}
				else
				{
					WebServiceDelegate?.ReceiveTimeoutError("Host Unreachable!", "The URL host for Banochi cannot be reached and seems to be unavailable. Please try again later!");
				}
			}
			else
			{
				WebServiceDelegate?.ReceiveTimeoutError("No Internet Connection!", "Please check your internet connection, and try again!");
			}
		}
	}

    public enum RestMethod
    {
        Post,
        Put
    }
}
