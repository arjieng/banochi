﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace banochixamarin
{
	public interface IRestService
	{
		Task PostRequestAsync(string url, string dictionary, CancellationToken ct);
		Task GetRequest(string url, CancellationToken ct);
		Task PutRequestAsync(string url, string dictionary, CancellationToken ct);
		Task DeleteRequestAsync(string url, CancellationToken ct);
        Task MultiPartDataContentAsync(string url, string primaryKey, string dictionary, System.IO.Stream image = null, CancellationToken ct = new CancellationToken(), RestMethod restMethod = RestMethod.Put, string secondaryKey = "token", string keyParameter = "image");
	}
}
