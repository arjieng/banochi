﻿using System;
using System.Threading.Tasks;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using Xamarin.Forms;

namespace banochixamarin
{
	public class CameraHelper : ICameraInterface
	{
		public async Task<MediaFile> PickPhotoAsync(PickMediaOptions options = null)
		{
			var status = PermissionStatus.Unknown;
			status = await PermissionHelper.CheckPermissions(Permission.Photos);
			if (status == PermissionStatus.Granted)
			{
				if (!CrossMedia.Current.IsPickPhotoSupported)
				{
					Application.Current?.MainPage?.DisplayAlert("Photos Not Supported", "Permission not granted to photos.", "Okay");
					return null;
				}
				var file = await CrossMedia.Current.PickPhotoAsync(options);

				if (file == null)
					return null;

				return file;
			}
			else
			{
				Application.Current?.MainPage?.DisplayAlert("Photos Not Supported", "Permission not granted to photos.", "Okay");
				return null;
			}
		}

		public async Task<MediaFile> TakePhotoAsync(StoreCameraMediaOptions options)
		{
			var cameraStatus = PermissionStatus.Unknown;
			var storageStatus = PermissionStatus.Unknown;
			cameraStatus = await PermissionHelper.CheckPermissions(Permission.Camera);
			storageStatus = await PermissionHelper.CheckPermissions(Permission.Storage);

			if (cameraStatus != PermissionStatus.Granted || storageStatus != PermissionStatus.Granted)
			{
				var results = await CrossPermissions.Current.RequestPermissionsAsync(new[] { Permission.Camera, Permission.Storage });
				if (results.ContainsKey(Permission.Camera) && results.ContainsKey(Permission.Storage))
				{
					cameraStatus = results[Permission.Camera];
					storageStatus = results[Permission.Storage];
				}
			}
			if (cameraStatus == PermissionStatus.Granted && storageStatus == PermissionStatus.Granted)
			{
				if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
				{
					Application.Current?.MainPage?.DisplayAlert("No Camera", "No camera available.", "Okay");
					return null;
				}

				var file = await CrossMedia.Current.TakePhotoAsync(options);

				if (file == null)
					return null;

				return file;
			}
			else
			{
				Application.Current?.MainPage?.DisplayAlert("No Camera", "Permission not granted.", "Okay");
				return null;
			}
		}

		public async Task<MediaFile> PickVideoAsync()
		{
			var status = PermissionStatus.Unknown;
			status = await PermissionHelper.CheckPermissions(Permission.Photos);
			if (status == PermissionStatus.Granted)
			{
				if (!CrossMedia.Current.IsPickVideoSupported)
				{
					Application.Current?.MainPage?.DisplayAlert("Videos Not Supported", "Permission not granted to videos.", "Okay");
					return null;
				}

				var file = await CrossMedia.Current.PickVideoAsync();

				if (file == null)
					return null;

				return file;
			}
			else
			{
				Application.Current?.MainPage?.DisplayAlert("Videos Not Supported", "Permission not granted to videos.", "Okay");
				return null;
			}
		}

		public async Task<MediaFile> TakeVideoAsync(StoreVideoOptions options)
		{
			var status = PermissionStatus.Unknown;
			status = await PermissionHelper.CheckPermissions(Permission.Camera);
			if (status == PermissionStatus.Granted)
			{
				if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakeVideoSupported)
				{
					Application.Current?.MainPage?.DisplayAlert("No Camera", "No camera available.", "Okay");
					return null;
				}

				var file = await CrossMedia.Current.TakeVideoAsync(options);

				if (file == null)
					return null;

				return file;
			}
			else
			{
				Application.Current?.MainPage?.DisplayAlert("No Camera", "Permission not granted.", "Okay");
				return null;
			}
		}
	}
}
