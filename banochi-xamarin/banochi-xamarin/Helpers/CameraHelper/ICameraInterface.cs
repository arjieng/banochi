﻿using System;
using System.Threading.Tasks;
using Plugin.Media.Abstractions;

namespace banochixamarin
{
	public interface ICameraInterface
	{
		Task<MediaFile> PickPhotoAsync(PickMediaOptions options = null);
		Task<MediaFile> TakePhotoAsync(StoreCameraMediaOptions options);
		Task<MediaFile> PickVideoAsync();
		Task<MediaFile> TakeVideoAsync(StoreVideoOptions options);
	}
}
