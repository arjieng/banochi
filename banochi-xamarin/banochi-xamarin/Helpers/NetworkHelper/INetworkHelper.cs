﻿using System;
using System.Threading.Tasks;

namespace banochixamarin
{
	public interface INetworkHelper
	{
		bool HasInternet();
		Task<bool> IsHostReachable();
	}
}
