﻿using System;
using System.IO;
using System.Threading.Tasks;

namespace banochixamarin.Helpers.ImageHelper
{
    public interface IImagePicker
    {
        Task<Stream> GetImageStreamAsync();
    }
}
