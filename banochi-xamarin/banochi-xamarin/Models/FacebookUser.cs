﻿using System;

namespace banochixamarin
{
	public class FacebookUser
	{
		public FacebookUser(string id, string token, string name, string email, string age, string imageUrl)
		{
			Id = id;
			Token = token;
			Name = name;
			Email = email;
			Picture = imageUrl;
			Age = age;
		}

		public string Id { get; set; }
		public string Token { get; set; }
		public string Name { get; set; }
		public string Email { get; set; }
		public string Age { get; set; }
		public string Picture { get; set; }
	}
}
