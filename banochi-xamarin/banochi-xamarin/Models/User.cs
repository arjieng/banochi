﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace banochixamarin
{
    public class User : INotifyPropertyChanged
    {
		public int id { get ; set; }
		string _email;
		public string email { get { return _email; } set { _email = value; OnPropertyChanged(nameof(email)); } }
		string _token;
		public string token { get { return _token; } set { _token = value; OnPropertyChanged(nameof(token)); } }

        string _image;
        public string image
        {
            get
            {
                if (_image != "ProfilePlaceHolder.png")
                    return Constants.ROOT + _image;
                return _image;
            }
            set { if (value == null) { _image = "ProfilePlaceHolder.png"; } else { _image = value; } OnPropertyChanged(nameof(image)); }
        }

        string _name;
        public string name { get { return _name; } set { _name = value; OnPropertyChanged(nameof(name)); } }

    	public event PropertyChangedEventHandler PropertyChanged;
    	protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = "")
    	{
    		PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    	}

        public void Update(){
            Application.Current.Properties["user"] = JsonConvert.DeserializeObject<User>(JsonConvert.SerializeObject(new { id = this.id, token = _token, email = _email, image = _image, name = _name }));
            Application.Current.SavePropertiesAsync();
        }
    }
}
