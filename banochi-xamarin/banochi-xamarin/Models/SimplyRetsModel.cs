﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace banochixamarin
{
    public class Parking
    {
        public object leased { get; set; }
        public int spaces { get; set; }
        public string description { get; set; }
    }

    public class Property
    {
        public string roof { get; set; }
        public object cooling { get; set; }
        public string style { get; set; }
        public int area { get; set; }
        public int bathsFull { get; set; }
        public int bathsHalf { get; set; }
        public int stories { get; set; }
        public int fireplaces { get; set; }
        public object flooring { get; set; }
        public string heating { get; set; }
        public object bathrooms { get; set; }
        public string foundation { get; set; }
        public string laundryFeatures { get; set; }
        public object occupantName { get; set; }
        public object ownerName { get; set; }
        public string lotDescription { get; set; }
        public string pool { get; set; }
        public object subType { get; set; }
        public int bedrooms { get; set; }
        public string interiorFeatures { get; set; }
        public string lotSize { get; set; }
        public string areaSource { get; set; }
        public object maintenanceExpense { get; set; }
        public string additionalRooms { get; set; }
        public string exteriorFeatures { get; set; }
        public object water { get; set; }
        public string view { get; set; }
        public object lotSizeArea { get; set; }
        public string subdivision { get; set; }
        public string construction { get; set; }
        public Parking parking { get; set; }
        public object lotSizeAreaUnits { get; set; }
        public string type { get; set; }
        public double garageSpaces { get; set; }
        public object bathsThreeQuarter { get; set; }
        public string accessibility { get; set; }
        public object acres { get; set; }
        public object occupantType { get; set; }
        public object subTypeText { get; set; }
        public int yearBuilt { get; set; }
    }

    public class Office
    {
        public object contact { get; set; }
        public object name { get; set; }
        public object servingName { get; set; }
        public object brokerid { get; set; }
    }

    public class Address
    {
        public string crossStreet { get; set; }
        public string state { get; set; }
        public string country { get; set; }
        public string postalCode { get; set; }
        public string streetName { get; set; }
        public string streetNumberText { get; set; }
        public string city { get; set; }
        public int streetNumber { get; set; }
        public string full { get; set; }
        public string unit { get; set; }

        public string fullAddress { get { return full + ", " + city + ", " + state + ", " + country; } }
    }

    public class Agent
    {
        public string lastName { get; set; }
        public object contact { get; set; }
        public string firstName { get; set; }
        public string id { get; set; }
    }

    public class School
    {
        public string middleSchool { get; set; }
        public string highSchool { get; set; }
        public string elementarySchool { get; set; }
        public object district { get; set; }
    }

    public class Mls
    {
        public string status { get; set; }
        public string area { get; set; }
        public int daysOnMarket { get; set; }
        public object originatingSystemName { get; set; }
        public string statusText { get; set; }
        public object areaMinor { get; set; }
    }

    public class Geo
    {
        public string county { get; set; }
        public double lat { get; set; }
        public double lng { get; set; }
        public string marketArea { get; set; }
        public string directions { get; set; }
    }

    public class Tax
    {
        public int taxYear { get; set; }
        public int taxAnnualAmount { get; set; }
        public string id { get; set; }
    }

    public class CoAgent
    {
        public object lastName { get; set; }
        public object contact { get; set; }
        public object firstName { get; set; }
        public string id { get; set; }
    }

    public class Office2
    {
        public object contact { get; set; }
        public string name { get; set; }
        public string servingName { get; set; }
        public string brokerid { get; set; }
    }

    public class Agent2
    {
        public string lastName { get; set; }
        public object contact { get; set; }
        public string firstName { get; set; }
        public string id { get; set; }
    }

    public class Sales
    {
        public DateTime closeDate { get; set; }
        public Office2 office { get; set; }
        public int closePrice { get; set; }
        public Agent2 agent { get; set; }
        public object contractDate { get; set; }
    }

    public class Association
    {
        public int fee { get; set; }
        public string name { get; set; }
        public string amenities { get; set; }
    }

    public class SimplyRetsModel
    {
        public string privateRemarks { get; set; }
        public Property property { get; set; }
        public int mlsId { get; set; }
        public string terms { get; set; }
        public string showingInstructions { get; set; }
        public Office office { get; set; }
        public object leaseTerm { get; set; }
        public string disclaimer { get; set; }
        public Address address { get; set; }
        public string agreement { get; set; }
        public DateTime listDate { get; set; }
        public Agent agent { get; set; }
        public DateTime modified { get; set; }
        public School school { get; set; }
        public List<string> photos { get; set; }
        public int listPrice { get; set; }
        public string listingId { get; set; }
        public Mls mls { get; set; }
        public Geo geo { get; set; }
        public Tax tax { get; set; }
        public CoAgent coAgent { get; set; }
        public Sales sales { get; set; }
        public string leaseType { get; set; }
        public object virtualTourUrl { get; set; }
        public string remarks { get; set; }
        public Association association { get; set; }
    }
}
