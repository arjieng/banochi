﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Xamarin.Forms;

namespace banochixamarin
{
    public class UserDecision : INotifyPropertyChanged
    {
		int _id;
		public int id { get { return _id; } set { _id = value; OnPropertyChanged(nameof(id)); } }
		string _home_type;
		public string home_type { get { return _home_type; } set { _home_type = value; OnPropertyChanged(nameof(home_type)); } }
		string _zip_code;
		public string zip_code { get { return _zip_code; } set { _zip_code = value; OnPropertyChanged(nameof(zip_code)); } }
		string _date_created;
		public string date_created { get { return _date_created; } set { _date_created = value; OnPropertyChanged(nameof(date_created)); } }
		
        public ObservableCollection<DecisionOption> userOptions { get; set; }
		
        public ObservableCollection<CriteriaModel> criteriaSelected { get; set; }
		
        public ObservableCollection<DecisionResult> decisionResult { get; set; }

		//for listview drawer
		bool _is_swiped;
		public bool is_swiped { get { return _is_swiped; } set { _is_swiped = value; OnPropertyChanged(nameof(is_swiped)); } }

        //option count
        public string option_count { get { if (userOptions != null) { switch(userOptions.Count) { case 0: return "No Property"; case 1: return userOptions.Count + " Property"; default: return userOptions.Count + " Properties"; }  } return "No Property"; } }

		public event PropertyChangedEventHandler PropertyChanged;
		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = "")
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}

		public void Update(UserDecision userDecision)
		{
            id = userDecision.id;
            home_type = userDecision.home_type;
            zip_code = userDecision.zip_code;
            date_created = userDecision.date_created;

            userOptions = new ObservableCollection<DecisionOption>();
            if(userDecision.userOptions != null)
            {
                foreach(var item in userDecision.userOptions)
                {
                    DecisionOption decisionOption = new DecisionOption();
                    decisionOption.Update(item);
                    userOptions.Add(decisionOption);
                }
            }

            criteriaSelected = new ObservableCollection<CriteriaModel>();
            if(userDecision.criteriaSelected != null)
            {
                foreach(var item in userDecision.criteriaSelected)
                {
                    CriteriaModel criteriaModel = new CriteriaModel();
                    criteriaModel.Update(item);
                    criteriaSelected.Add(criteriaModel);
                }
            }

            decisionResult = new ObservableCollection<DecisionResult>();
            if(userDecision.decisionResult != null)
            {
                foreach(var item in userDecision.decisionResult)
                {
                    DecisionResult result = new DecisionResult();
                    result.Update(item);
                    decisionResult.Add(result);
                }
            }
		}
    }

    public class DecisionOption : INotifyPropertyChanged
    {
		int _id;
		public int id { get { return _id; } set { _id = value; OnPropertyChanged(nameof(id)); } }
		string _home_name;
		public string home_name { get { return _home_name; } set { _home_name = value; OnPropertyChanged(nameof(home_name)); } }
		string _mls_id;
		public string mls_id { get { return _mls_id; } set { _mls_id = value; OnPropertyChanged(nameof(mls_id)); } }
		string _address;
		public string address { get { return _address; } set { _address = value; OnPropertyChanged(nameof(address)); } }
		
        public ObservableCollection<CriteriaModel> criteriaWeight { get; set; }
		int _has_mls;
		public int has_mls { get { return _has_mls; } set { _has_mls = value; OnPropertyChanged(nameof(has_mls)); } }
		
        public SimplyRetsModel simplyRetsProperty { get; set; }

		//for listview drawer
		bool _is_swiped;
		public bool is_swiped { get { return _is_swiped; } set { _is_swiped = value; OnPropertyChanged(nameof(is_swiped)); } }

		public event PropertyChangedEventHandler PropertyChanged;
		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = "")
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}

        public void Update(DecisionOption decisionOption)
        {
            id = decisionOption.id;
            home_name = decisionOption.home_name;
            mls_id = decisionOption.mls_id;
            address = decisionOption.address;

            criteriaWeight = new ObservableCollection<CriteriaModel>();
            if(decisionOption.criteriaWeight != null)
            {
                foreach(var item in decisionOption.criteriaWeight)
                {
                    CriteriaModel criteriaModel = new CriteriaModel();
                    criteriaModel.Update(item);
                    criteriaWeight.Add(criteriaModel);
                }
            }

            has_mls = decisionOption.has_mls;

            simplyRetsProperty = new SimplyRetsModel();
			if (decisionOption.simplyRetsProperty != null)
			{
                simplyRetsProperty = decisionOption.simplyRetsProperty;
			}
        }
    }

    public class DecisionResult : INotifyPropertyChanged
    {
		int _id;
		public int id { get { return _id; } set { _id = value; OnPropertyChanged(nameof(id)); } }
		string _home_name;
		public string home_name { get { return _home_name; } set { _home_name = value; OnPropertyChanged(nameof(home_name)); } }
		double _result_value;
		public double result_value { get { return _result_value; } set { _result_value = value; OnPropertyChanged(nameof(result_value)); OnPropertyChanged(nameof(bar_height)); } }

        //for graph
		public double bar_height { get { if (result_value.Equals(0.0)) { return 0.0; } return (result_value / 10) * Constants.BAR_FULL_WIDTH; } }

		public event PropertyChangedEventHandler PropertyChanged;
		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = "")
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}

		public void Update(DecisionResult decisionResult)
		{
            id = decisionResult.id;
            home_name = decisionResult.home_name;
            result_value = decisionResult.result_value;
		}
    }

	public class CriteriaModel : INotifyPropertyChanged
	{
		int _id;
		public int id { get { return _id; } set { _id = value; OnPropertyChanged(nameof(id)); } }
		int _is_selected;
		public int is_selected { get { return _is_selected; } set { _is_selected = value; OnPropertyChanged(nameof(is_selected)); } }
		string _title;
		public string title { get { return _title; } set { _title = value; OnPropertyChanged(nameof(title)); } }
		double _criteria_value;
		public double criteria_value { get { return _criteria_value; } set { _criteria_value = value; OnPropertyChanged(nameof(criteria_value)); OnPropertyChanged(nameof(bar_height)); OnPropertyChanged(nameof(label_margin)); } }

		//for progress
		public double bar_height { get { if (criteria_value.Equals(0)) { return 0.0; } return criteria_value / 10; } }

		//for graph
		public Thickness label_margin 
        { 
            get 
            { 
                switch(Math.Round(criteria_value, 1))
                {
                    case 0.0:
                        return new Thickness(0, 0, 0, 0);
                    case 9.7:
                        return new Thickness(((criteria_value / 10) * Constants.PROGRESS_FULL_WIDTH) - 10.ScaleWidth(), 0, 0, 0);
					case 9.8:
                        return new Thickness(((criteria_value / 10) * Constants.PROGRESS_FULL_WIDTH) - 12.ScaleWidth(), 0, 0, 0);
                    case 9.9:
                        return new Thickness(((criteria_value / 10) * Constants.PROGRESS_FULL_WIDTH) - 14.ScaleWidth(), 0, 0, 0);
                    case 10.0:
                        if(Device.RuntimePlatform == Device.iOS)
                        {
                            return new Thickness(((criteria_value / 10) * Constants.PROGRESS_FULL_WIDTH) - 23.ScaleWidth(), 0, 0, 0);
                        }
                        return new Thickness(((criteria_value / 10) * Constants.PROGRESS_FULL_WIDTH) - 20.ScaleWidth(), 0, 0, 0);
                    default:
                        return new Thickness(((criteria_value / 10) * Constants.PROGRESS_FULL_WIDTH) - 8.ScaleWidth(), 0, 0, 0);
                }
            } 
        }

		//for icons
		string _image;
		public string image { get { return _image; } set { _image = value; OnPropertyChanged(nameof(image)); } }

		public event PropertyChangedEventHandler PropertyChanged;
		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = "")
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}

        public void Update(CriteriaModel criteriaModel)
        {
            id = criteriaModel.id;
            is_selected = criteriaModel.is_selected;
            title = criteriaModel.title;
            criteria_value = criteriaModel.criteria_value;
            image = criteriaModel.image;
        }
	}
}
