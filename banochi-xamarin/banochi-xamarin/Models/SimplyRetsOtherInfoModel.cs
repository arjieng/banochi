﻿using System;
namespace banochixamarin.Models
{
    public class SimplyRetsOtherInfoModel
    {
        public string image { get; set; }
        public string title { get; set; }
        public SimplyRetsOtherInfoModel(string image, string title){
            this.image = image;
            this.title = title;
        }
    }
}
