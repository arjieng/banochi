﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace banochixamarin
{
    public partial class MyMasterDetailPage : MasterDetailPage
    {
        DataClass dataClass = DataClass.GetInstance;

        public MyMasterDetailPage()
        {
            InitializeComponent();

			IsGestureEnabled = Device.RuntimePlatform == Device.iOS ? false : true;
			masterPage.ListView.ItemSelected += OnItemSelected;
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();

			MessagingCenter.Unsubscribe<Object, string>(this, "ResetPassword");
		}

		async void OnItemSelected(object sender, SelectedItemChangedEventArgs e)
		{
			var item = e.SelectedItem as MasterPageItem;
			if (item != null)
			{
				if (item.Title.Equals("Logout"))
				{
					MasterPage.PageSelected.Invoke(this, 1);
					await dataClass.Logout();
					App.Logout();
				}
				else
				{
					MasterPage.SelectedPage.Invoke(this, item.TargetType);
					Detail = new NavigationPage((Page)Activator.CreateInstance(item.TargetType));
					IsPresented = false;
				}
			}
		}
    }
}
