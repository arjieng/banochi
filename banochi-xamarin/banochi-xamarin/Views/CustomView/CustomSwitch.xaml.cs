﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace banochixamarin
{
	public partial class CustomSwitch : ContentView
	{
		public static readonly BindableProperty IsToggledProperty = BindableProperty.Create("IsToggled", typeof(int), typeof(CustomSwitch), 0, propertyChanged: OnProperty_Changed);
		public static readonly BindableProperty SwitchLabelProperty = BindableProperty.Create("SwitchLabel", typeof(string), typeof(CustomSwitch), string.Empty);
		public CustomSwitch()
		{
			InitializeComponent();
		}

		static void OnProperty_Changed(BindableObject bindable, object oldValue, object newValue)
		{
			CircularBoxView backgroundButton = (bindable as ContentView).Content.FindByName<CircularBoxView>("buttonBackground");
			CircularBoxView switchButton = (bindable as ContentView).Content.FindByName<CircularBoxView>("buttonSwitch");

			if (Int32.Parse(newValue.ToString()) == 1)
			{
				backgroundButton.BackgroundColor = Constants.SWITCH_ON_BACKGROUND_COLOR;
				switchButton.BackgroundColor = Constants.SWITCH_ON_COLOR;
				switchButton.TranslateTo(16.ScaleWidth(), 0, 500, Easing.CubicOut);
			}
			else
			{
				backgroundButton.BackgroundColor = Constants.SWITCH_OFF_BACKGROUND_COLOR;
				switchButton.BackgroundColor = Constants.SWITCH_OFF_COLOR;
				switchButton.TranslateTo(0, 0, 500, Easing.CubicOut);
			}
		}

		public int IsToggled
		{
			get { return Int32.Parse(base.GetValue(IsToggledProperty).ToString()); }
			set { SetValue(IsToggledProperty, value); }
		}

		public string SwitchLabel
		{
			get { return base.GetValue(SwitchLabelProperty).ToString(); }
			set { SetValue(SwitchLabelProperty, value); }
		}
	}
}
