﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace banochixamarin
{
    public partial class AddressView : ContentView
    {
        Address address;

        public AddressView(Address address)
        {
            InitializeComponent();

            this.address = address;
        }

        protected override void OnBindingContextChanged()
        {
            base.OnBindingContextChanged();
            BindingContext = address;
        }

        void OnStack_Tapped(object sender, EventArgs e)
        {
            
        }
    }
}
