﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace banochixamarin
{
    public partial class TaxView : ContentView
    {
        Tax tax;

        public TaxView(Tax tax)
        {
            InitializeComponent();

            this.tax = tax;
        }

		protected override void OnBindingContextChanged()
		{
			base.OnBindingContextChanged();
			BindingContext = tax;
		}

		void OnStack_Tapped(object sender, EventArgs e)
		{

		}
    }
}
