﻿using System;
using FFImageLoading.Forms;
using Xamarin.Forms;

namespace banochixamarin
{
	public class ViewTemplate : DataTemplateSelector
	{
		protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
		{
			if (item != null)
			{
				return new DataTemplate(() =>
				{
					var dtemp = new CachedImage()
					{
						DownsampleWidth = 320.ScaleFont(),
						DownsampleHeight = 220.ScaleHeight(),
						Aspect = Aspect.AspectFill,
						Source = item.ToString()
					};

                    TapGestureRecognizer tap = new TapGestureRecognizer();
                    tap.Tapped += (sender, e) =>
                    {
                        ((CachedImage)sender).Navigation.PushAsync(new PropertyImageViewerPage(item.ToString()));
                    };

                    dtemp.GestureRecognizers.Add(tap);
                   
					return dtemp;
				});
			}
			return null;
		}
	}
}
