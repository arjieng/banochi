﻿using System;
using System.Collections.Generic;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;

namespace banochixamarin
{
    public partial class ResetPasswordPage : RootViewPage, IRestConnector
    {
        DataClass dataClass = DataClass.GetInstance;
		bool isClicked;

#if DEBUG == false
        RestServices restService = new RestServices();
        CancellationTokenSource cts;
#endif
		public ResetPasswordPage()
        {
            InitializeComponent();

			this.PageTitle = "RESET PASSWORD";
			this.RightIcon2 = "CloseLight";
			this.RightButton2Command = new Command((obj) => { Navigation.PopModalAsync(); });

#if DEBUG == false
            restService.WebServiceDelegate = this;
#endif
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();

            isClicked = false;

			DependencyService.Get<IStatusStyle>().StatusStyle(0);
		}

		protected override void OnDisappearing()
		{
			base.OnDisappearing();
		}

		void OnEntry_Completed(object sender, EventArgs e)
		{
			confirmPasswordEntry.Focus();
		}

		void DisplayError(Entry entry, string error)
		{
			entry.Text = String.Empty;
			entry.Placeholder = error;
			entry.PlaceholderColor = Color.Red;
		}

		void OnSend_Clicked(object sender, EventArgs e)
		{
			bool isNotError = true;

			if (String.IsNullOrEmpty(passwordEntry.Text))
			{
				DisplayError(passwordEntry, "New Password is required!");
				confirmPasswordEntry.Text = string.Empty;
				isNotError = false;
			}
			else
			{
				if (!passwordEntry.Text.Equals(confirmPasswordEntry.Text))
				{
					DisplayError(passwordEntry, "Password didn't match!");
					confirmPasswordEntry.Text = string.Empty;
					isNotError = false;
				}
			}

			if (isNotError && isClicked == false)
			{
				isClicked = true;
#if DEBUG == false
                //cts = new CancellationTokenSource();

                //try
                //{
                //    var json = JsonConvert.SerializeObject(new { email = dataClass.user.email, password = newPassword.Text });
                //    await restService.PutRequestAsync(Constants.ROOT_URL + Constants.RESET_PASSWORD_URL, json, cts.Token);
                //}
                //catch (OperationCanceledException ex)
                //{
                //    App.Log(ex.StackTrace + ex.Message, "ResetPassword");
                //    isClicked = false;
                //}
                //catch (Exception ex)
                //{
                //    App.Log(ex.StackTrace + ex.Message, "ResetPassword");
                //    isClicked = false;
                //}

                //cts = null;    
#endif
			}
		}

		public void ReceiveJSONData(JObject jsonData, CancellationToken ct)
		{
			if (int.Parse(jsonData["status"].ToString()) == 200)
			{
				DisplayAlert("Reset Password", "Password changed successfully", "Okay");

				dataClass.user = JsonConvert.DeserializeObject<User>(jsonData["user"].ToString());
				dataClass.token = jsonData["user"]["token"].ToString();

				dataClass.Update(false);
				App.ShowMainPage();
			}
			else
			{
				DisplayError(passwordEntry, jsonData["error"].ToString());
			}

			isClicked = false;
		}

		public void ReceiveTimeoutError(string title, string error)
		{
			DisplayAlert(title, error, "Okay");
			isClicked = false;
		}
    }
}
