﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace banochixamarin
{
    public partial class SavedResultsPage : RootViewPage
    {
		UserDecision deleteItem;
		DataClass dataClass = DataClass.GetInstance;

        public SavedResultsPage()
        {
            InitializeComponent();

            this.PageTitle = "SAVED RESULT";
			this.LeftIcon = "BurgerDark";
			this.LeftButtonCommand = new Command((obj) => { DependencyService.Get<IStatusStyle>().StatusStyle(1); MyMasterDetailPage detail = (MyMasterDetailPage)this.Parent.Parent; detail.IsPresented = true; });

            optionsList.ItemsSource = dataClass.savedResults;
        }

		protected override void OnAppearing()
		{
			base.OnAppearing();

			DependencyService.Get<IStatusStyle>().StatusStyle(1);
		}

		protected override void OnDisappearing()
		{
			base.OnDisappearing();
		}

		void ItemOptions_Clicked(object sender, System.EventArgs e)
		{
			Button button = sender as Button;
			var item = button.CommandParameter as UserDecision;
			DecisionListAction(button.ClassId, item);
		}

		void OptionItem_Tapped(object sender, object e)
		{
			GestureStackLayout button = sender as GestureStackLayout;
			var item = e as UserDecision;
			DecisionListAction(button.ClassId, item);
		}

		void DecisionListAction(string classid, UserDecision decisionItem)
		{
			switch (classid)
			{
				case "0"://View

					break;
				case "1"://Edit

					break;
				case "2"://Remove
					deleteItem = decisionItem;
					OnDeleteSuccessWS();
					break;
			}
		}

		void OnDeleteSuccessWS()
		{
			if (deleteItem != null)
			{
				for (int i = 0; i < dataClass.savedResults.Count; i++)
				{
					if (deleteItem.id == dataClass.savedResults[i].id)
					{
						// Xamarin Forms Bug : Bug 56793 - ListView with uneven row height not updating layout if items change size after item has been deleted
						// https://bugzilla.xamarin.com/show_bug.cgi?id=56793
						// Bug in Android: Cannot swipe left and right after one item is deleted because swiping involves changing of layout position.
						// Workaround: Set ItemSource to null, remove the item and restore the source.

						switch (Device.RuntimePlatform)
						{
							case Device.iOS:
								dataClass.savedResults.Remove(deleteItem);
								break;
							case Device.Android:
								optionsList.ItemsSource = null;
								dataClass.savedResults.Remove(deleteItem);
								//dataClass.savedResults.DeleteReference.Invoke(this, null);
								optionsList.ItemsSource = dataClass.savedResults;
								break;
						}
					}
				}
			}
			deleteItem = null;
		}

		void OnLast_ItemAppearing(object sender, Xamarin.Forms.ItemVisibilityEventArgs e)
		{

		}
    }
}
