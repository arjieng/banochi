﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using banochixamarin.Helpers.ImageHelper;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Xamarin.Forms;

namespace banochixamarin
{
    public partial class ProfilePage : RootViewPage, IRestConnector
    {
        DataClass dataClass = DataClass.GetInstance;
		CameraHelper cameraHelper = new CameraHelper();
		MediaFile file;

#if DEBUG == false
        CancellationTokenSource cts;
        RestServices restService = new RestServices();
#endif


        public ProfilePage()
        {
            InitializeComponent();

			this.PageTitle = "PROFILE";
			this.RightIcon2 = "CloseLight";
			this.RightButton2Command = new Command((obj) => { Navigation.PopModalAsync(); });

            System.Diagnostics.Debug.WriteLine("User: "+ dataClass.user);

            //ciUserImage.Source = dataClass.user.image;
            BindingContext = dataClass.user;
        }

        public void ReceiveJSONData(JObject jsonData, CancellationToken ct)
        {
            Device.BeginInvokeOnMainThread(() => {
                if(jsonData["status"].ToString() == "200"){
                    dataClass.user = JsonConvert.DeserializeObject<User>(jsonData["user"].ToString());
                }
            });
        }

        public void ReceiveTimeoutError(string title, string error)
        {
            Device.BeginInvokeOnMainThread(()=>{
                DisplayAlert(title, error, "OK");
            });
        }

        protected override void OnAppearing()
		{
			base.OnAppearing();
#if DEBUG == false
            restService.WebServiceDelegate = this;
#endif
            DependencyService.Get<IStatusStyle>().StatusStyle(0);
		}

		protected override void OnDisappearing()
		{
			base.OnDisappearing();
		}

        async void ImagePickTapped(object sender, System.EventArgs e)
        {

            //Stream imgstream = await DependencyService.Get<IImagePicker>().GetImageStreamAsync();

            //if (imgstream != null)
            //{
            //    ciProfileImage.Source = ImageSource.FromStream(() => imgstream);
            //    this.RightIcon1 = "CheckWhite";
            //    this.RightButton1Command = new Command((arg) =>
            //    {
            //        Navigation.PopModalAsync();
            //    });
            //}

			var myAction = await DisplayActionSheet(null, "Cancel", null, "Camera", "Gallery");
			if (myAction != null)
			{
				if (myAction.ToString() != "Cancel")
				{
                    file = null;
					await CrossMedia.Current.Initialize();
					if (myAction.ToString() == "Camera")
					{
						file = await cameraHelper.TakePhotoAsync(new StoreCameraMediaOptions()
						{
							Directory = "Banochi",
							Name = $"{DateTime.UtcNow}.jpg",
							CompressionQuality = 92,
							MaxWidthHeight = 320.scale(),
							DefaultCamera = CameraDevice.Front,
							PhotoSize = PhotoSize.Custom
						});
						
					}
					else if (myAction.ToString() == "Gallery")
					{
						file = await cameraHelper.PickPhotoAsync(new PickMediaOptions()
						{
							MaxWidthHeight = 320.scale(),
							CompressionQuality = 92,
							PhotoSize = PhotoSize.Custom,
							RotateImage = true
						});
						//if (file != null)
						//{
							//ciProfileImage.Source = ImageSource.FromStream(() => { var stream = file.GetStreamWithImageRotatedForExternalStorage(); return stream; });
                        //    App.Log(ImageSource.FromStream(() => { var stream = file.GetStreamWithImageRotatedForExternalStorage(); return stream; }).ToString(), "OK111111");
                        //}
					}

                    if (file != null)
                    {
                        try{
                            ciProfileImage.Source = file.Path;
                            cts = new CancellationTokenSource();

                            var json = JsonConvert.SerializeObject(new { user = new { image = "", token = dataClass.token }, device = new { platform = App.DeviceType, token = App.UDID } });
                            await restService.MultiPartDataContentAsync(Constants.IMAGE_UPLOAD, "user", json, file.GetStreamWithImageRotatedForExternalStorage(), cts.Token, RestMethod.Post, "device", "image");
                        }catch(Exception ee){
                            App.Log(ee.ToString(), "OKOKOKOKOKOKO");
                        }
                    }


				}
			}
        }
    }
}
