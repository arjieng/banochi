﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;

namespace banochixamarin
{
    public partial class SignUpPage : ContentPage, IFileConnector, IRestConnector
    {
		DataClass dataClass = DataClass.GetInstance;
		bool isClicked;
		CancellationTokenSource cts;
#if DEBUG
		FileReader fileReader = FileReader.GetInstance;
		int loginType;
		string email, image, name;
#else
        RestServices restService = new RestServices();
#endif
		public SignUpPage()
        {
            InitializeComponent();
        }

		protected override void OnAppearing()
		{
			base.OnAppearing();
#if DEBUG
			fileReader.FileReaderDelegate = this;
#else
            restService.WebServiceDelegate = this;
#endif
            isClicked = false;

			DependencyService.Get<IStatusStyle>().StatusStyle(1);
		}

		protected override void OnDisappearing()
		{
			base.OnDisappearing();
		}

		void OnEntry_Completed(object sender, EventArgs e)
		{
            switch(((CustomEntry)sender).ClassId)
            {
                case "1": emailEntry.Focus(); break;
                case "2": passwordEntry.Focus(); break;  
                case "3": confirmPasswordEntry.Focus(); break;    
            }
		}

        void OnBackButton_Clicked(object sender, EventArgs e)
        {
            Navigation.PopAsync();
        }

		public async Task<Stream> ImageSteam(string url)
		{
			HttpClient client = new HttpClient();
			client.MaxResponseContentBufferSize = 256000;
			Stream stream = await client.GetStreamAsync(url);
			return stream;
		}

		void OnFacebook_Clicked(object sender, EventArgs e)
		{
            if (!isClicked)
            {
                isClicked = true;
                DependencyService.Get<IFacebookManager>().Login(OnFacebookLoginComplete);
            }
		}

		async void OnFacebookLoginComplete(FacebookUser facebookUser, string message)
		{
			if (facebookUser != null)
			{
				await Task.Run(async () =>
				{
					cts = new CancellationTokenSource();

                    try
                    {
#if DEBUG
                        loginType = 1;
                        email = facebookUser.Email;
                        image = facebookUser.Picture;
                        name = facebookUser.Name;
                        await fileReader.ReadFile("User.json", cts.Token);
#else
                        var json = JsonConvert.SerializeObject(new { user = new { name = facebookUser.Name, email = facebookUser.Email, image = facebookUser.Picture, provider_id = facebookUser.Id }, device = new { platform = App.DeviceType, token = App.UDID } });
                        Stream imageStream = await ImageSteam(facebookUser.Picture);
                        await restService.MultiPartDataContentAsync(Constants.SOCIAL_REGISTER, "user", json, imageStream, cts.Token, RestMethod.Post, "device", "image");
#endif
					}
					catch (OperationCanceledException ex)
					{
						App.Log(ex.StackTrace + ex.Message, "FacebookSignUp");
                        isClicked = false;
					}
					catch (Exception ex)
					{
						App.Log(ex.StackTrace + ex.Message, "FacebookSignUp");
                        isClicked = false;
					}

					cts = null;
				});
			}
			else
			{
				App.Log(message, "FacebookSignUp");
                isClicked = false;
			}
		}

		void OnTwitter_Clicked(object sender, EventArgs e)
		{
            if (!isClicked)
            {
                isClicked = true;
                DependencyService.Get<ITwitter>().Login(OnTwitterLoginComplete);
            }
		}

		async void OnTwitterLoginComplete(JObject twitterUser, string message)
		{
			if (twitterUser != null)
			{
				await Task.Run(async () =>
				{
					cts = new CancellationTokenSource();

					try
					{
#if DEBUG
						loginType = 1;
						email = twitterUser["email"].ToString();
                        image = twitterUser["profile_image_url"].ToString();
                        name = twitterUser["name"].ToString();
						await fileReader.ReadFile("User.json", cts.Token);

#else
                        var json = JsonConvert.SerializeObject(new { user = new { name = twitterUser["name"].ToString(), email = twitterUser["email"].ToString(), image = twitterUser["profile_image_url"].ToString(), provider_id = twitterUser["id_str"].ToString() }, device = new { platform = App.DeviceType, token = App.UDID } });
                        Stream imageStream = await ImageSteam(twitterUser["profile_image_url"].ToString());
                        await restService.MultiPartDataContentAsync(Constants.SOCIAL_REGISTER, "user", json, imageStream, cts.Token, RestMethod.Post, "device", "image");
#endif
					}
					catch (OperationCanceledException ex)
					{
						App.Log(ex.StackTrace + ex.Message, "TwitterSignUp");
                        isClicked = false;
					}
					catch (Exception ex)
					{
						App.Log(ex.StackTrace + ex.Message, "TwitterSignUp");
                        isClicked = false;
					}

					cts = null;
				});
			}
			else
			{
				App.Log(message, "TwitterSignUp");
                isClicked = false;
			}
		}

		void DisplayError(Entry entry, string error)
		{
			entry.Text = String.Empty;
			entry.Placeholder = error;
			entry.PlaceholderColor = Color.Red;
		}

        async void OnSignUp_Clicked(object sender, EventArgs e)
        {
			bool isNotError = true;

			if (String.IsNullOrEmpty(emailEntry.Text))
			{
				DisplayError(emailEntry, "Email is required!");
				isNotError = false;
			}

			if (String.IsNullOrEmpty(passwordEntry.Text))
			{
				DisplayError(passwordEntry, "Password is required!");
				isNotError = false;
			}
            else
            {
                if(!passwordEntry.Text.Equals(confirmPasswordEntry.Text))
                {
					DisplayError(passwordEntry, "Password didn't match!");
					isNotError = false;
                }
            }

			if (isNotError && isClicked == false)
			{
				isClicked = true;

				await Task.Run(async () =>
				{
					cts = new CancellationTokenSource();

					try
					{
#if DEBUG
                        loginType = 0;
                        await fileReader.ReadFile("User.json", cts.Token);
#else
                        var json = JsonConvert.SerializeObject(new { user = new { email = emailEntry.Text, password = passwordEntry.Text } });
                        await restService.PostRequestAsync(Constants.REGISTER, json, cts.Token);
#endif
					}
					catch (OperationCanceledException ex)
					{
						App.Log(ex.StackTrace + ex.Message, "SignUpPage");
                        isClicked = false;
					}
					catch (Exception ex)
					{
						App.Log(ex.StackTrace + ex.Message, "SignUpPage");
                        isClicked = false;
					}

					cts = null;
				});
			}
        }

		public void ReceiveJSONData(JObject jsonData, CancellationToken ct)
		{
            Device.BeginInvokeOnMainThread(() => {
                if(int.Parse(jsonData["status"].ToString()) == 200){
                    dataClass.user = JsonConvert.DeserializeObject<User>(jsonData["user"].ToString());
                    dataClass.token = jsonData["user"]["token"].ToString();


#if DEBUG
                  switch (loginType)
                  {
                      case 0:
                          dataClass.user.email = emailEntry.Text;
                          break;
                      case 1:
                          dataClass.user.email = email;
                            dataClass.user.image = image;
                            dataClass.user.name = name;
                          break;
                  }
#endif


                    dataClass.Update(false);
                    App.ShowMainPage();
                }else{
                    var errors = jsonData["errors"];
                    if (!string.IsNullOrEmpty(errors["email"].ToString()))
                    {
                        DisplayError(this.emailEntry, errors["email"].ToString());
                    }
                    if (!string.IsNullOrEmpty(errors["password"].ToString()))
                    {
                        this.passwordEntry.FontSize = 11.scale();
                        DisplayError(this.passwordEntry, errors["password"].ToString());
                    }
                }
                isClicked = false;
            });
		}

		public void ReceiveTimeoutError(string title, string error)
		{
			Device.BeginInvokeOnMainThread(async () =>
			{
				await DisplayAlert(title, error, "Okay");
				isClicked = false;
			});
		}
    }
}
