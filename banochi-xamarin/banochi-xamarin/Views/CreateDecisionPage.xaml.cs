﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;

namespace banochixamarin
{
    public partial class CreateDecisionPage : RootViewPage, IFileConnector
    {
		public static readonly BindableProperty HomeTypeProperty = BindableProperty.Create(nameof(HomeType), typeof(string), typeof(HomePage), string.Empty);
		public string HomeType
		{
			get { return (string)GetValue(HomeTypeProperty); }
			set { SetValue(HomeTypeProperty, value); }
		}

		ObservableCollection<CriteriaModel> userCriteria = new ObservableCollection<CriteriaModel>();
		ObservableCollection<CriteriaModel> selectedCriteria = new ObservableCollection<CriteriaModel>();
		DataClass dataClass = DataClass.GetInstance;
		bool isClicked, isTapped;
        int pageType;
        UserDecision userDecision = new UserDecision();
        CancellationTokenSource cts;
        FileReader fileReader = FileReader.GetInstance;

        //0 - Create : 1- Edit
        public CreateDecisionPage(UserDecision userDecision, int pageType)
        {
            InitializeComponent();

            this.pageType = pageType;

            switch (this.pageType)
            {
                case 0:
                    this.PageTitle = "CREATE DECISION";
                    this.userDecision = userDecision;
					this.RightIcon2 = "CloseLight";
					this.RightButton2Command = new Command((obj) => { Navigation.PopModalAsync(); });
                    break;
                case 1:
					this.PageTitle = "EDIT DECISION";
					this.userDecision.Update(userDecision);
                    this.LeftIcon = "BackLight";
                    this.LeftButtonCommand = new Command((obj) => { Navigation.PopAsync(); });
                    break;
            }

            GetData();
        }

		protected override void OnAppearing()
		{
			base.OnAppearing();

            DependencyService.Get<IStatusStyle>().StatusStyle(0);

			isClicked = isTapped = false;
		}

		protected override void OnDisappearing()
		{
			base.OnDisappearing();
		}

        async void GetData()
        {
            fileReader.FileReaderDelegate = this;

			cts = new CancellationTokenSource();

			try
			{
                await fileReader.ReadFile("Criteria.json", cts.Token);
			}
			catch (OperationCanceledException ex)
			{
				App.Log(ex.StackTrace + ex.Message, "GetCriteria");
			}
			catch (Exception ex)
			{
				App.Log(ex.StackTrace + ex.Message, "GetCriteria");
			}

			cts = null;
        }

		void OnCriteria_ItemTapped(object sender, ItemTappedEventArgs e)
		{
			criteriaSelection.SelectedItem = null;
		}

		void OnSwitch_Tapped(object sender, EventArgs e)
		{
            if(!isTapped)
            {
                isTapped = true;
				if (selectedCriteria.Count() < 8 || ((CustomSwitch)sender).IsToggled == 1)
				{
					((CustomSwitch)sender).IsToggled = ((CustomSwitch)sender).IsToggled ^ 1;
					userCriteria.FirstOrDefault(x => x.id == int.Parse(((CustomSwitch)sender).ClassId)).is_selected = ((CustomSwitch)sender).IsToggled;
					switch (((CustomSwitch)sender).IsToggled)
					{
						case 0:
							if (selectedCriteria.ToList().Exists(x => x.id == int.Parse(((CustomSwitch)sender).ClassId)))
							{
								selectedCriteria.Remove(selectedCriteria.FirstOrDefault(x => x.id == int.Parse(((CustomSwitch)sender).ClassId)));
							}
							break;
						case 1:
							selectedCriteria.Add(userCriteria.FirstOrDefault(x => x.id == int.Parse(((CustomSwitch)sender).ClassId)));
							break;
					}
				}
				else
				{
					DisplayAlert("Criteria Limit", "You've reach the maximum limit of criteria selected!", "Okay");
				}
                isTapped = false;
            }
		}

        void  DisplayError(Entry entry, string error)
		{
			entry.Text = String.Empty;
			entry.Placeholder = error;
			entry.PlaceholderColor = Color.Red;
		}

        void OnNext_Clicked(object sender, EventArgs e)
        {
            bool isNotError = true;

            if (String.IsNullOrEmpty(homeType.Text) || homeType.Text.Equals("Home type is required") ||homeType.Text.Equals("Select a home type"))
            {
				this.ClassId = "1";
				isNotError = false;
			}

			if (String.IsNullOrEmpty(zipCodeEntry.Text))
			{
				DisplayError(zipCodeEntry, "Zip Code is required!");
				isNotError = false;
                customScroll.ScrollToAsync( 0,0,true);
			}

            if(!ValidateZipCode(zipCodeEntry.Text)){
                DisplayError(zipCodeEntry, "Zip Code is invalid!");
                isNotError = false;
                customScroll.ScrollToAsync(0, 0, true);
            }

			if (isNotError && isClicked == false)
			{
				isClicked = true;

				if (selectedCriteria.Any())
				{
                    if (this.pageType == 0)
                    {
                        this.userDecision = new UserDecision() { id = dataClass.inProgressDecisions.Count + dataClass.savedResults.Count + 1, home_type = homeType.Text, zip_code = zipCodeEntry.Text, date_created = DateTime.Now.ToString("d"), criteriaSelected = selectedCriteria, decisionResult = new ObservableCollection<DecisionResult>(), userOptions = new ObservableCollection<DecisionOption>() };
                    }
                    else
                    {
						this.userDecision.home_type = this.HomeType;
						this.userDecision.zip_code = zipCodeEntry.Text;
                    }

					Navigation.PushAsync(new ImportancePage(this.userDecision, pageType));
				}
				else
				{
					DisplayAlert("Criteria Limit", "Minimum of 1 criteria must be selected!", "Okay");
					isClicked = false;
				}
			}
		}

        protected bool ValidateZipCode(string zipCode)
        {
            string pattern = @"^(?(^00000(|-0000))|(\d{5}(|-\d{4})))$";// @"^\d{5}(?:[-\s]\d{4})?$";//^\d{5}(-\d{4})?$
            var regex = new Regex(pattern);
            System.Diagnostics.Debug.WriteLine("V", regex.IsMatch(zipCode).ToString());
            return regex.IsMatch(zipCode);
        }

		async void OnDropDown_Tapped(object sender, EventArgs e)
		{
			var myAction = await DisplayActionSheet(null, "Cancel", null, "Single", "Family", "Town House", "Condominium");
            if (myAction != null)
            {
                if (myAction != "Cancel")
                {
                    this.HomeType = myAction;
                    this.ClassId = "2";
                }
            }
            else
            {
                this.ClassId = "0";
            }
		}

        public void ReceiveJSONData(JObject jsonData, CancellationToken ct)
        {
            if (int.Parse(jsonData["status"].ToString()) == 200)
            {
                userCriteria = JsonConvert.DeserializeObject<ObservableCollection<CriteriaModel>>(jsonData["criteria"].ToString());

				if (this.pageType == 1)
				{
					selectedCriteria = this.userDecision.criteriaSelected;
					foreach (var item in this.userDecision.criteriaSelected)
					{
						userCriteria.FirstOrDefault(x => x.id == item.id).criteria_value = item.criteria_value;
						userCriteria.FirstOrDefault(x => x.id == item.id).is_selected = item.is_selected;
					}

					this.HomeType = this.userDecision.home_type;
					this.ClassId = "2";
					zipCodeEntry.Text = this.userDecision.zip_code;
				}

				criteriaSelection.ItemsSource = userCriteria;
				criteriaSelection.HeightRequest = userCriteria.Count * 50.ScaleHeight();
            }
        }

        public void ReceiveTimeoutError(string title, string error)
        {
            DisplayAlert(title, error, "Okay");
        }
    }
}
