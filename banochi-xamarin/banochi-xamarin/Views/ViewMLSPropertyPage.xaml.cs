﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using banochixamarin.Models;
using CarouselView.FormsPlugin.Abstractions;
using Xamarin.Forms;

namespace banochixamarin
{
    public partial class ViewMLSPropertyPage : RootViewPage
    {
        List<SimplyRetsOtherInfoModel> category = new List<SimplyRetsOtherInfoModel>();
        SimplyRetsModel mlsProperty;
        ContentView contentView;
        bool hasPopUp;

        public ViewMLSPropertyPage(SimplyRetsModel mlsProperty)
        {
            InitializeComponent();

            this.PageTitle = mlsProperty.mlsId.ToString();
			this.LeftIcon = "BackLight";
            this.LeftButtonCommand = new Command((obj) => { if(!hasPopUp) { Navigation.PopAsync(); } });

            this.mlsProperty = mlsProperty;
            BindingContext = this.mlsProperty;

            category.Add(new SimplyRetsOtherInfoModel("Location", "Address"));
            category.Add(new SimplyRetsOtherInfoModel("Taxes","Tax"));
            category.Add(new SimplyRetsOtherInfoModel("Garage","Parking"));
            category.Add(new SimplyRetsOtherInfoModel("School","School"));//==> Change Icon
            category.Add(new SimplyRetsOtherInfoModel("Association","Association"));//==> Change Icon
            category.Add(new SimplyRetsOtherInfoModel("MLSIcon","Mls"));
            category.Add(new SimplyRetsOtherInfoModel("HomeSize","Office"));
            category.Add(new SimplyRetsOtherInfoModel("Agent","Agent"));//==> Change Icon
            category.Add(new SimplyRetsOtherInfoModel("Price","Sales"));
            otherInfo.ItemsSource = category;
        }

		protected override void OnAppearing()
		{
			base.OnAppearing();

            hasPopUp = false;

			DependencyService.Get<IStatusStyle>().StatusStyle(0);
		}

		protected override void OnDisappearing()
		{
			base.OnDisappearing();
		}

		void OnOtherInfo_ItemTapped(object sender, ItemTappedEventArgs e)
		{
			otherInfo.SelectedItem = null;

            switch((e.Item as SimplyRetsOtherInfoModel).title)
            {
                case "Address": contentView = new AddressView(this.mlsProperty.address); break;
                case "Tax": contentView = new TaxView(this.mlsProperty.tax); break;
                case "Parking": break;
                case "School": break;
                case "Association": break;
                case "Mls": break;
                case "Office": break;
                case "Agent": break;
                case "Sales": break;
            }

            if(contentView != null)
            {
				var tapGestureRecognizer = new TapGestureRecognizer();
				tapGestureRecognizer.Tapped += ContentView_TapGestureRecognizer;
				contentView.GestureRecognizers.Add(tapGestureRecognizer);

				parentLayout.Children.Add(contentView);

                hasPopUp = true;
                this.LeftIcon = null;
				this.TitleFontColor = Constants.LABEL_BLACK;
				this.NavBackgroundColor = Color.Transparent;
            }
		}

        void ContentView_TapGestureRecognizer(object sender, EventArgs e)
        {
            parentLayout.Children.Remove(contentView);
            contentView = null;

            hasPopUp = false;
            this.LeftIcon = "BackLight";
            this.TitleFontColor = Constants.BUTTON_TEXTCOLOR_WHITE;
            this.NavBackgroundColor = Constants.NAVIGATION_BACKGROUND_BLUE;
        }
    }
}
