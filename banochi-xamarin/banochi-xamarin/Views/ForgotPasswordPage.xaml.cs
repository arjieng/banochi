﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;

namespace banochixamarin
{
    public partial class ForgotPasswordPage : RootViewPage, IRestConnector
    {
        bool isClicked;

#if DEBUG == false
        RestServices restService = new RestServices();
        CancellationTokenSource cts;
#endif

		public ForgotPasswordPage()
        {
            InitializeComponent();

            this.PageTitle = "FORGOT PASSWORD";
			this.RightIcon2 = "CloseLight";
			this.RightButton2Command = new Command((obj) => { Navigation.PopModalAsync(); });

#if DEBUG == false
            restService.WebServiceDelegate = this;
#endif
		}

        protected override void OnAppearing()
		{
			base.OnAppearing();

            isClicked = false;

			DependencyService.Get<IStatusStyle>().StatusStyle(0);
		}

		protected override void OnDisappearing()
		{
			base.OnDisappearing();
		}

		void DisplayError(Entry entry, string error)
		{
			entry.Text = String.Empty;
			entry.Placeholder = error;
			entry.PlaceholderColor = Color.Red;
		}

        async void OnSend_Clicked(object sender, EventArgs e)
        {
            bool isNotError = true;

            if (String.IsNullOrEmpty(emailEntry.Text))
            {
                DisplayError(emailEntry, "Email is required!");
                isNotError = false;
            }

            if (isNotError && isClicked == false)
            {
                isClicked = true;
#if DEBUG
                isClicked = false;
                DisplayAlert("Forgot Password", "Email Sent successfully", "Okay");
                Navigation.PopModalAsync();
#else
                await Task.Run(async () =>
                {
                    cts = new CancellationTokenSource();

                    try
                    {
                        string json = JsonConvert.SerializeObject(new { email = emailEntry.Text });
                        await restService.PostRequestAsync(Constants.FORGOT_PASSWORD, json, cts.Token);
                    } 
                    catch (OperationCanceledException ex)
                    {
                        App.Log(ex.StackTrace + ex.Message, "ForgotPassword");
                        isClicked = false;
                    }
                    catch (Exception ex)
                    {
                        App.Log(ex.StackTrace + ex.Message, "ForgotPassword");
                        isClicked = false;
                    }
                });
#endif
			}
        }

		public void ReceiveJSONData(JObject jsonData, CancellationToken ct)
		{
            Device.BeginInvokeOnMainThread(() =>
            {
                if (int.Parse(jsonData["status"].ToString()) == 200)
                {
                    DisplayAlert("Forgot Password", "Email Sent successfully", "Okay");
                    Navigation.PopModalAsync();
                }
                else
                {
                    DisplayError(emailEntry, jsonData["error"].ToString());
                }

                isClicked = false;
            });
		}

		public void ReceiveTimeoutError(string title, string error)
		{
			DisplayAlert(title, error, "Okay");
			isClicked = false;
		}
    }
}
