﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Refractored.XamForms.PullToRefresh;
using Xamarin.Forms;

namespace banochixamarin
{
    public partial class HomePage : ContentPage, IRestConnector
    {
        UserDecision deleteItem;
        DataClass dataClass = DataClass.GetInstance;
        bool isOpen, isClicked, isRefreshing;
        public uint FabAnimTime = 250;
        int requestType = 0;

#if DEBUG == false
        CancellationTokenSource cts;
        RestServices restService = new RestServices();
#endif

        public HomePage()
        {
            InitializeComponent();

            MessagingCenter.Unsubscribe<Object, string>(this, "ResetPassword");

            inProgressList.ItemsSource = dataClass.inProgressDecisions;
            savedResultList.ItemsSource = dataClass.savedResults;

            refreshViewSavedResult.RefreshCommand = new Command(RefreshCommandResults);
            refreshViewInProgress.RefreshCommand = new Command(RefreshCommandInProgress);

        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            DependencyService.Get<IStatusStyle>().StatusStyle(0);
#if DEBUG == false
            restService.WebServiceDelegate = this;
#endif
            isOpen = isClicked = isRefreshing = false;

            inProgressList.ItemsSource = dataClass.inProgressDecisions;
            savedResultList.ItemsSource = dataClass.savedResults;
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            CloseDrawer(false);
            isRefreshing = false;
        }

        async void RefreshCommandResults()
        {
            refreshViewSavedResult.IsRefreshing = true;
            CloseDrawer(true);
#if DEBUG == false
            requestType = 1;
            await Task.Run(async () =>
            {
                cts = new CancellationTokenSource();
                var json = JsonConvert.SerializeObject(new { token = dataClass.token });
                await restService.PostRequestAsync(Constants.RELOAD_DECISIONS, json, cts.Token);
            });

#endif
            isRefreshing = false;
            refreshViewSavedResult.IsRefreshing = false;
        }

        async void RefreshCommandInProgress()
        {
            refreshViewInProgress.IsRefreshing = true;
            CloseDrawer(true);
#if DEBUG == false
            requestType = 1;
            await Task.Run(async () =>
            {
                cts = new CancellationTokenSource();
                var json = JsonConvert.SerializeObject(new { token = dataClass.token });
                await restService.PostRequestAsync(Constants.RELOAD_DECISIONS, json, cts.Token);
            });
#endif
            isRefreshing = false;
            refreshViewInProgress.IsRefreshing = false;
        }

        void CloseDrawer(bool setItemSource)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                if (!isRefreshing)
                {
                    isRefreshing = true;

                    if (dataClass.inProgressDecisions.ToList().Exists(x => x.is_swiped == true))
                    {
                        dataClass.inProgressDecisions.FirstOrDefault(x => x.is_swiped == true).is_swiped = false;
                    }

                    if (dataClass.savedResults.ToList().Exists(x => x.is_swiped == true))
                    {
                        dataClass.savedResults.FirstOrDefault(x => x.is_swiped == true).is_swiped = false;
                    }

                    inProgressList.ItemsSource = null;
                    savedResultList.ItemsSource = null;

                    if (setItemSource)
                    {
                        inProgressList.ItemsSource = dataClass.inProgressDecisions;
                        savedResultList.ItemsSource = dataClass.savedResults;
                    }
                }
            });
        }

        void OnButton_Clicked(object sender, EventArgs e)
        {
            if (!isClicked)
            {
                isClicked = true;
                this.ClassId = ((ControlTemplateButton)sender).ClassId;
                CloseDrawer(true);
                isRefreshing = false;
                isClicked = false;
            }
        }

        void ItemOptions_Clicked(object sender, System.EventArgs e)
        {
            if (!isClicked)
            {
                isClicked = true;
                Button button = sender as Button;
                var item = button.CommandParameter as UserDecision;
                DecisionListAction(button.ClassId, item);
            }
        }

        void OptionItem_Tapped(object sender, object e)
        {
            if (!isClicked)
            {
                isClicked = true;
                GestureStackLayout button = sender as GestureStackLayout;
                var item = e as UserDecision;
                DecisionListAction(button.ClassId, item);
            }
        }

        async void DecisionListAction(string classid, UserDecision decisionItem)
        {
            switch (classid)
            {
                case "0"://Edit
                    Navigation.PushAsync(new CreateDecisionPage(decisionItem, 1));
                    break;
                case "1"://Remove
                    requestType = 0;
                    deleteItem = decisionItem;
#if DEBUG
                    OnDeleteSuccessWS();
#else
                    await Task.Run(async () =>
                    {
                        cts = new CancellationTokenSource();
                        var json = JsonConvert.SerializeObject(new { token = dataClass.token, id = decisionItem.id });
                        await restService.PostRequestAsync(Constants.DELETE_DECISION, json, cts.Token);
                    });

#endif

                    break;
                case "2"://View
                    switch (this.ClassId)
                    {
                        case "0":
                            Navigation.PushAsync(new ViewResultPage(decisionItem, 1));
                            break;
                        case "1":
                            Navigation.PushAsync(new CreateDecisionPage(decisionItem, 1));
                            break;
                    }
                    break;
            }
        }

        void OnDeleteSuccessWS()
        {
            if (deleteItem != null)
            {
                switch (this.ClassId)
                {
                    case "0":
                        for (int i = 0; i < dataClass.savedResults.Count; i++)
                        {
                            if (deleteItem.id == dataClass.savedResults[i].id)
                            {
                                // Xamarin Forms Bug : Bug 56793 - ListView with uneven row height not updating layout if items change size after item has been deleted
                                // https://bugzilla.xamarin.com/show_bug.cgi?id=56793
                                // Bug in Android: Cannot swipe left and right after one item is deleted because swiping involves changing of layout position.
                                // Workaround: Set ItemSource to null, remove the item and restore the source.

                                switch (Device.RuntimePlatform)
                                {
                                    case Device.iOS:
                                        dataClass.savedResults.Remove(deleteItem);
                                        break;
                                    case Device.Android:
                                        savedResultList.ItemsSource = null;
                                        dataClass.savedResults.Remove(deleteItem);
                                        //dataClass.savedResults.DeleteReference.Invoke(this, null);
                                        savedResultList.ItemsSource = dataClass.savedResults;
                                        break;
                                }
                            }
                        }
                        break;
                    case "1":
                        for (int i = 0; i < dataClass.inProgressDecisions.Count; i++)
                        {
                            if (deleteItem.id == dataClass.inProgressDecisions[i].id)
                            {
                                // Xamarin Forms Bug : Bug 56793 - ListView with uneven row height not updating layout if items change size after item has been deleted
                                // https://bugzilla.xamarin.com/show_bug.cgi?id=56793
                                // Bug in Android: Cannot swipe left and right after one item is deleted because swiping involves changing of layout position.
                                // Workaround: Set ItemSource to null, remove the item and restore the source.

                                switch (Device.RuntimePlatform)
                                {
                                    case Device.iOS:
                                        dataClass.inProgressDecisions.Remove(deleteItem);
                                        break;
                                    case Device.Android:
                                        inProgressList.ItemsSource = null;
                                        dataClass.inProgressDecisions.Remove(deleteItem);
                                        //dataClass.inProgressDecisions.DeleteReference.Invoke(this, null);
                                        inProgressList.ItemsSource = dataClass.inProgressDecisions;
                                        break;
                                }
                            }
                        }
                        break;
                }
            }
            deleteItem = null;
            isClicked = false;
        }

        void OnLast_ItemAppearing(object sender, Xamarin.Forms.ItemVisibilityEventArgs e)
        {
            //Pagination
        }

        void OnFabPlus_Tapped(object sender, EventArgs e)
        {
            if (!isClicked)
            {
                isClicked = true;
                NavigationPage nav = new NavigationPage(new CreateDecisionPage(new UserDecision(), 0));
                Navigation.PushModalAsync(nav);
                Close_Fab(0);
            }
        }

        void OnFabProfile_Tapped(object sender, EventArgs e)
        {
            if (!isClicked)
            {
                isClicked = true;
                Navigation.PushModalAsync(new ProfilePage());
                Close_Fab(0);
            }
        }

        async void OnFabLogOut_Tapped(object sender, EventArgs e)
        {
            if (!isClicked)
            {
                isClicked = true;
                await dataClass.Logout();
                App.Logout();
            }
        }

        void Close_Fab(uint delayTime)
        {
            this.StyleId = "0";
            isOpen = false;
            fabLogOut.TranslateTo(0, 0, delayTime, Easing.CubicOut);
            fabProfile.TranslateTo(0, 0, delayTime, Easing.CubicOut);
            fabPlus.TranslateTo(0, 0, delayTime, Easing.CubicOut);
        }

        void OnFabMenu_TappedAsync(object sender, EventArgs e)
        {
            if (!isOpen)
            {
                this.StyleId = "1";
                isOpen = true;
                fabLogOut.TranslateTo(0, -70.ScaleHeight(), FabAnimTime, Easing.CubicOut);
                fabProfile.TranslateTo(0, -140.ScaleHeight(), FabAnimTime, Easing.CubicOut);
                fabPlus.TranslateTo(0, -210.ScaleHeight(), FabAnimTime, Easing.CubicOut);
            }
            else
            {
                Close_Fab(FabAnimTime);
            }
        }

        public void ReceiveJSONData(JObject jsonData, CancellationToken ct)
        {
            Device.BeginInvokeOnMainThread(() => {
                isClicked = false;

                if (int.Parse(jsonData["status"].ToString()) == 200)
                {
                    switch (requestType)
                    {
                        case 0:
                            OnDeleteSuccessWS();
                            break;
                        case 1:
                            dataClass.inProgressDecisions = JsonConvert.DeserializeObject<System.Collections.ObjectModel.ObservableCollection<UserDecision>>(jsonData["decisions"]["progress"].ToString());
                            dataClass.savedResults = JsonConvert.DeserializeObject<System.Collections.ObjectModel.ObservableCollection<UserDecision>>(jsonData["decisions"]["saved"].ToString());

                            inProgressList.ItemsSource = null;
                            savedResultList.ItemsSource = null;

                            inProgressList.ItemsSource = dataClass.inProgressDecisions;
                            savedResultList.ItemsSource = dataClass.savedResults;

                            dataClass.Update(false);
                            break;
                    }
                }
            });
        }

        public void ReceiveTimeoutError(string title, string error)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                await DisplayAlert(title, error, "Okay");
                isClicked = false;
            });
        }
    }
}