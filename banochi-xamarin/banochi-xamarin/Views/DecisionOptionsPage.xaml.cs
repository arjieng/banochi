﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;

namespace banochixamarin
{
    public partial class DecisionOptionsPage : RootViewPage, IRestConnector
    {
        DecisionOption deleteItem;
        DataClass dataClass = DataClass.GetInstance;
        UserDecision userDecision;
        bool isClicked;
        int reqType = 0;
#if DEBUG == false
        CancellationTokenSource cts;
        RestServices restService = new RestServices();
#endif

        public DecisionOptionsPage(UserDecision userDecision)
        {
            InitializeComponent();

            this.userDecision = userDecision;

            this.PageTitle = this.userDecision.home_type.ToUpper();
            refreshViewOptions.RefreshCommand = new Command(PullRefreshOptionss);

            this.RightIcon1 = "PlusLight";
            this.RightButton1Command = new Command((obj) =>
            {
                if (!isClicked)
                {
                    isClicked = true;
                    if (userDecision.userOptions.Count < 8)
                    {
                        Navigation.PushAsync(new AddOptionPage(0, userDecision, 0));
                    }
                    else
                    {
                        DisplayAlert("Option Limit", "You've reach the maximum limit of Options!", "Okay");
                        isClicked = false;
                    }
                }
            });

            this.RightIcon2 = "CloseLight";
            this.RightButton2Command = new Command((obj) => { App.ShowMainPage(); });
        }

        private async void PullRefreshOptionss(object obj)
        {
            reqType = 1;
            refreshViewOptions.IsRefreshing = true;
#if DEBUG == false
            await Task.Run(async () =>
            {
                cts = new CancellationTokenSource();
                var json = JsonConvert.SerializeObject(new { token = dataClass.token, id = userDecision.id });
                await restService.PostRequestAsync(Constants.RELOAD_OPTIONS, json, cts.Token);
            });

#endif
            refreshViewOptions.IsRefreshing = false;
        
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

#if DEBUG == false
            restService.WebServiceDelegate = this;
#endif
            DependencyService.Get<IStatusStyle>().StatusStyle(0);

            isClicked = false;

            CloseDrawer();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }

        void CloseDrawer()
        {
            if (this.userDecision.userOptions.ToList().Exists(x => x.is_swiped == true))
            {
                this.userDecision.userOptions.FirstOrDefault(x => x.is_swiped == true).is_swiped = false;
            }
            optionsList.ItemsSource = null;
            optionsList.ItemsSource = dataClass.decisionOptions = this.userDecision.userOptions;
        }

        void ItemOptions_Clicked(object sender, System.EventArgs e)
        {
            if (!isClicked)
            {
                isClicked = true;
                Button button = sender as Button;
                var item = button.CommandParameter as DecisionOption;
                DecisionListAction(button.ClassId, item);
            }
        }

        void OptionItem_Tapped(object sender, object e)
        {
            if (!isClicked)
            {
                isClicked = true;
                GestureStackLayout button = sender as GestureStackLayout;
                var item = e as DecisionOption;
                DecisionListAction(button.ClassId, item);
            }
        }

        async void DecisionListAction(string classid, DecisionOption decisionItem)
        {
            switch (classid)
            {
                case "0"://Edit

                    break;
                case "1"://Remove
                    deleteItem = decisionItem;
                    reqType = 0;
#if DEBUG == false
                    await Task.Run(async () =>
                    {
                        cts = new CancellationTokenSource();
                        var json = JsonConvert.SerializeObject(new { token = dataClass.token, id = decisionItem.id });
                        await restService.PostRequestAsync(Constants.DELETE_OPTION, json, cts.Token);
                    });
#else
                    OnDeleteSuccessWS();
#endif
					break;
				case "2"://View
                    await Navigation.PushAsync(new AddOptionPage(1, userDecision, decisionItem.id));
					break;
			}
		}

		void OnDeleteSuccessWS()
		{
			if (deleteItem != null)
			{
				for (int i = 0; i < dataClass.decisionOptions.Count; i++)
				{
					if (deleteItem.id == dataClass.decisionOptions[i].id)
					{
						// Xamarin Forms Bug : Bug 56793 - ListView with uneven row height not updating layout if items change size after item has been deleted
						// https://bugzilla.xamarin.com/show_bug.cgi?id=56793
						// Bug in Android: Cannot swipe left and right after one item is deleted because swiping involves changing of layout position.
						// Workaround: Set ItemSource to null, remove the item and restore the source.

						switch (Device.RuntimePlatform)
						{
							case Device.iOS:
								dataClass.decisionOptions.Remove(deleteItem);
								break;
							case Device.Android:
								optionsList.ItemsSource = null;
								dataClass.decisionOptions.Remove(deleteItem);
								//dataClass.decisionOptions.DeleteReference.Invoke(this, null);
								optionsList.ItemsSource = dataClass.decisionOptions;
								break;
						}
					}
				}
			}

            isClicked = false;
			deleteItem = null;
		}

        void OnLast_ItemAppearing(object sender, Xamarin.Forms.ItemVisibilityEventArgs e)
        {

        }

		void OnShowResult_Clicked(object sender, EventArgs e)
		{
            if (!isClicked)
            {
                isClicked = true;
                if (userDecision.userOptions.Count > 1)
                {
                    Navigation.PushModalAsync(new ViewResultPage(userDecision, 0));
                }
                else
                {
                    DisplayAlert("Option Limit", "You must add atleast two options to show result!", "Okay");
                    isClicked = false;
                }
            }
		}

        public void ReceiveJSONData(JObject jsonData, CancellationToken ct)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                isClicked = false;
                if (int.Parse(jsonData["status"].ToString()) == 200)
                {
                    switch(reqType){
                        case 0:
                            OnDeleteSuccessWS();
                            break;
                        case 1:
                            //userDecision.userOptions = JsonConvert.DeserializeObject<ObservableCollection<DecisionOption>>(jsonData["options"].ToString());
                            //optionsList.ItemsSource = null;
                            //optionsList.ItemsSource = userDecision.userOptions;

                            var inprogress = dataClass.inProgressDecisions.FirstOrDefault(x => x.id == userDecision.id);
                            var saved = dataClass.savedResults.FirstOrDefault(x => x.id == userDecision.id);
                            if(inprogress != null){
                                ObservableCollection<DecisionOption> result = JsonConvert.DeserializeObject<ObservableCollection<DecisionOption>>(jsonData["options"].ToString());
                                userDecision.userOptions = result;
                                inprogress.Update(userDecision);
                            }
                            if (saved != null)
                            {
                                ObservableCollection<DecisionOption> result = JsonConvert.DeserializeObject<ObservableCollection<DecisionOption>>(jsonData["options"].ToString());
                                userDecision.userOptions = result;
                                inprogress.Update(userDecision);
                            }
                            break;
                    }
                }
            });
        }

        public void ReceiveTimeoutError(string title, string error)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                await DisplayAlert(title, error, "Okay");
                isClicked = false;
            });
        }
    }
}
