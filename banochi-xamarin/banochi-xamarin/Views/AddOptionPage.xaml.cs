﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;

namespace banochixamarin
{
    public partial class AddOptionPage : RootViewPage, ISimplyRetsConnector, IRestConnector
    {
        SimplyRetsHelper simplyRetsHelper = new SimplyRetsHelper();
        DataClass dataClass = DataClass.GetInstance;
        ObservableCollection<CriteriaModel> selectedCriteria = new ObservableCollection<CriteriaModel>();
        UserDecision userDecision;
        bool isClicked, hasMLS, fromMenu;
        SimplyRetsModel mlsProperty;
        CancellationTokenSource cts;
        int _pageType, optionId;
        DecisionOption decisionOption = new DecisionOption();
#if DEBUG == false
        RestServices restService = new RestServices();
#endif
        //pageType: 0 - Add Option to Decision Options , 1 - Edit , 3 - Add Option to Result
        public AddOptionPage(int pageType, UserDecision userDecision, int optionId)
        {
            InitializeComponent();

            this._pageType = pageType;
            this.optionId = optionId;
            this.userDecision = userDecision;
            this.PageTitle = "ADD OPTION";
            this.LeftIcon = "BackLight";
            this.LeftButtonCommand = new Command((obj) => { Navigation.PopAsync(); });
            this.RightIcon1 = "ListLight";
            this.RightButton1Command = new Command((obj) =>
            {
                if (!isClicked)
                {
                    isClicked = true;
                    Navigation.PushAsync(new MLSListingsPage(userDecision.home_type, userDecision.zip_code));
                }
            });

            this.RightIcon2 = "BarChart";
#if DEBUG
            this.RightButton2Command = new Command(async (obj) =>
            {
                if (!isClicked)
                {
                    isClicked = true;
                    if ((this.userDecision.userOptions.Count > 0 && _pageType == 0) || (this.userDecision.userOptions.Count > 1 && _pageType == 1))
                    {
                        if (AddOption())
                        {
                            await Navigation.PushModalAsync(new ViewResultPage(this.userDecision, 0));
                            Navigation.RemovePage(this);
                        }
                        else
                        {
                            isClicked = false;
                        }
                    }
                    else if (_pageType == 3)
                    {
                        if (AddOption())
                        {
                            await Navigation.PopAsync();
                        }
                        else
                        {
                            isClicked = false;
                        }
                    }
                    else
                    {
                        await DisplayAlert("Option Limit", "You must add atleast two options to show result!", "Okay");
                        isClicked = false;
                    }
                }
            });
#else
            this.RightButton2Command = new Command(async (obj) =>
            {
                if (!isClicked)
                {
                    fromMenu = true;
                    isClicked = true;
                    if ((this.userDecision.userOptions.Count > 0 && _pageType == 0) || (this.userDecision.userOptions.Count > 1 && _pageType == 1))
                    {
                        AddOption();
                    }
                    else if (_pageType == 3)
                    {
                        AddOption();
                    }
                    else
                    {
                        await DisplayAlert("Option Limit", "You must add atleast two options to show result!", "Okay");
                        isClicked = false;
                    }
                }
            });
#endif
            if (_pageType == 0 || _pageType == 3)
            {
                foreach (var item in this.userDecision.criteriaSelected)
                {
                    selectedCriteria.Add(new CriteriaModel() { id = item.id, title = item.title, is_selected = item.is_selected, criteria_value = 1.0, image = item.image });
                }

                weightSelection.ItemsSource = null;
                weightSelection.ItemsSource = selectedCriteria;
                weightSelection.HeightRequest = selectedCriteria.Count * 65.ScaleHeight();
            }
            else
            {
                if (this.userDecision.userOptions.ToList().Exists(x => x.id == optionId))
                {
                    decisionOption.Update(this.userDecision.userOptions.FirstOrDefault(x => x.id == optionId));
                    homeNameEntry.Text = decisionOption.home_name;

                    if (!string.IsNullOrEmpty(decisionOption.mls_id))
                    {
                        mlsEntry.Text = decisionOption.mls_id;
                        mlsProperty = new SimplyRetsModel();
                        mlsProperty = decisionOption.simplyRetsProperty;
                    }

                    addressEntry.Text = decisionOption.address;

                    foreach (var item in this.userDecision.criteriaSelected)
                    {
                        if (!decisionOption.criteriaWeight.ToList().Exists(x => x.id == item.id))
                        {
                            decisionOption.criteriaWeight.Add(new CriteriaModel() { id = item.id, title = item.title, is_selected = item.is_selected, criteria_value = 1.0, image = item.image });
                        }
                    }

                    for (int i = decisionOption.criteriaWeight.Count - 1; i >= 0; i--)
                    {
                        if (!this.userDecision.criteriaSelected.ToList().Exists(x => x.id == (decisionOption.criteriaWeight[i]).id))
                        {
                            decisionOption.criteriaWeight.Remove(decisionOption.criteriaWeight.FirstOrDefault(x => x.id == (decisionOption.criteriaWeight[i]).id));
                        }
                    }

                    weightSelection.ItemsSource = null;
                    weightSelection.ItemsSource = decisionOption.criteriaWeight;
                    weightSelection.HeightRequest = decisionOption.criteriaWeight.Count * 65.ScaleHeight();
                }
            }
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            isClicked = false;
#if DEBUG == false
            restService.WebServiceDelegate = this;
#endif
            simplyRetsHelper.WebServiceDelegate = this;

            DependencyService.Get<IStatusStyle>().StatusStyle(0);
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }

        void OnEntry_Completed(object sender, System.EventArgs e)
        {
            mlsEntry.Focus();
        }

        void OnWeights_ValueChanged(object sender, Xamarin.Forms.ValueChangedEventArgs e)
        {
            if (((Slider)sender).ClassId != null)
            {
                if (_pageType == 0 || _pageType == 3)
                {
                    if (selectedCriteria.ToList().Exists(x => x.id == int.Parse(((Slider)sender).ClassId)))
                    {
                        (selectedCriteria.FirstOrDefault(x => x.id == int.Parse(((Slider)sender).ClassId))).criteria_value = Math.Round(e.NewValue, 1);
                    }
                }
                else
                {
                    if (decisionOption.criteriaWeight.ToList().Exists(x => x.id == int.Parse(((Slider)sender).ClassId)))
                    {
                        (decisionOption.criteriaWeight.FirstOrDefault(x => x.id == int.Parse(((Slider)sender).ClassId))).criteria_value = Math.Round(e.NewValue, 1);
                    }
                }
            }

            foreach (var item in selectedCriteria)
            {
                App.Log(item.criteria_value.ToString(), "OldCritValue");
            }
            if (decisionOption.criteriaWeight != null)
            {
                foreach (var item in decisionOption.criteriaWeight)
                {
                    App.Log(item.criteria_value.ToString(), "NewCritValue");
                }
            }
        }

        void OnWeight_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            weightSelection.SelectedItem = null;
        }

        void DisplayError(Entry entry, string error)
        {
            entry.Text = String.Empty;
            entry.Placeholder = error;
            entry.PlaceholderColor = Color.Red;
        }


        void OnSaveOption_Clicked(object sender, EventArgs e)
        {
            if (!isClicked)
            {
                fromMenu = false;
                isClicked = true;
#if DEBUG
                if (AddOption())
                {
                    Navigation.PopAsync();
                }
                else
                {
                    isClicked = false;
                }
#else
                AddOption();
#endif
            }
        }

#if DEBUG
        bool AddOption()
        {
            bool isNotError = true;

            if (String.IsNullOrEmpty(homeNameEntry.Text))
            {
                DisplayError(homeNameEntry, "Home Name is required!");
                isNotError = isClicked = false;
            }

            if (!String.IsNullOrEmpty(mlsEntry.Text))
            {
                if (mlsEntry.Text.Length != 7)
                {
                    DisplayError(mlsEntry, "MLS ID is invalid!");
                    isNotError = isClicked = false;
                }
                else
                {
                    if (mlsProperty == null)
                    {
                        DisplayAlert("Error", "No MLS Property Found using the MLS ID " + mlsEntry.Text, "Okay");
                        isNotError = isClicked = false;
                    }
                }
            }

            if (String.IsNullOrEmpty(addressEntry.Text))
            {
                DisplayError(addressEntry, "Address is required!");
                isNotError = isClicked = false;
            }

            if (isNotError)
            {
                if (this._pageType == 0 || this._pageType == 3)
                {
                    this.userDecision.userOptions.Add(new DecisionOption() { id = this.userDecision.userOptions.Count + 1, mls_id = mlsEntry.Text, home_name = homeNameEntry.Text, address = addressEntry.Text, criteriaWeight = selectedCriteria, has_mls = hasMLS ? 1 : 0, simplyRetsProperty = mlsProperty ?? new SimplyRetsModel() });
                }
                else
                {
                    decisionOption.mls_id = mlsEntry.Text;
                    decisionOption.home_name = homeNameEntry.Text;
                    decisionOption.address = addressEntry.Text;
                    if (hasMLS)
                    {
                        decisionOption.simplyRetsProperty = mlsProperty;
                        decisionOption.has_mls = 1;
                    }
                    this.userDecision.userOptions.FirstOrDefault(x => x.id == this.optionId).Update(decisionOption);
                }
                return true;
            }
            return false;
        }

#else

        async void AddOption()
        {
            bool isNotError = true;

            if (String.IsNullOrEmpty(homeNameEntry.Text))
            {
                DisplayError(homeNameEntry, "Home Name is required!");
                isNotError = isClicked = false;
            }

            if (!String.IsNullOrEmpty(mlsEntry.Text))
            {
                if (mlsEntry.Text.Length != 7)
                {
                    DisplayError(mlsEntry, "MLS ID is invalid!");
                    isNotError = isClicked = false;
                }
                else
                {
                    if (mlsProperty == null)
                    {
                        DisplayAlert("Error", "No MLS Property Found using the MLS ID " + mlsEntry.Text, "Okay");
                        isNotError = isClicked = false;
                    }
                }
            }

            if (String.IsNullOrEmpty(addressEntry.Text))
            {
                DisplayError(addressEntry, "Address is required!");
                isNotError = isClicked = false;
            }

            if (isNotError)
            {
                await Task.Run(async () =>
                {
                    cts = new CancellationTokenSource();
                    ObservableCollection<Criteria> criterium = new ObservableCollection<Criteria>();


                    if (this._pageType == 0 || this._pageType == 3)
                    {
                        foreach (var criteria in selectedCriteria)
                        {
                            criterium.Add(new Criteria() { criteria_type_id = criteria.id, title = criteria.title, criteria_value = criteria.criteria_value, image = criteria.image });
                        }
                        var json = JsonConvert.SerializeObject(new { token = dataClass.token, decision_id = userDecision.id, option = new { option_name = homeNameEntry.Text, mls_id = mlsEntry.Text, address = addressEntry.Text }, weight_criteria = criterium });
                        await restService.PostRequestAsync(Constants.ADD_OPTION, json, cts.Token);
                    }
                    else
                    {
                        foreach (var criteria in decisionOption.criteriaWeight)
                        {
                            criterium.Add(new Criteria() { criteria_type_id = criteria.id, title = criteria.title, criteria_value = criteria.criteria_value, image = criteria.image });
                        }
                        var json = JsonConvert.SerializeObject(new { token = dataClass.token, id = decisionOption.id, decision_id = userDecision.id, option = new { option_name = homeNameEntry.Text, mls_id = mlsEntry.Text, address = addressEntry.Text }, weight_criteria = criterium });
                        await restService.PostRequestAsync(Constants.UPDATE_OPTION, json, cts.Token);
                    }
                });
            }else{
                isClicked = false;
            }
        }
#endif


        void OnMLS_Completed(object sender, System.EventArgs e)
        {
            addressEntry.Focus();
        }

        async void OnMLS_TextChanged(object sender, System.EventArgs e)
        {
            if (mlsEntry.Text.Length == 7)
            {
                cts = new CancellationTokenSource();
                try
                {
                    await simplyRetsHelper.GetRequest(Constants.SIMPLYRETS_URL + "/" + mlsEntry.Text, cts.Token, 0);
                }
                catch (OperationCanceledException ex)
                {
                    App.Log(ex.StackTrace + ex.Message, "SimplyRets");
                    hasMLS = false;
                }
                catch (Exception ex)
                {
                    App.Log(ex.StackTrace + ex.Message, "SimplyRets");
                    hasMLS = false;
                }

                cts = null;
            }
            else
            {
                this.ClassId = "0";
                hasMLS = false;
            }
        }

        void OnViewMLS_Clicked(object sender, EventArgs e)
        {
            if (!isClicked)
            {
                isClicked = true;
                Navigation.PushAsync(new ViewMLSPropertyPage(mlsProperty));
            }
        }

        public void ReceiveSimplyRetsJSONData(JObject jsonData, CancellationToken ct)
        {
            
            mlsProperty = new SimplyRetsModel();
            mlsProperty = JsonConvert.DeserializeObject<SimplyRetsModel>(jsonData.ToString());

            this.ClassId = "1";

            hasMLS = true;
        }

        public void ReceiveSimplyRetsJSONData(JArray jsonData, CancellationToken ct)
        {

        }

        public void ReceiveSimplyRetsTimeoutError(string title, string error)
        {
            this.ClassId = "0";

            hasMLS = false;

            DisplayAlert(title, error, "Okay");
        }

        public void ReceiveJSONData(JObject jsonData, CancellationToken ct)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                isClicked = false;
                if (int.Parse(jsonData["status"].ToString()) == 200)
                {
                    DecisionOption option = JsonConvert.DeserializeObject<DecisionOption>(jsonData["option"].ToString());
                    if (this._pageType == 0 || this._pageType == 3)
                    {
                        option.simplyRetsProperty = mlsProperty ?? new SimplyRetsModel();
                        this.userDecision.userOptions.Add(option);
                    }
                    else
                    {
                        if (hasMLS)
                        {
                            option.simplyRetsProperty = mlsProperty;
                            option.has_mls = 1;
                        }
                        this.userDecision.userOptions.FirstOrDefault(x => x.id == this.optionId).Update(option);

                        //if (this._pageType == 0 || this._pageType == 3)
                        //{
                        //    this.userDecision.userOptions.Add(new DecisionOption() { id = this.userDecision.userOptions.Count + 1, mls_id = mlsEntry.Text, home_name = homeNameEntry.Text, address = addressEntry.Text, criteriaWeight = selectedCriteria, has_mls = hasMLS ? 1 : 0, simplyRetsProperty = mlsProperty ?? new SimplyRetsModel() });
                        //}
                        //else
                        //{
                        //    decisionOption.mls_id = mlsEntry.Text;
                        //    decisionOption.home_name = homeNameEntry.Text;
                        //    decisionOption.address = addressEntry.Text;
                        //    if (hasMLS)
                        //    {
                        //        decisionOption.simplyRetsProperty = mlsProperty;
                        //        decisionOption.has_mls = 1;
                        //    }
                        //    this.userDecision.userOptions.FirstOrDefault(x => x.id == this.optionId).Update(decisionOption);
                        //}
                    }

                    if(fromMenu){
                        if ((this.userDecision.userOptions.Count > 0 && _pageType == 0) || (this.userDecision.userOptions.Count > 1 && _pageType == 1))
                        {
                            await Navigation.PushModalAsync(new ViewResultPage(this.userDecision, 0));
                            Navigation.RemovePage(this);
                        }
                        else
                        {
                            await Navigation.PopAsync();
                        }
                    }else{
                        await Navigation.PopAsync();
                    }
                }
                else
                {
                    isClicked = false;
                }
            });
        }

        public void ReceiveTimeoutError(string title, string error)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                await DisplayAlert(title, error, "Okay");
                isClicked = false;
            });
        }
    }
}