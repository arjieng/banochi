﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;

namespace banochixamarin
{
    public partial class ImportancePage : RootViewPage, IRestConnector
    {
        DataClass dataClass = DataClass.GetInstance;
        UserDecision userDecision;
        CancellationTokenSource cts;
        int pageType;
        bool isClicked;

#if DEBUG == false
        RestServices restService = new RestServices();
#endif


        //0 - Create : 1- Edit
        public ImportancePage(UserDecision userDecision, int pageType)
        {
            InitializeComponent();

            this.userDecision = userDecision;

            this.PageTitle = "IMPORTANCE";
            this.LeftIcon = "BackLight";
            this.pageType = pageType;
            this.LeftButtonCommand = new Command((obj) => { Navigation.PopAsync(); });

            importanceSelection.ItemsSource = this.userDecision.criteriaSelected;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
#if DEBUG == false
            restService.WebServiceDelegate = this;
#endif

            DependencyService.Get<IStatusStyle>().StatusStyle(0);

            isClicked = false;
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }

        void OnImportance_ValueChanged(object sender, Xamarin.Forms.ValueChangedEventArgs e)
        {
            if (((Slider)sender).ClassId != null)
            {
                if (this.userDecision.criteriaSelected.ToList().Exists(x => x.id == int.Parse(((Slider)sender).ClassId)))
                {
                    (this.userDecision.criteriaSelected.ToList().FirstOrDefault(x => x.id == int.Parse(((Slider)sender).ClassId))).criteria_value = Math.Round(e.NewValue, 1);
                }
            }
        }

        void OnImportance_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            importanceSelection.SelectedItem = null;
        }

        async void OnDone_Clicked(object sender, EventArgs e)
        {
#if DEBUG
            if (!isClicked)
            {
                isClicked = true;

                if (dataClass.savedResults.ToList().Exists(x => x.id == userDecision.id))
                {
                    dataClass.savedResults.Remove(dataClass.savedResults.FirstOrDefault(x => x.id == userDecision.id));
                }

                if (dataClass.inProgressDecisions.ToList().Exists(x => x.id == userDecision.id))
                {
                    (dataClass.inProgressDecisions.FirstOrDefault(x => x.id == userDecision.id)).Update(userDecision);
                    dataClass.inProgressDecisions.Move(dataClass.inProgressDecisions.ToList().FindIndex((obj) => obj.id == userDecision.id), 0);
                }
                else
                {
                    dataClass.inProgressDecisions.Insert(0, userDecision);
                }

                App.ShowDecisionOptionPage(dataClass.inProgressDecisions.FirstOrDefault(x => x.id == userDecision.id));
            }
#else
            if(!isClicked){
                isClicked = true;

                await Task.Run(async () =>
                {
                    cts = new CancellationTokenSource();
                    ObservableCollection<Criteria> criterium = new ObservableCollection<Criteria>();

                    foreach (var criteria in userDecision.criteriaSelected)
                    {
                        criterium.Add(new Criteria { criteria_type_id = criteria.id, title = criteria.title, criteria_value = criteria.criteria_value, image = criteria.image });
                    }

                    string json = "";

                    if (pageType == 1)
                    {
                        json = JsonConvert.SerializeObject(new { token = dataClass.token, id = userDecision.id, decision = new { home_type = userDecision.home_type, zipcode = userDecision.zip_code, is_saved = false }, decision_criteria = criterium });
                        await restService.PostRequestAsync(Constants.UPDATE_DECISION, json, cts.Token);

                    }
                    else
                    {
                        json = JsonConvert.SerializeObject(new { token = dataClass.token, decision = new { home_type = userDecision.home_type, zipcode = userDecision.zip_code, is_saved = false }, decision_criteria = criterium });
                        await restService.PostRequestAsync(Constants.ADD_DECISION, json, cts.Token);
                    }
                });
            }
#endif
        }

        public void ReceiveJSONData(JObject jsonData, CancellationToken ct)
        {
            isClicked = false;
            UserDecision ud = JsonConvert.DeserializeObject<UserDecision>(jsonData["decision"].ToString());
            Device.BeginInvokeOnMainThread(() =>
            {
                if (dataClass.savedResults.ToList().Exists(x => x.id == ud.id))
                {
                    dataClass.savedResults.Remove(dataClass.savedResults.FirstOrDefault(x => x.id == ud.id));
                }

                if (dataClass.inProgressDecisions.ToList().Exists(x => x.id == ud.id))
                {
                    (dataClass.inProgressDecisions.FirstOrDefault(x => x.id == ud.id)).Update(ud);
                    dataClass.inProgressDecisions.Move(dataClass.inProgressDecisions.ToList().FindIndex((obj) => obj.id == ud.id), 0);
                }
                else
                {
                    dataClass.inProgressDecisions.Insert(0, ud);
                }
                App.ShowDecisionOptionPage(dataClass.inProgressDecisions.FirstOrDefault(x => x.id == ud.id));
            });
        }

        public void ReceiveTimeoutError(string title, string error)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                await DisplayAlert(title, error, "Okay");
                isClicked = false;
            });
        }
    }

    public class Criteria{
        public int criteria_type_id { get; set; }
        public string title { get; set; }
        public double criteria_value { get; set; }
        public string image { get; set; }
    }
}