﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;

namespace banochixamarin
{
    public partial class MLSListingsPage : RootViewPage, ISimplyRetsConnector
    {
        ObservableCollection<SimplyRetsModel> mlsPropertyListing = new ObservableCollection<SimplyRetsModel>();
        CancellationTokenSource cts;
        SimplyRetsHelper simplyRetsHelper = new SimplyRetsHelper();
        string homeType, zipCode;

        public MLSListingsPage(string homeType, string zipCode)
        {
            InitializeComponent();

            this.homeType = homeType;
            this.zipCode = zipCode;

            this.PageTitle = "MLS LISTINGS";
			this.LeftIcon = "BackLight";
			this.LeftButtonCommand = new Command((obj) => { Navigation.PopAsync(); });
        }

        protected override void OnAppearing()
		{
			base.OnAppearing();

            GetMLSListings();

			DependencyService.Get<IStatusStyle>().StatusStyle(0);
		}

		protected override void OnDisappearing()
		{
			base.OnDisappearing();
		}

        async void GetMLSListings()
        {
            simplyRetsHelper.WebServiceDelegate = this;

			cts = new CancellationTokenSource();
			try
			{
				await simplyRetsHelper.GetRequest(Constants.SIMPLYRETS_URL + "?q=" + zipCode + "&type=" + homeType + "&status=Active&count=false&limit=10", cts.Token, 1);
			}
			catch (OperationCanceledException ex)
			{
				App.Log(ex.StackTrace + ex.Message, "SimplyRetsListing");
			}
			catch (Exception ex)
			{
				App.Log(ex.StackTrace + ex.Message, "SimplyRetsListing");
			}

			cts = null;
        }

        void OnImportance_ItemTapped(object sender, ItemTappedEventArgs e)
		{
			MLSList.SelectedItem = null;

            Navigation.PushAsync(new ViewMLSPropertyPage(e.Item as SimplyRetsModel));
		}

		public void ReceiveSimplyRetsJSONData(JObject jsonData, CancellationToken ct)
		{
			
		}

		public void ReceiveSimplyRetsJSONData(JArray jsonData, CancellationToken ct)
		{
			mlsPropertyListing = JsonConvert.DeserializeObject<ObservableCollection<SimplyRetsModel>>(jsonData.ToString());
			MLSList.ItemsSource = mlsPropertyListing;
		}

		public void ReceiveSimplyRetsTimeoutError(string title, string error)
		{
			DisplayAlert(title, error, "Okay");
		}
    }
}
