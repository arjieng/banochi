﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Plugin.Share;
using Plugin.Share.Abstractions;
using Xamarin.Forms;

namespace banochixamarin
{
    public partial class ViewResultPage : RootViewPage, IRestConnector
    {
		DataClass dataClass = DataClass.GetInstance;
		UserDecision userDecision = new UserDecision();
        double answer, result;
        int pageType, optionsCount, reqType=0;
        bool isClicked, isSaved;

#if DEBUG == false
        CancellationTokenSource cts;
        RestServices restService = new RestServices();
#endif

        // 0 - DecisionOption and AddOption : 1 - SavedResults
        public ViewResultPage(UserDecision userDecision, int pageType)
        {
            InitializeComponent();

            this.pageType = pageType;

            switch(this.pageType)
            {
                case 0: this.userDecision = userDecision; break;
                case 1: this.userDecision.Update(userDecision); break;
            }

            optionsCount = this.userDecision.userOptions.Count;

			this.PageTitle = "RESULT";

            this.LeftIcon = this.pageType == 0 ? "CloseLight" : "BackLight";
            this.LeftButtonCommand = new Command((obj) => 
            {
                if (!isClicked)
                {
                    isClicked = true;
                    switch (this.pageType)
                    {
                        case 0:
                            Navigation.PopModalAsync();
                            break;
                        case 1:
                            Navigation.PopAsync();
                            break;
                    }
                }
            });

			this.RightIcon1 = "PlusLight";
			this.RightButton1Command = new Command(async (obj) =>
			{
				if (!isClicked)
				{
					isClicked = true;
					switch (this.pageType)
					{
						case 0:
							await Navigation.PopModalAsync();
							break;
						case 1:
                            await Navigation.PushAsync(new AddOptionPage(3, this.userDecision, 0));
							break;
					}
				}
			});

			this.RightIcon2 = "ShareLight";
			this.RightButton2Command = new Command(async (obj) => 
            {
				if (dataClass.inProgressDecisions.ToList().Exists(x => x.id == userDecision.id))
				{
					dataClass.inProgressDecisions.Remove(dataClass.inProgressDecisions.ToList().FirstOrDefault(x => x.id == userDecision.id));
				}

				if (dataClass.savedResults.ToList().Exists(x => x.id == userDecision.id))
				{
					(dataClass.savedResults.FirstOrDefault(x => x.id == userDecision.id)).Update(userDecision);
					if (optionsCount != this.userDecision.userOptions.Count)
					{
						dataClass.savedResults.Move(dataClass.savedResults.ToList().FindIndex((sResult) => sResult.id == this.userDecision.id), 0);
					}
				}
				else
				{
					dataClass.savedResults.Insert(0, userDecision);
				}

                //if (!CrossShare.IsSupported)
                //	return;
                //CrossShare.Current.Share(new ShareMessage() { Title = "Banochi", Text = "Sample Text", Url = "Sample URL" }, new ShareOptions() { ChooserTitle = "Share Banochi Result With :" });
                await Task.Run(async() => {
                    reqType = 1;
                    cts = new CancellationTokenSource();
                    var json = JsonConvert.SerializeObject(new { token = dataClass.token, id = userDecision.id });
                    await restService.PostRequestAsync(Constants.SEND_GRAPH, json, cts.Token);
                });


                isSaved = true;
            });
        }
        protected override void OnAppearing()
		{
			base.OnAppearing();

			DependencyService.Get<IStatusStyle>().StatusStyle(0);
#if DEBUG == false
            restService.WebServiceDelegate = this;
#endif
            isClicked = isSaved = false;

            Calculateresult();
		}

        protected override void OnDisappearing()
		{
			base.OnDisappearing();
		}

		void Calculateresult()
		{
            userDecision.decisionResult = new ObservableCollection<DecisionResult>();
            foreach(var option in userDecision.userOptions)
            {
                answer = 0.0;
                foreach(var criterion in userDecision.criteriaSelected)
                {
                    answer += (criterion.criteria_value * (option.criteriaWeight.ToList().FirstOrDefault(x => x.id == criterion.id)).criteria_value);
                }
                result = (answer / userDecision.criteriaSelected.Count()) / 10;
                userDecision.decisionResult.Add(new DecisionResult() { id = option.id, home_name = option.home_name, result_value = result });
            }

            resultList.ItemsSource = userDecision.decisionResult;
            resultList.HeightRequest = userDecision.decisionResult.Count() * 25.ScaleHeight();
		}

		void OnResult_ItemTapped(object sender, ItemTappedEventArgs e)
		{
			resultList.SelectedItem = null;
		}

        async void OnSave_Clicked(object sender, EventArgs e)
        {
            if (!isClicked)
            {
                isClicked = true;


#if DEBUG
                if (dataClass.inProgressDecisions.ToList().Exists(x => x.id == userDecision.id))
                {
                  dataClass.inProgressDecisions.Remove(dataClass.inProgressDecisions.ToList().FirstOrDefault(x => x.id == userDecision.id));
                }

                if (dataClass.savedResults.ToList().Exists(x => x.id == userDecision.id))
                {
                  (dataClass.savedResults.FirstOrDefault(x => x.id == userDecision.id)).Update(userDecision);
                  if (optionsCount != this.userDecision.userOptions.Count)
                  {
                      dataClass.savedResults.Move(dataClass.savedResults.ToList().FindIndex((obj) => obj.id == this.userDecision.id), 0);
                  }
                }
                else
                {
                  dataClass.savedResults.Insert(0, userDecision);
                }
#else
                await Task.Run(async () =>
                {
                    if (!isSaved)
                    {
                        reqType = 0;
                        cts = new CancellationTokenSource();
                        var json = JsonConvert.SerializeObject(new { token = dataClass.token, id = userDecision.id, is_saved = true });
                        await restService.PostRequestAsync(Constants.CHANGE_STATUS, json, cts.Token);
                    }
                    else
                    {
                        if (this.pageType == 0)
                        {
                            App.ShowMainPage();
                        }
                        else
                        {
                            await Navigation.PopAsync();
                        }
                    }
                });
#endif
            }
        }

        public void ReceiveJSONData(JObject jsonData, CancellationToken ct)
        {
            Device.BeginInvokeOnMainThread(async () => {
                isClicked = false;
                if (int.Parse(jsonData["status"].ToString()) == 200)
                {
                    if(reqType ==0){
                        if (dataClass.inProgressDecisions.ToList().Exists(x => x.id == userDecision.id))
                        {
                            dataClass.inProgressDecisions.Remove(dataClass.inProgressDecisions.ToList().FirstOrDefault(x => x.id == userDecision.id));
                        }

                        if (dataClass.savedResults.ToList().Exists(x => x.id == userDecision.id))
                        {
                            (dataClass.savedResults.FirstOrDefault(x => x.id == userDecision.id)).Update(userDecision);
                            if (optionsCount != this.userDecision.userOptions.Count)
                            {
                                dataClass.savedResults.Move(dataClass.savedResults.ToList().FindIndex((obj) => obj.id == this.userDecision.id), 0);
                            }
                        }
                        else
                        {
                            dataClass.savedResults.Insert(0, userDecision);
                        }

                        if (this.pageType == 0)
                        {
                            App.ShowMainPage();
                        }
                        else
                        {
                            await Navigation.PopAsync();
                        }
                    }else{
                        await DisplayAlert("Email Sent", "Please check your email.", "OK");
                    }
                }
            });
        }

        public void ReceiveTimeoutError(string title, string error)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                await DisplayAlert(title, error, "Okay");
                isClicked = false;
            });
        }

    }
}
