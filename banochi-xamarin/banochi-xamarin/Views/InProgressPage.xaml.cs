﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace banochixamarin
{
    public partial class InProgressPage : RootViewPage
    {
		UserDecision deleteItem;
		DataClass dataClass = DataClass.GetInstance;

        public InProgressPage()
        {
            InitializeComponent();

            this.PageTitle = "IN - PROGRESS";
			this.LeftIcon = "BurgerDark";
			this.LeftButtonCommand = new Command((obj) => { DependencyService.Get<IStatusStyle>().StatusStyle(1); MyMasterDetailPage detail = (MyMasterDetailPage)this.Parent.Parent; detail.IsPresented = true; });
        
            optionsList.ItemsSource = dataClass.inProgressDecisions;
        }

		protected override void OnAppearing()
		{
			base.OnAppearing();

			DependencyService.Get<IStatusStyle>().StatusStyle(1);
		}

		protected override void OnDisappearing()
		{
			base.OnDisappearing();
		}

		void ItemOptions_Clicked(object sender, System.EventArgs e)
		{
			Button button = sender as Button;
			var item = button.CommandParameter as UserDecision;
			DecisionListAction(button.ClassId, item);
		}

		void OptionItem_Tapped(object sender, object e)
		{
			GestureStackLayout button = sender as GestureStackLayout;
			var item = e as UserDecision;
			DecisionListAction(button.ClassId, item);
		}

		void DecisionListAction(string classid, UserDecision decisionItem)
		{
			switch (classid)
			{
				case "0"://View

					break;
				case "1"://Edit

					break;
				case "2"://Remove
					deleteItem = decisionItem;
					OnDeleteSuccessWS();
					break;
			}
		}

		void OnDeleteSuccessWS()
		{
			if (deleteItem != null)
			{
				for (int i = 0; i < dataClass.inProgressDecisions.Count; i++)
				{
					if (deleteItem.id == dataClass.inProgressDecisions[i].id)
					{
						// Xamarin Forms Bug : Bug 56793 - ListView with uneven row height not updating layout if items change size after item has been deleted
						// https://bugzilla.xamarin.com/show_bug.cgi?id=56793
						// Bug in Android: Cannot swipe left and right after one item is deleted because swiping involves changing of layout position.
						// Workaround: Set ItemSource to null, remove the item and restore the source.

						switch (Device.RuntimePlatform)
						{
							case Device.iOS:
								dataClass.inProgressDecisions.Remove(deleteItem);
								break;
							case Device.Android:
								optionsList.ItemsSource = null;
								dataClass.inProgressDecisions.Remove(deleteItem);
								//dataClass.inProgressDecisions.DeleteReference.Invoke(this, null);
								optionsList.ItemsSource = dataClass.inProgressDecisions;
								break;
						}
					}
				}
			}
			deleteItem = null;
		}

		void OnLast_ItemAppearing(object sender, Xamarin.Forms.ItemVisibilityEventArgs e)
		{

		}
    }
}
