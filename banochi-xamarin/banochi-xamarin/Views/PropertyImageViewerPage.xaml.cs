﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace banochixamarin
{
    public partial class PropertyImageViewerPage : RootViewPage
    {
        public PropertyImageViewerPage(String photoUrl)
        {
            InitializeComponent();

            this.LeftIcon = "BackLight";
            this.LeftButtonCommand = new Command((obj) => { Navigation.PopAsync(); });

            imgImage.Source = photoUrl;
        }

		protected override void OnAppearing()
		{
			base.OnAppearing();

			DependencyService.Get<IStatusStyle>().StatusStyle(0);
		}

		protected override void OnDisappearing()
		{
			base.OnDisappearing();
		}
    }
}
