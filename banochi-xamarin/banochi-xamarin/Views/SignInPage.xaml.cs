﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xamarin.Forms;

namespace banochixamarin
{
    public partial class SignInPage : ContentPage, IFileConnector, IRestConnector
    {
        DataClass dataClass = DataClass.GetInstance;
        bool isClicked;
        CancellationTokenSource cts;

#if DEBUG
        FileReader fileReader = FileReader.GetInstance;
        int loginType;
        string email, image, name;
#else
		RestServices restService = new RestServices();
#endif
        public SignInPage()
        {
            InitializeComponent();

            MessagingCenter.Subscribe<Object, string>(this, "ResetPassword", (obj, email) =>
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    dataClass.user = new User();
                    dataClass.user.email = email;
                    App.ShowResetPasswordPage();
                });
            });
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
#if DEBUG
            fileReader.FileReaderDelegate = this;
#else
            restService.WebServiceDelegate = this;
#endif
            isClicked = false;

            DependencyService.Get<IStatusStyle>().StatusStyle(1);

            OnCachedEmail_Show();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();

            OnCachedEmail_Show();
        }

        void OnCachedEmail_Show()
        {
            emailEntry.Placeholder = "Email";
            passwordEntry.Placeholder = "Password";
            emailEntry.PlaceholderColor = Constants.LABEL_GRAY;
            passwordEntry.PlaceholderColor = Constants.LABEL_GRAY;

            if (dataClass.user != null)
            {
                if (!string.IsNullOrEmpty(dataClass.user.email))
                {
                    emailLabel.Opacity = 1;
                    emailEntry.Text = dataClass.user.email;
                }
                else
                {
                    emailLabel.Opacity = 0;
                    emailEntry.Text = string.Empty;
                }
            }
        }

        void Email_Completed(object sender, EventArgs e)
        {
            passwordEntry.Focus();
        }

        void OnForgot_Clicked(object sender, EventArgs e)
        {
            if (!isClicked)
            {
                isClicked = true;
                Navigation.PushModalAsync(new ForgotPasswordPage());
            }
        }

        void ToSignUp_Tapped(object sender, EventArgs e)
        {
            if (!isClicked)
            {
                isClicked = true;
                Navigation.PushAsync(new SignUpPage());
            }
        }

        void DisplayError(Entry entry, string error)
        {
            entry.Text = String.Empty;
            entry.Placeholder = error;
            entry.PlaceholderColor = Color.Red;
        }

        public async Task<Stream> ImageSteam(string url)
        {
            HttpClient client = new HttpClient();
            client.MaxResponseContentBufferSize = 256000;
            Stream stream = await client.GetStreamAsync(url);
            return stream;
        }

        void OnFacebook_Clicked(object sender, EventArgs e)
        {
            if (!isClicked)
            {
                isClicked = true;
                DependencyService.Get<IFacebookManager>().Login(OnFacebookLoginComplete);
            }
        }

        async void OnFacebookLoginComplete(FacebookUser facebookUser, string message)
        {
            if (facebookUser != null)
            {
                await Task.Run(async () =>
                {
                    cts = new CancellationTokenSource();

                    try
                    {
#if DEBUG
                        loginType = 1;
                        email = facebookUser.Email;
                        image = facebookUser.Picture;
                        name = facebookUser.Name;
                        await fileReader.ReadFile("User.json", cts.Token);

#else
                        var json = JsonConvert.SerializeObject(new { user = new { provider_id = facebookUser.Id.ToString() } });
                        await restService.PostRequestAsync(Constants.SOCIAL_SIGNIN, json, cts.Token);
#endif
                    }
                    catch (OperationCanceledException ex)
                    {
                        App.Log(ex.StackTrace + ex.Message, "FacebookSignIn");
                        isClicked = false;
                    }
                    catch (Exception ex)
                    {
                        App.Log(ex.StackTrace + ex.Message, "FacebookSignIn");
                        isClicked = false;
                    }

                    cts = null;
                });
            }
            else
            {
                App.Log(message, "FacebookSignIn");
                isClicked = false;
            }
        }

        void OnTwitter_Clicked(object sender, EventArgs e)
        {
            if (!isClicked)
            {
                isClicked = true;
                DependencyService.Get<ITwitter>().Login(OnTwitterLoginComplete);
            }
        }

        async void OnTwitterLoginComplete(JObject twitterUser, string message)
        {
            if (twitterUser != null)
            {
                await Task.Run(async () =>
                {
                    cts = new CancellationTokenSource();

                    try
                    {
#if DEBUG
                        loginType = 1;
                        email = twitterUser["email"].ToString();
                        image = twitterUser["profile_image_url"].ToString();
                        name = twitterUser["name"].ToString();
                        await fileReader.ReadFile("User.json", cts.Token);

#else
                        var json = JsonConvert.SerializeObject(new { user = new { provider_id = twitterUser["id"].ToString() } });
                        await restService.PostRequestAsync(Constants.SOCIAL_SIGNIN, json, cts.Token);

#endif
                    }
                    catch (OperationCanceledException ex)
                    {
                        App.Log(ex.StackTrace + ex.Message, "TwitterSignIn");
                        isClicked = false;
                    }
                    catch (Exception ex)
                    {
                        App.Log(ex.StackTrace + ex.Message, "TwitterSignIn");
                        isClicked = false;
                    }

                    cts = null;
                });
            }
            else
            {
                App.Log(message, "TwitterSignIn");
                isClicked = false;
            }
        }

        async void OnSignIn_Clicked(object sender, EventArgs e)
        {
            bool isNotError = true;

            if (String.IsNullOrEmpty(emailEntry.Text))
            {
                DisplayError(emailEntry, "Email is required!");
                isNotError = false;
            }

            if (String.IsNullOrEmpty(passwordEntry.Text))
            {
                DisplayError(passwordEntry, "Password is required!");
                isNotError = false;
            }

            if (isNotError && isClicked == false)
            {
                isClicked = true;

                await Task.Run(async () =>
                {
                    cts = new CancellationTokenSource();

                    try
                    {
#if DEBUG
                        loginType = 0;
                        await fileReader.ReadFile("User.json", cts.Token);

#else
                        var json = JsonConvert.SerializeObject(new { user = new { email = emailEntry.Text, password = passwordEntry.Text } });
                        await restService.PostRequestAsync(Constants.SIGNIN, json, cts.Token); 
#endif
                    }
                    catch (OperationCanceledException ex)
                    {
                        App.Log(ex.StackTrace + ex.Message, "SignInPage");
                        isClicked = false;
                    }
                    catch (Exception ex)
                    {
                        App.Log(ex.StackTrace + ex.Message, "SignInPage");
                        isClicked = false;
                    }

                    cts = null;
                });
            }
        }

        public void ReceiveJSONData(JObject jsonData, CancellationToken ct)
        {
            App.Log(jsonData.ToString(), "asd");
            Device.BeginInvokeOnMainThread(() =>
            {
            if (int.Parse(jsonData["status"].ToString()) == 200)
            {
                dataClass.user = JsonConvert.DeserializeObject<User>(jsonData["user"].ToString());
                dataClass.token = jsonData["user"]["token"].ToString();

#if DEBUG
                switch (loginType)
                {
                    case 0:
                        dataClass.user.email = emailEntry.Text;
                        break;
                    case 1:
                        dataClass.user.email = email;
                        dataClass.user.image = image;
                        dataClass.user.name = name;
                        break;
                }
#else
                    dataClass.inProgressDecisions = JsonConvert.DeserializeObject<System.Collections.ObjectModel.ObservableCollection<UserDecision>>(jsonData["decisions"]["progress"].ToString());
                    dataClass.savedResults = JsonConvert.DeserializeObject<System.Collections.ObjectModel.ObservableCollection<UserDecision>>(jsonData["decisions"]["saved"].ToString());

                    if (Application.Current.Properties.ContainsKey("saved_pagination"))
                    {
                        Application.Current.Properties["saved_pagination"] = jsonData["paginations"]["saved"].ToString();
                    }
                    else
                    {
                        Application.Current.Properties.Add("saved_pagination", jsonData["paginations"]["saved"].ToString());
                    }

                    if (Application.Current.Properties.ContainsKey("progress_pagination"))
                    {
                        Application.Current.Properties["progress_pagination"] = jsonData["paginations"]["progress"].ToString();
                    }
                    else
                    {
                        Application.Current.Properties.Add("progress_pagination", jsonData["paginations"]["progress"].ToString());
                    }

                    Application.Current.SavePropertiesAsync();
#endif
                    dataClass.Update(false);
                    App.ShowMainPage();
    			}
    			else
    			{
    				DisplayError(emailEntry, jsonData["error"].ToString());
    				emailEntry.FontSize = 11.scale();
    				DisplayError(passwordEntry, "");
    			}

    			isClicked = false;
			});
        }

        public void ReceiveTimeoutError(string title, string error)
        {
			Device.BeginInvokeOnMainThread(async () =>
			{
				await DisplayAlert(title, error, "Okay");
                isClicked = false;
			});
        }
    }
}
