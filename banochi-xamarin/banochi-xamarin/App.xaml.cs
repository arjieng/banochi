﻿using System;
using System.Collections.ObjectModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace banochixamarin
{
    public partial class App : Application
    {
		public static float screenHeight { get; set; }
		public static float screenWidth { get; set; }
		public static float DeviceScale { get; set; }
		public static int DeviceType { get { return Constants.ChangeValueTo(0, 1); } }
		public static double screenScale { get { return (screenWidth + screenHeight) / (320.0f + 568.0f); } }
		public static string UDID { get; set; }

        DataClass dataClass = DataClass.GetInstance;

        public App()
        {
            InitializeComponent();

            dataClass.decisionOptions = new ObservableCollection<DecisionOption>();
            dataClass.inProgressDecisions = new ObservableCollection<UserDecision>();
            dataClass.savedResults = new ObservableCollection<UserDecision>();

			if (Application.Current.Properties.ContainsKey("token"))
			{
				ShowMainPage();
			}
			else
			{
				Logout();
			}
        }

        protected override void OnStart()
        {
            // Handle when your app starts
            dataClass.Update(true);
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
            dataClass.Update(false);
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
            dataClass.Update(true);
        }

		public static void Log(string msg, [System.Runtime.CompilerServices.CallerMemberName] string memberName = "")
		{
			msg = DateTime.Now.ToString("HH:mm:ss tt") + " [Banochi]-[" + memberName + "]: " + msg;
#if DEBUG
			System.Diagnostics.Debug.WriteLine(msg);
#else
            DependencyService.Get<ILogServices>().Log(msg);
#endif
		}

		public static void ShowMainPage()
		{
            //Application.Current.MainPage = new MyMasterDetailPage();
            Application.Current.MainPage = new NavigationPage(new HomePage());
		}

		public static void Logout()
		{
            Application.Current.MainPage = new NavigationPage(new SignInPage());
		}

		public static void ShowResetPasswordPage()
		{
			Application.Current.MainPage = new ResetPasswordPage();
		}

		public static void ShowDecisionOptionPage(UserDecision userDecision)
		{
			Application.Current.MainPage = new NavigationPage(new DecisionOptionsPage(userDecision));
		}
    }
}
