﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Xamarin.Forms;

namespace banochixamarin
{
    public partial class MasterPage : ContentPage
    {
		public ListView ListView { get { return listView; } }
		public static EventHandler<Type> SelectedPage { get; set; }
		public static EventHandler<int> PageSelected { get; set; }
		public static ObservableCollection<MasterPageItem> masterPageItems;
		DataClass dataClass = DataClass.GetInstance;
		int lastPageIndex;

		public MasterPage()
		{
			InitializeComponent();

			masterPageItems = new ObservableCollection<MasterPageItem>();
			masterPageItems.Add(new MasterPageItem { Title = "Create Decision", TargetType = typeof(HomePage), IsSelected = 1 });
            masterPageItems.Add(new MasterPageItem { Title = "In-Progress", TargetType = typeof(InProgressPage), IsSelected = 0 });
            masterPageItems.Add(new MasterPageItem { Title = "Saved Results", TargetType = typeof(SavedResultsPage), IsSelected = 0 });
			masterPageItems.Add(new MasterPageItem { Title = "Logout" });
			listView.ItemsSource = masterPageItems;

			SelectedPage += (sender, e) => { OnMasterPage_Selected(masterPageItems, e); };
			PageSelected += (sender, e) => { OnMaster_PageSelected(sender, e); };
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();

            DependencyService.Get<IStatusStyle>().StatusStyle(1);
		}

		protected override void OnDisappearing()
		{
			base.OnDisappearing();
		}

		void OnMasterPage_Selected(ObservableCollection<MasterPageItem> _masterPageItems, Type e)
		{
			_masterPageItems[lastPageIndex].IsSelected = 0;
			lastPageIndex = _masterPageItems.ToList().FindIndex((obj) => obj.TargetType == e);
			_masterPageItems[lastPageIndex].IsSelected = 1;
			listView.SelectedItem = null;
		}

		void OnMaster_PageSelected(object sender, int e)
		{
			masterPageItems[lastPageIndex].IsSelected = 0;
			lastPageIndex = e;
			masterPageItems[lastPageIndex].IsSelected = 1;
			listView.SelectedItem = null;
		}

		void OnClose_Clicked(object sender, System.EventArgs e)
		{
			DependencyService.Get<IStatusStyle>().StatusStyle(0);
			MyMasterDetailPage detail = (MyMasterDetailPage)this.Parent;
			detail.IsPresented = false;
		}
    }
}
