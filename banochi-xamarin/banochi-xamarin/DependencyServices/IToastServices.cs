﻿using System;

namespace banochixamarin
{
    public interface IToastServices
    {
        void DisplayToast(string text, ToastPosition position);
    }

	public enum ToastPosition
	{
		Top,
		Center,
		Bottom
	}
}
