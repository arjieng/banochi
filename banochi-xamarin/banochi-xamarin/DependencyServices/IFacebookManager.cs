﻿using System;

namespace banochixamarin
{
	public interface IFacebookManager
	{
		void Login(Action<FacebookUser, string> onLoginComplete);
		void Logout();
	}
}
