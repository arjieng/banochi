﻿using System;
using Newtonsoft.Json.Linq;

namespace banochixamarin
{
	public interface ITwitter
	{
		void Login(Action<JObject, string> onLoginComplete);
	}
}
