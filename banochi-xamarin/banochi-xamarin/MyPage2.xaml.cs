﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Input;
using Xamarin.Forms;

namespace banochixamarin
{
    public partial class MyPage2 : ContentPage
    {
        ObservableCollection<aa> aaa;
        public MyPage2()
        {
            InitializeComponent();
            aaa = new ObservableCollection<aa>();

            add();
        }
        private bool _isRefreshing = false;
        public bool IsRefreshing
        {
            get { return _isRefreshing; }
            set
            {
                _isRefreshing = value;
                OnPropertyChanged(nameof(IsRefreshing));
            }
        }

        public ICommand RefreshCommand
        {
            get
            {
                return new Command(async () =>
                {
                    IsRefreshing = true;

                    //await RefreshData();  

                    IsRefreshing = false;
                });
            }
        }


        void add(){
            aaa.Add(new aa() { aaa = "asdasdf" });
            aaa.Add(new aa() { aaa = "asdasdf" });
            aaa.Add(new aa() { aaa = "asdasdf" });
            aaa.Add(new aa() { aaa = "asdasdf" });
            aaa.Add(new aa() { aaa = "asdasdf" });
            aaa.Add(new aa() { aaa = "asdasdf" });
            aaa.Add(new aa() { aaa = "asdasdf" });
            aaa.Add(new aa() { aaa = "asdasdf" });
            aaa.Add(new aa() { aaa = "asdasdf" });
            aaa.Add(new aa() { aaa = "asdasdf" });
            aaa.Add(new aa() { aaa = "asdasdf" });
            aaa.Add(new aa() { aaa = "asdasdf" });
            aaa.Add(new aa() { aaa = "asdasdf" });
            aaa.Add(new aa() { aaa = "asdasdf" });
            aaa.Add(new aa() { aaa = "asdasdf" });
            aaa.Add(new aa() { aaa = "asdasdf" });
            asd.ItemsSource = aaa;
        }
    }

    public class aa{
        public string aaa { get; set; }
    }
}
