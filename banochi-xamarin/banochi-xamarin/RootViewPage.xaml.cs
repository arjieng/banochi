﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using Xamarin.Forms;

namespace banochixamarin
{
    public partial class RootViewPage : ContentPage
    {
		public static readonly BindableProperty PageTitleProperty = BindableProperty.Create("PageTitle", typeof(string), typeof(RootViewPage), null);
		public static readonly BindableProperty TitleFontFamilyProperty = BindableProperty.Create("TitleFontFamily", typeof(string), typeof(RootViewPage), Constants.SF_REGULAR);
		public static readonly BindableProperty TitleFontColorProperty = BindableProperty.Create("TitleFontColor", typeof(Color), typeof(RootViewPage), Constants.BUTTON_TEXTCOLOR_WHITE);
		public static readonly BindableProperty LeftIconProperty = BindableProperty.Create("LeftIcon", typeof(string), typeof(RootViewPage), null);
		public static readonly BindableProperty RightIcon1Property = BindableProperty.Create("RightIcon1", typeof(string), typeof(RootViewPage), null);
		public static readonly BindableProperty RightIcon2Property = BindableProperty.Create("RightIcon2", typeof(string), typeof(RootViewPage), null);
		public static readonly BindableProperty LeftButtonCommandProperty = BindableProperty.Create("LeftButtonCommand", typeof(ICommand), typeof(RootViewPage), null);
		public static readonly BindableProperty RightButton1CommandProperty = BindableProperty.Create("RightButton1Command", typeof(ICommand), typeof(RootViewPage), null);
		public static readonly BindableProperty RightButton2CommandProperty = BindableProperty.Create("RightButton2Command", typeof(ICommand), typeof(RootViewPage), null);
		public static readonly BindableProperty NavBackgroundColorProperty = BindableProperty.Create("NavBackgroundColor", typeof(Color), typeof(RootViewPage), Constants.NAVIGATION_BACKGROUND_BLUE);
		public static readonly BindableProperty NavImageProperty = BindableProperty.Create("NavImage", typeof(string), typeof(RootViewPage), "");
		public static readonly BindableProperty IsLoadingProperty = BindableProperty.Create("IsLoading", typeof(bool), typeof(RootViewPage), false);
		public static readonly BindableProperty LeftTextProperty = BindableProperty.Create("LeftText", typeof(string), typeof(RootViewPage), null);
		public static readonly BindableProperty RightText1Property = BindableProperty.Create("RightText1", typeof(string), typeof(RootViewPage), null);
		public static readonly BindableProperty RightText2Property = BindableProperty.Create("RightText2", typeof(string), typeof(RootViewPage), null);
		public static readonly BindableProperty NavOpacityProperty = BindableProperty.Create("NavOpacity", typeof(double), typeof(RootViewPage), 1.0);
		public static readonly BindableProperty LeftButtonColorProperty = BindableProperty.Create("LeftButtonColor", typeof(Color), typeof(RootViewPage), Color.Transparent);
		public static readonly BindableProperty RightButton1ColorProperty = BindableProperty.Create("RightButton1Color", typeof(Color), typeof(RootViewPage), Color.Transparent);
		public static readonly BindableProperty RightButton2ColorProperty = BindableProperty.Create("RightButton2Color", typeof(Color), typeof(RootViewPage), Color.Transparent);

		public string PageTitle
		{
			set { SetValue(PageTitleProperty, value); }
			get { return (string)GetValue(PageTitleProperty); }
		}

		public string TitleFontFamily
		{
			set { SetValue(TitleFontFamilyProperty, value); }
			get { return (string)GetValue(TitleFontFamilyProperty); }
		}

		public Color TitleFontColor
		{
			set { SetValue(TitleFontColorProperty, value); }
			get { return (Color)GetValue(TitleFontColorProperty); }
		}

		public string LeftIcon
		{
			set { SetValue(LeftIconProperty, value); }
			get { return (string)GetValue(LeftIconProperty); }
		}

		public string RightIcon1
		{
			set { SetValue(RightIcon1Property, value); }
			get { return (string)GetValue(RightIcon1Property); }
		}

		public string RightIcon2
		{
			set { SetValue(RightIcon2Property, value); }
			get { return (string)GetValue(RightIcon2Property); }
		}

		public ICommand LeftButtonCommand
		{
			set { SetValue(LeftButtonCommandProperty, value); }
			get { return (ICommand)GetValue(LeftButtonCommandProperty); }
		}

		public ICommand RightButton1Command
		{
			set { SetValue(RightButton1CommandProperty, value); }
			get { return (ICommand)GetValue(RightButton1CommandProperty); }
		}

		public ICommand RightButton2Command
		{
			set { SetValue(RightButton2CommandProperty, value); }
			get { return (ICommand)GetValue(RightButton2CommandProperty); }
		}

		public Color NavBackgroundColor
		{
			set { SetValue(NavBackgroundColorProperty, value); }
			get { return (Color)GetValue(NavBackgroundColorProperty); }
		}

		public string NavImage
		{
			set { SetValue(NavImageProperty, value); }
			get { return (string)GetValue(NavImageProperty); }
		}

		public bool IsLoading
		{
			set { SetValue(IsLoadingProperty, value); }
			get { return (bool)GetValue(IsLoadingProperty); }
		}

		public string LeftText
		{
			set { SetValue(LeftTextProperty, value); }
			get { return (string)GetValue(LeftTextProperty); }
		}

		public string RightText1
		{
			set { SetValue(RightText1Property, value); }
			get { return (string)GetValue(RightText1Property); }
		}

		public string RightText2
		{
			set { SetValue(RightText2Property, value); }
			get { return (string)GetValue(RightText2Property); }
		}

		public double NavOpacity
		{
			set { SetValue(NavOpacityProperty, value); }
			get { return (double)GetValue(NavOpacityProperty); }
		}

		public Color LeftButtonColor
		{
			set { SetValue(LeftButtonColorProperty, value); }
			get { return (Color)GetValue(LeftButtonColorProperty); }
		}

		public Color RightButton1Color
		{
			set { SetValue(RightButton1ColorProperty, value); }
			get { return (Color)GetValue(RightButton1ColorProperty); }
		}

		public Color RightButton2Color
		{
			set { SetValue(RightButton2ColorProperty, value); }
			get { return (Color)GetValue(RightButton2ColorProperty); }
		}

		public RootViewPage()
        {
            InitializeComponent();
        }
    }
}
