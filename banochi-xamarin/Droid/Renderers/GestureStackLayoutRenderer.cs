﻿using System;
using Xamarin.Forms;
using Android.Views;
using banochixamarin;
using banochixamarin.Droid;
using System.Collections.ObjectModel;
using Android.Content;

[assembly: ExportRenderer(typeof(GestureStackLayout), typeof(GestureStackLayoutRenderer))]
namespace banochixamarin.Droid
{
	public class GestureStackLayoutRenderer : Xamarin.Forms.Platform.Android.VisualElementRenderer<StackLayout>
	{
		public GestureStackLayoutRenderer(Context context) : base(context)
        {

		}

		DataClass dataClass = DataClass.GetInstance;
		GestureStackLayout stackLayout;
		public static StackLayout lastLayout;
		float firstX, firstY;

		async void SwipeRightAnimation(StackLayout layout)
		{
			switch (stackLayout.CollectionType)
			{
				case 0:
					var decisionOption = layout.BindingContext as DecisionOption;
					if (decisionOption != null)
					{
						if (decisionOption.is_swiped == false)
						{
							return;
						}

						await layout.LayoutTo(new Rectangle(0, layout.Y, layout.Width, layout.Height), 100, Easing.CubicIn);

						for (int i = 0; i < dataClass.decisionOptions.Count; i++)
						{
							if (decisionOption.id == dataClass.decisionOptions[i].id)
							{
								dataClass.decisionOptions[i].is_swiped = false;
							}
						}
					}
					break;
				case 1:
					var inProgress = layout.BindingContext as UserDecision;
					if (inProgress != null)
					{
						if (inProgress.is_swiped == false)
						{
							return;
						}

						await layout.LayoutTo(new Rectangle(0, layout.Y, layout.Width, layout.Height), 100, Easing.CubicIn);

						for (int i = 0; i < dataClass.inProgressDecisions.Count; i++)
						{
							if (inProgress.id == dataClass.inProgressDecisions[i].id)
							{
								dataClass.inProgressDecisions[i].is_swiped = false;
							}
						}
					}
                    break;
				case 2:
					var saveResult = layout.BindingContext as UserDecision;
					if (saveResult != null)
					{
						if (saveResult.is_swiped == false)
						{
							return;
						}

						await layout.LayoutTo(new Rectangle(0, layout.Y, layout.Width, layout.Height), 100, Easing.CubicIn);

						for (int i = 0; i < dataClass.savedResults.Count; i++)
						{
							if (saveResult.id == dataClass.savedResults[i].id)
							{
								dataClass.savedResults[i].is_swiped = false;
							}
						}
					}
					break;
			}
		}

		protected override void OnElementChanged(Xamarin.Forms.Platform.Android.ElementChangedEventArgs<StackLayout> e)
		{
			base.OnElementChanged(e);

			if (e.NewElement == null)
			{
				this.Touch -= HandleTouch;

			}
			if (e.OldElement == null)
			{
				this.Touch += HandleTouch;
			}
			if (e.NewElement != null)
			{
				stackLayout = e.NewElement as GestureStackLayout;
			}
		}

		async void HandleTouch(object sender, TouchEventArgs e)
		{
			if (stackLayout.IsSwipeable)
			{
    			if (e.Event.Action == MotionEventActions.Down)
    			{
    				firstX = e.Event.GetX();
    				firstY = e.Event.GetY();
    			}
                if (e.Event.Action == MotionEventActions.Up || e.Event.Action == MotionEventActions.Cancel)
                {
                    float finalY = e.Event.GetY();

                    if ((firstX - e.Event.GetX()) > 20)
                    {
						switch (stackLayout.CollectionType)
						{
							case 0:
								var decisionOption = stackLayout.BindingContext as DecisionOption;
								if (decisionOption.is_swiped == true)
								{
									decisionOption.is_swiped = false;
									return;
								}
								if (lastLayout != null)
								{
									SwipeRightAnimation(lastLayout);
									lastLayout = null;
								}

								await stackLayout.LayoutTo(new Rectangle(stackLayout.X - 60, stackLayout.Y, stackLayout.Width, stackLayout.Height), 100, Easing.CubicIn);

								for (int i = 0; i < dataClass.decisionOptions.Count; i++)
								{
									if (decisionOption.id == dataClass.decisionOptions[i].id)
									{
										dataClass.decisionOptions[i].is_swiped = true;
									}
									else
									{
										dataClass.decisionOptions[i].is_swiped = false;
									}
								}

								lastLayout = stackLayout;
								break;
							case 1:
								var inProgress = stackLayout.BindingContext as UserDecision;
								if (inProgress.is_swiped == true)
								{
									inProgress.is_swiped = false;
									return;
								}
								if (lastLayout != null)
								{
									SwipeRightAnimation(lastLayout);
									lastLayout = null;
								}

								await stackLayout.LayoutTo(new Rectangle(stackLayout.X - 60, stackLayout.Y, stackLayout.Width, stackLayout.Height), 100, Easing.CubicIn);

								for (int i = 0; i < dataClass.inProgressDecisions.Count; i++)
								{
									if (inProgress.id == dataClass.inProgressDecisions[i].id)
									{
										dataClass.inProgressDecisions[i].is_swiped = true;
									}
									else
									{
										dataClass.inProgressDecisions[i].is_swiped = false;
									}
								}

								lastLayout = stackLayout;
                                break;
							case 2:
								var savedResult = stackLayout.BindingContext as UserDecision;
								if (savedResult.is_swiped == true)
								{
									savedResult.is_swiped = false;
									return;
								}
								if (lastLayout != null)
								{
									SwipeRightAnimation(lastLayout);
									lastLayout = null;
								}

								await stackLayout.LayoutTo(new Rectangle(stackLayout.X - 120, stackLayout.Y, stackLayout.Width, stackLayout.Height), 100, Easing.CubicIn);

								for (int i = 0; i < dataClass.savedResults.Count; i++)
								{
									if (savedResult.id == dataClass.savedResults[i].id)
									{
										dataClass.savedResults[i].is_swiped = true;
									}
									else
									{
										dataClass.savedResults[i].is_swiped = false;
									}
								}

								lastLayout = stackLayout;
								break;
						}
                    }
                    else if ((firstX - e.Event.GetX()) < -20)
                    {
                        SwipeRightAnimation(stackLayout);
                    }
                    else
                    {
                        if ((firstY - finalY) > 10)
                        {
                            System.Diagnostics.Debug.WriteLine("Up to Down swipe performed");
                        }
                        else if ((firstY - finalY) < -10)
                        {
                            System.Diagnostics.Debug.WriteLine("Down to Up swipe performed");
                        }
                        else
                        {
                            //tap to open item
							switch (stackLayout.CollectionType)
							{
								case 0:
									var decisionOption = stackLayout.BindingContext as DecisionOption;
									stackLayout.OnTappedEvent(decisionOption);
									break;
								case 1:
								case 2:
                                    var userDecision = stackLayout.BindingContext as UserDecision;
                                    stackLayout.OnTappedEvent(userDecision);
									break;
							}
                        }
                    }
                }
			}
		}
	}
}
