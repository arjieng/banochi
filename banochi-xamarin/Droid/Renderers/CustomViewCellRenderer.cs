﻿using System;
using banochixamarin.Droid;
using banochixamarin;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(ViewCell), typeof(CustomViewCellRenderer))]
namespace banochixamarin.Droid
{
    public class CustomViewCellRenderer : ViewCellRenderer
    {
		protected override Android.Views.View GetCellCore(Cell item, Android.Views.View convertView, Android.Views.ViewGroup parent, Android.Content.Context context)
		{
			var cell = base.GetCellCore(item, convertView, parent, context);

			var listView = parent as Android.Widget.ListView;

			if (listView != null)
			{
				// DISABLES LISTVIEW ROW HIGHLIGHT
				listView.SetSelector(Android.Resource.Color.Transparent);
				listView.CacheColorHint = Android.Graphics.Color.Transparent;
			}

			return cell;
		}
    }
}
