﻿using System;
using Xamarin.Forms.Platform.Android;
using Xamarin.Forms;
using banochixamarin;
using banochixamarin.Droid;
using Android.Content;
using Android.Text;
using Android.Views.InputMethods;
using Android.Views;

[assembly: ExportRenderer(typeof(CustomEntry), typeof(CustomEntryRenderer))]
namespace banochixamarin.Droid
{
	public class CustomEntryRenderer : EntryRenderer
	{
		public CustomEntryRenderer(Context context) : base(context)
        {

		}

		CustomEntry customEntry;
		protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
		{
			base.OnElementChanged(e);
			if (Control != null)
			{
				customEntry = Element as CustomEntry;
				this.Control.SetBackgroundColor(Android.Graphics.Color.Transparent);
				Control.SetRawInputType(InputTypes.TextFlagNoSuggestions);
				//Control.InputType(InputTypes.)
				Element.Focused += Element_Focused;
				Control.SetPadding(0, 0, 0, 0);
                Control.Gravity = GravityFlags.CenterVertical;

				switch (customEntry.TextAlignment)
				{
					case 0: // left
						Control.Gravity = GravityFlags.Start;
						break;
					case 1: // right    
						Control.Gravity = GravityFlags.End;
						break;
					case 2: // center    
                        Control.Gravity = GravityFlags.CenterHorizontal;
						break;
					case 3: // justified    
						Control.TextAlignment = Android.Views.TextAlignment.Inherit;
						break;
					default: // natural    
						Control.TextAlignment = Android.Views.TextAlignment.ViewStart;
						break;
				}

				if (customEntry.AutoCapitalization == TextAutoCapitalization.Word)
				{
					Control.InputType = InputTypes.TextFlagCapWords;
				}
				else if (customEntry.AutoCapitalization == TextAutoCapitalization.Sentence)
				{
					Control.InputType = InputTypes.TextFlagCapSentences;
				}
			}

			if (e.OldElement != null)
			{
				Element.Focused -= Element_Focused;
			}
		}

		private void Element_Focused(object sender, FocusEventArgs e)
		{
			var imm = (InputMethodManager)Context.GetSystemService(Android.Content.Context.InputMethodService);
			imm.ShowSoftInput(Control, Android.Views.InputMethods.ShowFlags.Implicit);
		}
	}
}
