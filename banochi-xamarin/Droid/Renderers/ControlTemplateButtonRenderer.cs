﻿using System;
using banochixamarin.Droid;
using banochixamarin;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Android.Views;
using Android.Content;

[assembly: ExportRenderer(typeof(ControlTemplateButton), typeof(ControlTemplateButtonRenderer))]
namespace banochixamarin.Droid
{
	public class ControlTemplateButtonRenderer : ButtonRenderer
	{
		public ControlTemplateButtonRenderer(Context context) : base(context)
        {

		}

		protected override void OnDraw(Android.Graphics.Canvas canvas)
		{
			base.OnDraw(canvas);
		}

		protected override void OnElementChanged(ElementChangedEventArgs<Button> e)
		{
			base.OnElementChanged(e);

            if (Control != null)
            {
                Control.SetPadding(0, 0, 0, 0);
                Control.SetAllCaps(false);
            }
		}
	}
}
