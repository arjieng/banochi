﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Android.Views;
using banochixamarin;
using banochixamarin.Droid;
using Android.Content;

[assembly: ExportRenderer(typeof(ListViewNoScroll), typeof(ListViewNoScrollRenderer))]
namespace banochixamarin.Droid
{
	public class ListViewNoScrollRenderer : ListViewRenderer
	{
		public ListViewNoScrollRenderer(Context context) : base(context)
        {

		}

		protected ListViewNoScroll listViewNoScroll { get; private set; }
		protected override void OnElementChanged(ElementChangedEventArgs<ListView> e)
		{
			base.OnElementChanged(e);

			if (Control != null || e.NewElement != null)
			{
				this.listViewNoScroll = (ListViewNoScroll)this.Element;
				Control.VerticalScrollBarEnabled = false;
				Control.HorizontalScrollBarEnabled = false;
			}
		}

		public override bool DispatchTouchEvent(MotionEvent e)
		{
			if (!listViewNoScroll.Scroll)
			{
				if (e.Action == MotionEventActions.Move)
					return true;
			}
			
			return base.DispatchTouchEvent(e);
		}
	}
}
