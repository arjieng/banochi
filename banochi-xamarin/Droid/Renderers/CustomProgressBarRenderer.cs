﻿using System;
using Android.Content;
using banochixamarin;
using banochixamarin.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(CustomProgressBar), typeof(CustomProgressBarRenderer))]
namespace banochixamarin.Droid
{
	public class CustomProgressBarRenderer : ProgressBarRenderer
	{
        CustomProgressBar view;

		public CustomProgressBarRenderer(Context context) : base(context)
        {

		}

		protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.ProgressBar> e)
		{
			base.OnElementChanged(e);

			if (e.OldElement != null || e.NewElement == null)
				return;

			view = (CustomProgressBar)Element;

			if (view.MaxColor != Xamarin.Forms.Color.Default)
			{
				//this is for Maximum Progress line Color 
                Control.ProgressDrawable.SetColorFilter(view.MaxColor.ToAndroid(), Android.Graphics.PorterDuff.Mode.SrcIn);
			}

			if (view.MinColor != Xamarin.Forms.Color.Default)
			{
				//this is for Minimum Progress line Color 
                Control.ProgressTintList = Android.Content.Res.ColorStateList.ValueOf(view.MinColor.ToAndroid());
			}

			Control.ScaleY = 4;
		}

		protected override void OnDraw(Android.Graphics.Canvas canvas)
		{
			base.OnDraw(canvas);
		}
	}
}
