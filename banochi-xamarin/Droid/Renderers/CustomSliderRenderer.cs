﻿using System;
using Android.Content;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.Views;
using Android.Widget;
using banochixamarin;
using banochixamarin.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(CustomSlider), typeof(CustomSliderRenderer))]
namespace banochixamarin.Droid
{
    public class CustomSliderRenderer : SliderRenderer
    {
		public CustomSliderRenderer(Context context) : base(context)
        {

		}

        CustomSlider view;
        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.Slider> e)
		{
			base.OnElementChanged(e);
			if (e.OldElement != null || e.NewElement == null)
				return;
            
			view = (CustomSlider)Element;

			if (!string.IsNullOrEmpty(view.ThumbImage))
			{    // Set Thumb Icon  
				Control.SetThumb(Resources.GetDrawable(view.ThumbImage));
			}

            if(view.ThumbColor != Xamarin.Forms.Color.Default)
            {
				// Set Progress bar Thumb color 
				Control.Thumb.SetColorFilter(view.ThumbColor.ToAndroid(), PorterDuff.Mode.SrcIn);
            }

            if(view.MaxColor != Xamarin.Forms.Color.Default)
            {
				//this is for Maximum Slider line Color  
				Control.ProgressBackgroundTintList = Android.Content.Res.ColorStateList.ValueOf(view.MaxColor.ToAndroid());
				Control.ProgressBackgroundTintMode = PorterDuff.Mode.SrcIn;
            }

			if (view.MinColor != Xamarin.Forms.Color.Default)
            {
				//this is for Minimum Slider line Color 
				Control.ProgressTintList = Android.Content.Res.ColorStateList.ValueOf(view.MinColor.ToAndroid());
				Control.ProgressTintMode = PorterDuff.Mode.SrcIn;
            }
		}

		protected override void OnLayout(bool changed, int l, int t, int r, int b)
		{
			base.OnLayout(changed, l, t, r, b);
			if (Android.OS.Build.VERSION.SdkInt >= Android.OS.BuildVersionCodes.JellyBean)
			{
				if (Control == null)
				{ return; }
				SeekBar ctrl = Control;
				Drawable thumb = ctrl.Thumb;
				int thumbTop = ctrl.Height / 2 - thumb.IntrinsicHeight / 2;
				thumb.SetBounds(thumb.Bounds.Left, thumbTop, thumb.Bounds.Left + thumb.IntrinsicWidth, thumbTop + thumb.IntrinsicHeight);
			}
		}
	}
}
