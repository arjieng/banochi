﻿using System;
using System.ComponentModel;
using Android.Content;
using banochixamarin;
using banochixamarin.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(CustomScrollView), typeof(CustomScrollViewRenderer))]
namespace banochixamarin.Droid
{
    public class CustomScrollViewRenderer : ScrollViewRenderer
	{
		public CustomScrollViewRenderer(Context context) : base(context)
		{

		}

		CustomScrollView customScrollView { get; set; }

		protected override void OnElementChanged(VisualElementChangedEventArgs e)
		{
			base.OnElementChanged(e);

			this.VerticalScrollBarEnabled = false;
			this.HorizontalScrollBarEnabled = false;

			if (e.OldElement != null || this.Element == null)
				return;

			if (e.OldElement != null)
				e.OldElement.PropertyChanged -= OnElementPropertyChanged;

			e.NewElement.PropertyChanged += OnElementPropertyChanged;
			this.customScrollView = (CustomScrollView)this.Element;
		}

		private void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			this.VerticalScrollBarEnabled = false;
			this.HorizontalScrollBarEnabled = false;
			this.SmoothScrollingEnabled = customScrollView.Scroll;
		}
	}
}
