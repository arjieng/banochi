﻿using System;
using Xamarin.Forms.Platform.Android;
using Xamarin.Forms;
using banochixamarin;
using banochixamarin.Droid;
using Android.Content;
using Android.Text;
using Android.Views.InputMethods;
using System.ComponentModel;
using Android.Views;
using Android.Support.V4.Content;
using Android.Util;
using Android.Graphics;

[assembly: ExportRenderer(typeof(CustomEditor), typeof(CustomEditorRenderer))]
namespace banochixamarin.Droid
{
	public class CustomEditorRenderer : EditorRenderer
	{
		public CustomEditorRenderer(Context context) : base(context)
        {

		}

		protected CustomEditor customEditor { get; private set; }
		protected override void OnElementChanged(ElementChangedEventArgs<Editor> e)
		{
			base.OnElementChanged(e);
			if (Control != null || e.NewElement != null)
			{
				this.customEditor = (CustomEditor)this.Element;

				this.Control.SetBackgroundColor(Android.Graphics.Color.Transparent);

				switch (customEditor.EditorType)
				{
					case 0:
						Control.SetPadding(Control.PaddingLeft + 3, (int)25.ScaleHeight(), 0, (int)25.ScaleHeight());
						break;
					case 1:
						Control.SetPadding(Control.PaddingLeft + 3, Control.PaddingTop, 0, 9);
						break;
				}

				Control.SetRawInputType(InputTypes.TextFlagNoSuggestions);

				if (customEditor.BorderRadius != 0)
				{
					Control.SetBackgroundResource(Resource.Drawable.RoundedCorner);
				}

				Element.TextChanged += OnEditor_TextChanged;

				Element.Focused += Element_Focused;
				Element.Unfocused += Element_Unfocused;

				if (e.OldElement != null)
				{
					Element.Focused -= Element_Focused;
					Element.Unfocused -= Element_Unfocused;
				}

				if (e.OldElement == null)
				{

				}

				Control.Hint = ((CustomEditor)Element).Placeholder;
				Control.SetHintTextColor(((CustomEditor)Element).PlaceholderColor.ToAndroid());

				switch (customEditor.VerticalTextAlignment)
				{
					case EditorTextAlignment.Start:
						Control.TextAlignment = Android.Views.TextAlignment.ViewStart;
						break;
					case EditorTextAlignment.Center:
						Control.TextAlignment = Android.Views.TextAlignment.Center;
						break;
					case EditorTextAlignment.End:
						Control.TextAlignment = Android.Views.TextAlignment.ViewEnd;
						break;
				}

				switch (customEditor.HorizontalTextAlignment)
				{
					case EditorTextAlignment.Start:
						Control.Gravity = Android.Views.GravityFlags.Top;
						break;
					case EditorTextAlignment.Center:
						Control.Gravity = Android.Views.GravityFlags.CenterHorizontal;
						break;
					case EditorTextAlignment.End:
						Control.Gravity = Android.Views.GravityFlags.Bottom;
						break;
				}

				if (customEditor.AutoCapitalization == TextAutoCapitalization.Word)
				{
					Control.InputType = InputTypes.TextFlagCapWords;
				}
				else if (customEditor.AutoCapitalization == TextAutoCapitalization.Sentence)
				{
					Control.InputType = InputTypes.TextFlagCapSentences;
				}

				this.Control.SetLineSpacing((float)customEditor.LabelSpacing, 1);

				this.UpdateLayout();
			}
		}

		protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged(sender, e);

			if (e.PropertyName == CustomEditor.PlaceholderProperty.PropertyName)
			{
				var element = this.Element as CustomEditor;
				this.Control.Hint = element.Placeholder;
				this.Control.SetHintTextColor(element.PlaceholderColor.ToAndroid());
			}
		}

		private void OnEditor_TextChanged(object sender, Xamarin.Forms.TextChangedEventArgs e)
		{
			if (customEditor != null)
				if (customEditor.CharacterLimit != -1)
				{
					if (!string.IsNullOrEmpty(e.NewTextValue))
					{
						if (e.NewTextValue.Length <= customEditor.CharacterLimit)
						{
							((CustomEditor)sender).Text = e.NewTextValue;
						}
						else
						{
							((CustomEditor)sender).Text = e.OldTextValue;
						}
					}
				}

			customEditor.InvalidateLayout();
		}

		private void Element_Unfocused(object sender, FocusEventArgs e)
		{
			//AddPlaceholder();
		}

		private void Element_Focused(object sender, FocusEventArgs e)
		{
			//ClearPlaceHolder();
			var imm = (InputMethodManager)Context.GetSystemService(Android.Content.Context.InputMethodService);
			imm.ShowSoftInput(Control, Android.Views.InputMethods.ShowFlags.Implicit);
		}

		void AddPlaceholder()
		{
			if (string.IsNullOrEmpty(Element.Text))
			{
				Element.Text = ((CustomEditor)Element).Placeholder;
			}
		}

		void ClearPlaceHolder()
		{
			if (!string.IsNullOrEmpty(Element.Text))
				if (Element.Text.Equals(((CustomEditor)Element).Placeholder))
				{
					Element.Text = "";
				}
		}
	}
}
