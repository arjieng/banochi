﻿using System;
using System.IO;
using System.Threading.Tasks;
using Android.Content;
using banochixamarin.Droid.DependencyServices;
using banochixamarin.Helpers.ImageHelper;
using Xamarin.Forms;

[assembly: Dependency(typeof(ImagePicker))]

namespace banochixamarin.Droid.DependencyServices
{
    public class ImagePicker:IImagePicker
    {
        public ImagePicker()
        {
        }

        public Task<Stream> GetImageStreamAsync()
        {
            // Define the Intent for getting images
            Intent intent = new Intent();
            intent.SetType("image/*");
            intent.SetAction(Intent.ActionGetContent);

            // Get the MainActivity instance
            MainActivity activity = Forms.Context as MainActivity;

            // Start the picture-picker activity (resumes in MainActivity.cs)
            activity.StartActivityForResult(
                Intent.CreateChooser(intent, "Select Picture"),
                MainActivity.PickImageId);

            // Save the TaskCompletionSource object as a MainActivity property
            activity.PickImageTaskCompletionSource = new TaskCompletionSource<Stream>();

            // Return Task object
            return activity.PickImageTaskCompletionSource.Task;
        }
    }
}
