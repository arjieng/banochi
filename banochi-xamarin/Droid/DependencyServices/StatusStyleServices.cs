﻿using System;
using Android.App;
using Android.Views;
using banochixamarin.Droid;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: Xamarin.Forms.Dependency(typeof(StatusStyleServices))]
namespace banochixamarin.Droid
{
	public class StatusStyleServices : IStatusStyle
	{
		public void StatusStyle(int status)
		{
			var window = ((Activity)Forms.Context).Window;
			window.ClearFlags(WindowManagerFlags.TranslucentStatus);
			window.AddFlags(WindowManagerFlags.DrawsSystemBarBackgrounds);
			switch (status)
			{
				case 0:
					window.SetStatusBarColor((Constants.NAVIGATION_BACKGROUND_BLUE).ToAndroid());
					break;
				case 1:
					window.SetStatusBarColor((Constants.NAVIGATION_BACKGROUND_BLUE).ToAndroid());
					break;
			}
		}
	}
}
