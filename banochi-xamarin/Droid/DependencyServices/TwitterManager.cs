﻿using System;
using System.Collections.Generic;
using banochixamarin.Droid;
using Android.App;
using Newtonsoft.Json.Linq;
using Xamarin.Auth;
using Xamarin.Forms;

[assembly: Xamarin.Forms.Dependency(typeof(TwitterManager))]
namespace banochixamarin.Droid
{
	public class TwitterManager : ITwitter
	{
		public Action<JObject, string> _onLoginComplete;

		public void Login(Action<JObject, string> onLoginComplete)
		{
			_onLoginComplete = onLoginComplete;

			var auth = new OAuth1Authenticator(
				consumerKey: "8ZitkTONJkIXKUWmJK81acURp",
				consumerSecret: "WNeJM5gC12R2sPxanMX79xP4FNHbbZKRf4f4odxmY7yiDtdf5n",
				requestTokenUrl: new Uri("https://api.twitter.com/oauth/request_token"),
				authorizeUrl: new Uri("https://api.twitter.com/oauth/authorize"),
				accessTokenUrl: new Uri("https://api.twitter.com/oauth/access_token"),
				callbackUrl: new Uri("http://mobile.twitter.com/home"));

			var activity = ((Activity)Forms.Context);
			activity.StartActivity(auth.GetUI(activity), null);
			auth.Completed += (sender, e) =>
			{
				if (e.IsAuthenticated)
				{
					IDictionary<string, string> info = new Dictionary<string, string>();
					info.Add("include_email", "true");
					var request = new OAuth1Request("GET", new Uri("https://api.twitter.com/1.1/account/verify_credentials.json"), info, e.Account);
					request.GetResponseAsync().ContinueWith(t =>
					{
						if (t.IsFaulted)
						{
							Device.BeginInvokeOnMainThread(() =>
							{
								_onLoginComplete?.Invoke(null, t.Exception.InnerException.Message);
							});
						}
						else
						{
							Device.BeginInvokeOnMainThread(() =>
							{
								string json = t.Result.GetResponseText();
								_onLoginComplete?.Invoke(JObject.Parse(json), string.Empty);
							});
						}
					});
				}
			};
		}
	}
}
