﻿using System;
using Android.Views;
using Android.Widget;
using banochixamarin.Droid;
using Xamarin.Forms;

[assembly: Dependency(typeof(ToastServices))]
namespace banochixamarin.Droid
{
	public class ToastServices : IToastServices
	{
		public void DisplayToast(string text, ToastPosition position)
		{
			Toast toast = Toast.MakeText(Forms.Context, text, ToastLength.Short);
            switch (position)
            {
                case ToastPosition.Top: toast.SetGravity(GravityFlags.Top, 0, 0); break;
                case ToastPosition.Center: toast.SetGravity(GravityFlags.Center, 0, 0); break;
                case ToastPosition.Bottom: toast.SetGravity(GravityFlags.Bottom, 0, 0); break;
            }
			toast.Show();
		}
	}
}
