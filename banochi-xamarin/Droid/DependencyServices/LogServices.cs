﻿using System;
using banochixamarin.Droid;
using Xamarin.Forms;

[assembly: Dependency(typeof(LogServices))]
namespace banochixamarin.Droid
{
	public class LogServices : ILogServices
	{
		public void Log(string log)
		{
			Console.WriteLine(log);
		}
	}
}
