﻿using System;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using FFImageLoading.Forms.Droid;
using HockeyApp.Android;
using Xamarin.Forms;
using Plugin.Permissions;
using CarouselView.FormsPlugin.Android;
using System.IO;
using System.Threading.Tasks;

namespace banochixamarin.Droid
{
    [Activity(Label = "Banochi", Icon = "@drawable/AppIcon", Theme = "@style/MyTheme", ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation, ScreenOrientation = ScreenOrientation.Portrait)]
    [IntentFilter(new[] { Intent.ActionView }, Categories = new[] { Intent.CategoryBrowsable, Intent.CategoryDefault }, DataScheme = "banochi")]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        public static readonly int PickImageId = 1000;

        public TaskCompletionSource<Stream> PickImageTaskCompletionSource { set; get; }


        protected override void OnCreate(Bundle bundle)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(bundle);

            global::Xamarin.Forms.Forms.Init(this, bundle);

            var density = Resources.DisplayMetrics.Density;
            App.screenWidth = Resources.DisplayMetrics.WidthPixels / density;
            App.screenHeight = (Resources.DisplayMetrics.HeightPixels / density) - 26;
            App.DeviceScale = density;

            CarouselViewRenderer.Init();
            CachedImageRenderer.Init(false);

            LoadApplication(new App());

            Intent outsideIntent = Intent;
            try
            {
                var url = Intent.Data.ToString();
                Object[] arr = url.Split(new char[] { '&' });
                if (url.Contains("reset"))
                {
                    Object[] resetObj = arr[0].ToString().Split(new char[] { '=' });
                    Object[] emailObj = arr[1].ToString().Split(new char[] { '=' });

                    var email = emailObj[1].ToString();
                    var reset = resetObj[1].ToString();

                    if (reset == "1")
                    {
                        MessagingCenter.Send<Object, string>(this, "ResetPassword", email);
                    }
                }
            }
            catch (Exception ex)
            {
                App.Log(ex.StackTrace + ex.Message, "MainActivity");
            }
        }

        protected override void OnStart()
        {
            base.OnStart();
        }

        protected override void OnResume()
        {
            base.OnResume();
            CrashManager.Register(this, "6fe472e225614f689e722f4405409cb2");
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);



            if (requestCode == PickImageId)
            {
                if ((resultCode == Result.Ok) && (data != null))
                {
                    Android.Net.Uri uri = data.Data;
                    Stream stream = ContentResolver.OpenInputStream(uri);

                    // Set the Stream as the completion of the Task
                    PickImageTaskCompletionSource.SetResult(stream);
                }
                else
                {
                    PickImageTaskCompletionSource.SetResult(null);
                }
            }
            else
            {

                var manager = DependencyService.Get<IFacebookManager>();
                if (manager != null)
                {
                    (manager as FacebookManager)._callbackManager.OnActivityResult(requestCode, (int)resultCode, data);
                }
            }
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, Permission[] grantResults)
        {
            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
            PermissionsImplementation.Current.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}
