﻿using System;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Views;

namespace banochixamarin.Droid
{
	[Activity(Label = "Banochi", Icon = "@drawable/AppIcon", MainLauncher = true, NoHistory = true, Theme = "@style/MyTheme.Splash", ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation, WindowSoftInputMode = SoftInput.AdjustResize, ScreenOrientation = ScreenOrientation.Portrait)]

	public class SplashScreen : Activity
	{
		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

			Window.RequestFeature(WindowFeatures.NoTitle);

			var intent = new Intent(this, typeof(MainActivity));

			StartActivity(intent);
			Finish();
		}
	}
}
