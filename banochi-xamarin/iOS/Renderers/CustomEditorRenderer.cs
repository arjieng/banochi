﻿using System;
using System.Diagnostics;
using CoreGraphics;
using Foundation;
using banochixamarin;
using banochixamarin.iOS;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CustomEditor), typeof(CustomEditorRenderer))]
namespace banochixamarin.iOS
{
	public class CustomEditorRenderer : EditorRenderer
	{
		float animatedDistance;
		string prevPlaceholder;

		CustomEditor customEditor { get; set; }
		Color oldColor { get; set; }
		protected override void OnElementChanged(ElementChangedEventArgs<Editor> e)
		{

			base.OnElementChanged(e);
			if (this.Control == null) return;

			if (e.NewElement != null)
			{
				this.customEditor = (CustomEditor)this.Element;
				Control.Layer.CornerRadius = customEditor.BorderRadius;
				oldColor = ((CustomEditor)Element).TextColor;

				if (string.IsNullOrEmpty(((CustomEditor)Element).Text))
				{
					((CustomEditor)Element).Text = ((CustomEditor)Element).Placeholder;
					Control.TextColor = ((CustomEditor)Element).PlaceholderColor.ToUIColor();
					SetPlaceholder();
					prevPlaceholder = ((CustomEditor)Element).Placeholder;
				}

				CustomEditor customEntry = (CustomEditor)Element;

				Control.SpellCheckingType = UITextSpellCheckingType.No;
				Control.AutocorrectionType = UITextAutocorrectionType.No;
				Control.Started += OnEntryDidEdit;
				Control.Ended += OnEntryDidEnd;
				// add text length limit
				((CustomEditor)Element).TextChanged += OnEditor_TextChanged;

				switch (customEditor.VerticalTextAlignment)
				{
					case EditorTextAlignment.Start:
						Control.TextContainerInset = new UIEdgeInsets(Control.TextContainerInset.Top - 10, Control.TextContainerInset.Left, Control.TextContainerInset.Bottom, Control.TextContainerInset.Right);
						break;
					case EditorTextAlignment.Center:
						Control.TextContainerInset = new UIEdgeInsets(Control.TextContainerInset.Top, Control.TextContainerInset.Left, Control.TextContainerInset.Bottom, Control.TextContainerInset.Right);
						break;
					case EditorTextAlignment.End:
						Control.ContentMode = UIViewContentMode.Bottom;
						break;
				}


				if (customEntry.ReturnKey == ReturnKeyButton.Next)
				{
					Control.ReturnKeyType = UIReturnKeyType.Default;
				}
				else if (customEntry.ReturnKey == ReturnKeyButton.Done)
				{
					Control.ReturnKeyType = UIReturnKeyType.Default;
				}

				if (customEntry.AutoCapitalization == TextAutoCapitalization.Word)
				{
					Control.AutocapitalizationType = UITextAutocapitalizationType.Words;
				}
				else if (customEntry.AutoCapitalization == TextAutoCapitalization.Sentence)
				{
					Control.AutocapitalizationType = UITextAutocapitalizationType.Sentences;
				}
				else
				{
					Control.AutocapitalizationType = UITextAutocapitalizationType.None;
				}

				SetEditorSettings();

				//remove done button upper part of keyboard
				Control.InputAccessoryView = null;

				//disable scrollable
				Control.ScrollEnabled = customEditor.Scroll;

				if (customEntry.ReturnKey != ReturnKeyButton.Return)
				{
					var toolbar = new UIToolbar(new System.Drawing.RectangleF(0.0f, 0.0f, (float)Control.Frame.Size.Width, 44.0f));

					toolbar.Items = new[]
					{
						new UIBarButtonItem(UIBarButtonSystemItem.FlexibleSpace),
						new UIBarButtonItem(customEntry.ReturnKey.ToString(), UIBarButtonItemStyle.Bordered, delegate
						{
							if(customEntry.ReturnKey == ReturnKeyButton.Next)
							{
								if(customEntry.NextEntry != null)
								{
									customEntry.NextEntry.Focus();
								}
							}
							else if(customEntry.ReturnKey == ReturnKeyButton.Done)
							{
								Control.ResignFirstResponder();
							}
						})
					};

					Control.InputAccessoryView = toolbar;
				}
			}
		}

		protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged(sender, e);
			var element = Element as CustomEditor;
			{
				if (string.IsNullOrEmpty(element.Text) && element.IsFocused == false)
				{
					if (e.PropertyName == CustomEditor.PlaceholderProperty.PropertyName)
					{
						if (!element.Placeholder.Equals(prevPlaceholder))
						{
							SetPlaceholder();
						}
					}
					if (e.PropertyName == CustomEditor.TextProperty.PropertyName)
					{
						AddPlaceholder();
					}
				}
			}
		}

		void SetEditorHeight()
		{
			var textHeight = Control.Font.PointSize + customEditor.LabelSpacing;
		}

		void SetEditorSettings()
		{
			var paragraphStyle = new NSMutableParagraphStyle()
			{
				LineSpacing = (float)customEditor.LabelSpacing
			};

			if (((CustomEditor)this.Element) == null) { return; }

			if (((CustomEditor)this.Element).Text != null)
			{
				var labelString = new NSMutableAttributedString(((CustomEditor)this.Element).Text);
				labelString.AddAttribute(UIStringAttributeKey.ParagraphStyle, paragraphStyle, new NSRange(0, labelString.Length));
				this.Control.AttributedText = labelString;
			}

			Control.TextColor = ((CustomEditor)this.Element).TextColor.ToUIColor();

			Control.Font = UIFont.SystemFontOfSize((float)((CustomEditor)this.Element).FontSize);

			UITextField text = new UITextField();
			text.TextRect(Bounds);

			switch (customEditor.HorizontalTextAlignment)
			{
				case EditorTextAlignment.Start:
					Control.TextAlignment = UITextAlignment.Natural;
					break;
				case EditorTextAlignment.Center:
					Control.TextAlignment = UITextAlignment.Center;
					break;
				case EditorTextAlignment.End:
					Control.TextAlignment = UITextAlignment.Right;
					break;
			}

			SetEditorHeight();
		}

		private void OnEditor_TextChanged(object sender, TextChangedEventArgs e)
		{
			if (!string.IsNullOrEmpty(e.NewTextValue))
			{
				if (customEditor.CharacterLimit != -1)
				{
					if (e.NewTextValue.Length <= customEditor.CharacterLimit)
					{
						((CustomEditor)sender).Text = e.NewTextValue;
					}
					else
					{
						((CustomEditor)sender).Text = e.OldTextValue;
					}
				}
			}

			SetEditorSettings();
			if (((CustomEditor)this.Element) == null) { return; }
			if (customEditor.Text.Equals(((CustomEditor)Element).Placeholder))
			{
				Control.TextColor = ((CustomEditor)Element).PlaceholderColor.ToUIColor();
				SetPlaceholder();
			}
			customEditor.InvalidateLayout();
		}

		private void OnEntryDidEnd(object sender, EventArgs e)
		{
			AddPlaceholder();

			if (customEditor.MoveUp)
			{
				UIView parentView = getParentView();
				var viewFrame = parentView.Bounds;

				if (customEditor.ViewToSwiftUp == null)
					viewFrame.Y = 0.0f;
				else
					viewFrame.Height += animatedDistance;

				UIView.BeginAnimations(null, (IntPtr)null);
				UIView.SetAnimationBeginsFromCurrentState(true);
				UIView.SetAnimationDuration(0.3);

				parentView.Frame = viewFrame;

				UIView.CommitAnimations();
			}
		}

		void AddPlaceholder()
		{
			if (((CustomEditor)Element).Text.Length == 0)
			{
				SetPlaceholder();
			}
		}

		void SetPlaceholder()
		{
			SetEditorSettings();
			((CustomEditor)Element).Text = ((CustomEditor)Element).Placeholder;
			((CustomEditor)Element).TextColor = customEditor.PlaceholderColor;
		}
		void ClearPlaceHolder()
		{
			if (((CustomEditor)Element).Text.Equals(((CustomEditor)Element).Placeholder))
			{
				((CustomEditor)Element).Text = "";
				SetEditorSettings();
				((CustomEditor)Element).TextColor = oldColor;
			}

		}

		private void OnEntryDidEdit(object sender, EventArgs e)
		{
			ClearPlaceHolder();

			if (customEditor.MoveUp)
			{
				UIView parentWindow = getParentView();
				var textfieldRect = parentWindow.ConvertRectFromView(Control.Bounds, Control);
				var viewRect = parentWindow.ConvertRectFromView(parentWindow.Bounds, parentWindow);

				float midline = (float)(textfieldRect.Y + 0.5 * textfieldRect.Height);
				float numerator = (float)(midline - viewRect.Y - 0.2 * viewRect.Height);
				float denominator = (float)((1.0f - 0.2f) * viewRect.Height);
				float heightFraction = numerator / denominator;

				if (heightFraction < 0.0)
				{
					heightFraction = 0.0f;
				}
				else if (heightFraction > 1.0)
				{
					heightFraction = 1.0f;
				}

				UIInterfaceOrientation orientation = UIApplication.SharedApplication.StatusBarOrientation;
				if (orientation == UIInterfaceOrientation.Portrait || orientation == UIInterfaceOrientation.PortraitUpsideDown)
				{
					if (customEditor.ReturnKey != ReturnKeyButton.Return)
					{
						if (customEditor.ViewToSwiftUp == null)
							animatedDistance = (float)Math.Floor((227.0f + 44.0f) * heightFraction);
						else
							animatedDistance = (float)Math.Floor(215.0f + 44.0f);
					}
					else
					{
						if (customEditor.ViewToSwiftUp == null)
							animatedDistance = (float)Math.Floor(227.0f * heightFraction);
						else
							animatedDistance = (float)Math.Floor(215.0f);
					}
				}
				else
				{
					if (customEditor.ReturnKey != ReturnKeyButton.Return)
					{
						if (customEditor.ViewToSwiftUp == null)
							animatedDistance = (float)Math.Floor((162.0f + 44.0f) * heightFraction);
						else
							animatedDistance = (float)Math.Floor(162.0f + 44.0f);
					}
					else
					{
						if (customEditor.ViewToSwiftUp == null)
							animatedDistance = (float)Math.Floor(162.0f * heightFraction);
						else
							animatedDistance = (float)Math.Floor(162.0f);
					}
				}

				var viewFrame = parentWindow.Frame;
				if (customEditor.ViewToSwiftUp == null)
					viewFrame.Y -= animatedDistance;
				else
					viewFrame.Height -= animatedDistance;

				UIView.BeginAnimations(null, (IntPtr)null);
				UIView.SetAnimationBeginsFromCurrentState(true);
				UIView.SetAnimationDuration(0.3);
				parentWindow.Frame = viewFrame;
				UIView.CommitAnimations();
			}
		}

		UIView getParentView()
		{
			UIView view = Control.Superview;

			while (view != null && !(view is UIWindow))
			{
				view = view.Superview;
			}
			return view;
		}
	}
}
