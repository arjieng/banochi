﻿using System;
using banochixamarin;
using banochixamarin.iOS;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(ListViewNoScroll), typeof(ListViewNoScrollRenderer))]
namespace banochixamarin.iOS
{
	public class ListViewNoScrollRenderer : ListViewRenderer
	{
		ListViewNoScroll listViewNoScroll { get; set; }
		protected override void OnElementChanged(ElementChangedEventArgs<ListView> e)
		{
			base.OnElementChanged(e);

			Control.ShowsVerticalScrollIndicator = false;
			Control.ShowsHorizontalScrollIndicator = false;
			Control.Bounces = true;
			Control.SeparatorStyle = UITableViewCellSeparatorStyle.None;

			if (e.NewElement != null)
			{
				this.listViewNoScroll = (ListViewNoScroll)this.Element;
			}
			Control.ScrollEnabled = listViewNoScroll.Scroll;
		}

		protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged(sender, e);
			if (Element == null)
				return;
			if (e.PropertyName == ListViewNoScroll.ScrollProperty.PropertyName)
			{
				Control.ScrollEnabled = listViewNoScroll.Scroll;
			}
		}
	}
}
