﻿using System;
using banochixamarin.iOS;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(ViewCell), typeof(CustomViewCellRenderer))]
namespace banochixamarin.iOS
{
	public class CustomViewCellRenderer : ViewCellRenderer
	{
		public CustomViewCellRenderer()
		{
		}

		public override UITableViewCell GetCell(Cell item, UITableViewCell reusableCell, UITableView tv)
		{
			var cell = base.GetCell(item, reusableCell, tv);
			cell.SelectionStyle = UITableViewCellSelectionStyle.None; //remove selection style
			//cell.SelectedBackgroundView = new UIView {BackgroundColor = UIColor.Clear}; //change color of selection
			return cell;
		}
	}
}
