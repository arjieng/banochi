﻿using System;
using banochixamarin;
using banochixamarin.iOS;
using CoreGraphics;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CustomSlider), typeof(CustomSliderRenderer))]
namespace banochixamarin.iOS
{
	public class CustomSliderRenderer : SliderRenderer
	{
		CustomSlider view;
        UITapGestureRecognizer tap;

		protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.Slider> e)
		{
			base.OnElementChanged(e);
			//if (e.OldElement != null || e.NewElement == null)
				//return;

			if (e.OldElement != null)
			{
				Control.RemoveGestureRecognizer(tap);
			}

            if (e.NewElement != null)
            {
                view = (CustomSlider)Element;
                if (!string.IsNullOrEmpty(view.ThumbImage))
                {
                    //Assigns a thumb image to the specified control states.  
                    Control.SetThumbImage(UIImage.FromBundle(view.ThumbImage), UIControlState.Normal);
                }

                if (view.ThumbColor != Xamarin.Forms.Color.Default)
                {
                    // Set Progress bar Thumb color  
                    Control.ThumbTintColor = view.ThumbColor.ToUIColor();
                }

                if (view.MaxColor != Xamarin.Forms.Color.Default)
                {
                    //this is for Maximum Slider line Color  
                    Control.MaximumTrackTintColor = view.MaxColor.ToUIColor();
                }

                if (view.MinColor != Xamarin.Forms.Color.Default)
                {
                    //this is for Minimum Slider line Color  
                    Control.MinimumTrackTintColor = view.MinColor.ToUIColor();
                }

				tap = new UITapGestureRecognizer((obj) =>
				{
					e.NewElement.Value = (obj.LocationInView(Control).X) * e.NewElement.Maximum / Control.Frame.Size.Width;
				});
				Control.AddGestureRecognizer(tap);
            }
		}
	}
}
