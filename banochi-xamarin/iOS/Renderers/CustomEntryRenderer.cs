﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using UIKit;
using banochixamarin.iOS;
using banochixamarin;
using Foundation;

[assembly: ExportRenderer(typeof(CustomEntry), typeof(CustomEntryRenderer))]
namespace banochixamarin.iOS
{
	public class CustomEntryRenderer : EntryRenderer
	{
		float animatedDistance;
		CustomEntry customEntry;

		protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.Entry> e)
		{
			base.OnElementChanged(e);

			if (this.Control == null) return;

			if (e.NewElement != null)
			{
				customEntry = (CustomEntry)Element;

				Control.BorderStyle = UITextBorderStyle.None;
				Control.SpellCheckingType = UITextSpellCheckingType.No;
				Control.AutocorrectionType = UITextAutocorrectionType.No;

				//Control.TextAlignment = UITextAlignment.Right;

				Control.EditingDidBegin += OnEntryDidEdit;
				Control.EditingDidEnd += OnEntryDidEnd;

				switch (customEntry.TextAlignment)
				{
					case 0: // left
						Control.TextAlignment = UITextAlignment.Left;
						break;
					case 1: // right    
						Control.TextAlignment = UITextAlignment.Right;
						break;
					case 2: // center    
						Control.TextAlignment = UITextAlignment.Center;
						break;
					case 3: // justified    
						Control.TextAlignment = UITextAlignment.Justified;
						break;
					default: // justified    
						Control.TextAlignment = UITextAlignment.Natural;
						break;
				}

				if (customEntry.ReturnKey == ReturnKeyButton.Next)
				{
					Control.ReturnKeyType = UIReturnKeyType.Next;
				}
				else if (customEntry.ReturnKey == ReturnKeyButton.Done)
				{
					Control.ReturnKeyType = UIReturnKeyType.Done;
				}

				if (customEntry.AutoCapitalization == TextAutoCapitalization.Word)
				{
					Control.AutocapitalizationType = UITextAutocapitalizationType.Words;
				}
				else if (customEntry.AutoCapitalization == TextAutoCapitalization.Sentence)
				{
					Control.AutocapitalizationType = UITextAutocapitalizationType.Sentences;
				}
				else
				{
					Control.AutocapitalizationType = UITextAutocapitalizationType.None;
				}

				if (customEntry.ButtonMode)
				{
					Control.ClearButtonMode = UITextFieldViewMode.WhileEditing;
				}

				if (customEntry.Keyboard == Keyboard.Numeric || customEntry.Keyboard == Keyboard.Telephone)
				{
					var toolbar = new UIToolbar(new System.Drawing.RectangleF(0.0f, 0.0f, (float)Control.Frame.Size.Width, 44.0f));

					toolbar.Items = new[]
					{
						new UIBarButtonItem(UIBarButtonSystemItem.FlexibleSpace),
						new UIBarButtonItem(customEntry.ReturnKey.ToString(), UIBarButtonItemStyle.Bordered, delegate
						{
							if(customEntry.ReturnKey == ReturnKeyButton.Next)
							{
								if(customEntry.NextEntry != null)
								{
									customEntry.NextEntry.Focus();
								}
							}
							else if(customEntry.ReturnKey == ReturnKeyButton.Done)
							{
								Control.ResignFirstResponder();
							}
						})
					};

					Control.InputAccessoryView = toolbar;
				}
			}
		}

		private void OnEntryDidEnd(object sender, EventArgs e)
		{
			if (customEntry.MoveUp)
			{
				UIView parentView = getParentView();
				var viewFrame = parentView.Bounds;

				viewFrame.Y = 0.0f;

				UIView.BeginAnimations(null, (IntPtr)null);
				UIView.SetAnimationBeginsFromCurrentState(true);
				UIView.SetAnimationDuration(0.3);

				parentView.Frame = viewFrame;

				UIView.CommitAnimations();
			}
		}

		private void OnEntryDidEdit(object sender, EventArgs e)
		{
			if (customEntry.MoveUp)
			{
				UIView parentWindow = getParentView();
				var textfieldRect = parentWindow.ConvertRectFromView(Control.Bounds, Control);
				var viewRect = parentWindow.ConvertRectFromView(parentWindow.Bounds, parentWindow);

				float midline = (float)(textfieldRect.Y + 0.5 * textfieldRect.Height);
				float numerator = (float)(midline - viewRect.Y - 0.2 * viewRect.Height);
				float denominator = (float)((1.0f - 0.2f) * viewRect.Height);
				float heightFraction = numerator / denominator;

				if (heightFraction < 0.0)
				{
					heightFraction = 0.0f;
				}
				else if (heightFraction > 1.0)
				{
					heightFraction = 1.0f;
				}

				UIInterfaceOrientation orientation = UIApplication.SharedApplication.StatusBarOrientation;
				if (orientation == UIInterfaceOrientation.Portrait || orientation == UIInterfaceOrientation.PortraitUpsideDown)
				{
					if (customEntry.Keyboard == Keyboard.Numeric || customEntry.Keyboard == Keyboard.Telephone)
					{
						animatedDistance = (float)Math.Floor((216.0f + 44.0f) * heightFraction);
					}
					else
					{
						animatedDistance = (float)Math.Floor(216.0f * heightFraction);
					}
				}
				else
				{
					if (customEntry.Keyboard == Keyboard.Numeric || customEntry.Keyboard == Keyboard.Telephone)
					{
						animatedDistance = (float)Math.Floor((162.0f + 44.0f) * heightFraction);
					}
					else
					{
						animatedDistance = (float)Math.Floor(162.0f * heightFraction);
					}
				}

				var viewFrame = parentWindow.Frame;
				viewFrame.Y -= animatedDistance;

				UIView.BeginAnimations(null, (IntPtr)null);
				UIView.SetAnimationBeginsFromCurrentState(true);
				UIView.SetAnimationDuration(0.3);

				parentWindow.Frame = viewFrame;

				UIView.CommitAnimations();
			}
		}

		UIView getParentView()
		{
			UIView view = Control.Superview;

			while (view != null && !(view is UIWindow))
			{
				view = view.Superview;
			}

			return view;
		}
	}
}
