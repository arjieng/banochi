﻿using System;
using CoreGraphics;
using banochixamarin;
using banochixamarin.iOS;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CustomProgressBar), typeof(CustomProgressBarRenderer))]
namespace banochixamarin.iOS
{
	public class CustomProgressBarRenderer : ProgressBarRenderer
	{
        CustomProgressBar view;

		protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.ProgressBar> e)
		{
			base.OnElementChanged(e);

			if (e.OldElement != null || e.NewElement == null)
				return;

            view = (CustomProgressBar)Element;
			if (view.MaxColor != Xamarin.Forms.Color.Default)
			{
				//this is for Maximum Progress line Color 
				Control.TrackTintColor = view.MaxColor.ToUIColor();
			}

			if (view.MinColor != Xamarin.Forms.Color.Default)
			{
				//this is for Minimum Progress line Color
				Control.ProgressTintColor = view.MinColor.ToUIColor();
			}
		}

		public override void LayoutSubviews()
		{
			base.LayoutSubviews();

			var X = 1.0f;
			var Y = 7.0f;

			CGAffineTransform transform = CGAffineTransform.MakeScale(X, Y);
			//this.Control.Transform = transform;
            this.Transform = transform;

			this.ClipsToBounds = true;
			this.Layer.MasksToBounds = true;
			this.Layer.CornerRadius = 0; // For rounded corners - set this to half of progress bars height.
		}
	}
}
