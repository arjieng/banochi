﻿using System;
using banochixamarin;
using banochixamarin.iOS;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(GestureStackLayout), typeof(GestureStackLayoutRenderer))]
namespace banochixamarin.iOS
{
	public class GestureStackLayoutRenderer : VisualElementRenderer<StackLayout>
	{
		static StackLayout lastElement;
		GestureStackLayout gesture;
        bool hasGesture = false;

		protected override void OnElementChanged(ElementChangedEventArgs<StackLayout> e)
		{
			base.OnElementChanged(e);
			gesture = e.NewElement as GestureStackLayout;

			var tapped = new UITapGestureRecognizer((UITapGestureRecognizer obj) =>
			{
                switch (gesture.CollectionType)
                {
                    case 0:
						var decisionOption = gesture.BindingContext as DecisionOption;
						gesture.OnTappedEvent(decisionOption);
                        break;
                    case 1:
                    case 2:
						var userDecision = gesture.BindingContext as UserDecision;
						gesture.OnTappedEvent(userDecision); 
                        break;
                }
			})
			{ NumberOfTapsRequired = 1 };

			var swipeLeft = new UISwipeGestureRecognizer((UISwipeGestureRecognizer obj) =>
			{
				SwipeRightAnimation(e.NewElement);
				e.NewElement.TranslateTo(-((e.NewElement.Parent as AbsoluteLayout).Children[0].Width + 0), e.NewElement.Y, 100, Easing.CubicIn);
			})
			{ Direction = UISwipeGestureRecognizerDirection.Left };

			var swipeRight = new UISwipeGestureRecognizer((UISwipeGestureRecognizer obj) =>
			{
				SwipeRightAnimation(e.NewElement);
			})
			{ Direction = UISwipeGestureRecognizerDirection.Right };

			if (e.NewElement == null)
			{
                if (hasGesture)
                {
                    RemoveGestureRecognizer(swipeLeft);
                    RemoveGestureRecognizer(swipeRight);
                    RemoveGestureRecognizer(tapped);
                }
			}
			else
			{
                if (gesture.IsSwipeable)
                {
                    hasGesture = true;
                    AddGestureRecognizer(swipeLeft);
                    AddGestureRecognizer(swipeRight);
                    AddGestureRecognizer(tapped);
                }
			}
		}

		void SwipeRightAnimation(StackLayout newStackLayout)
		{
			if (lastElement != null)
			{
				lastElement.TranslateTo(0, lastElement.Y, 100, Easing.CubicIn);
			}
			lastElement = newStackLayout;
		}
	}
}
