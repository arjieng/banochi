﻿using System;
using banochixamarin;
using banochixamarin.iOS;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CircularBoxView), typeof(CircularBoxViewRenderer))]
namespace banochixamarin.iOS
{
	public class CircularBoxViewRenderer : BoxRenderer
	{
		protected override void OnElementChanged(ElementChangedEventArgs<BoxView> e)
		{
			base.OnElementChanged(e);

			if (Element == null)
				return;

			Layer.MasksToBounds = true;
			Layer.CornerRadius = (float)((CircularBoxView)this.Element).CornerRadius;
		}
	}
}
