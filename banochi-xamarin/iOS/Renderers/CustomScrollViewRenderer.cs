﻿using System;
using System.ComponentModel;
using banochixamarin;
using banochixamarin.iOS;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CustomScrollView), typeof(CustomScrollViewRenderer))]
namespace banochixamarin.iOS
{
	public class CustomScrollViewRenderer : ScrollViewRenderer
	{
		CustomScrollView customScrollView { get; set; }

		protected override void OnElementChanged(VisualElementChangedEventArgs e)
		{
			base.OnElementChanged(e);

			this.ShowsVerticalScrollIndicator = false;
			this.ShowsHorizontalScrollIndicator = false;
			this.Bounces = false;

			if (e.NewElement != null)
			{
				this.customScrollView = (CustomScrollView)this.Element;
				((CustomScrollView)e.NewElement).PropertyChanged += OnPropertyChanged;
				this.ScrollEnabled = customScrollView.Scroll;
			}
		}

		void OnPropertyChanged(object sender, PropertyChangedEventArgs propertyChangedEventArgs)
		{
			if (propertyChangedEventArgs.PropertyName == CustomScrollView.ScrollProperty.PropertyName)
			{
				this.ScrollEnabled = customScrollView.Scroll;
			}
		}
	}
}
