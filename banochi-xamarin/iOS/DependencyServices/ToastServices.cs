﻿using System;
using banochixamarin.iOS;
using ToastIOS;
using Xamarin.Forms;

[assembly: Dependency(typeof(ToastServices))]
namespace banochixamarin.iOS
{
	public class ToastServices : IToastServices
	{
		public void DisplayToast(string text, ToastPosition position)
		{
			switch (position)
			{
				case ToastPosition.Top: Toast.MakeText(text, Toast.LENGTH_SHORT).SetFontSize(12).SetCornerRadius(8).SetGravity(ToastGravity.Top).Show(); break;
				case ToastPosition.Center: Toast.MakeText(text, Toast.LENGTH_SHORT).SetFontSize(12).SetCornerRadius(8).SetGravity(ToastGravity.Center).Show(); break;
				case ToastPosition.Bottom: Toast.MakeText(text, Toast.LENGTH_SHORT).SetFontSize(12).SetCornerRadius(8).SetGravity(ToastGravity.Bottom).Show(); break;
			}
		}
	}
}
