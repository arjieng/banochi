﻿using System;
using banochixamarin.iOS;
using Xamarin.Forms;

[assembly: Dependency(typeof(LogServices))]
namespace banochixamarin.iOS
{
	public class LogServices : ILogServices
	{
		public void Log(string log)
		{
			Console.WriteLine(log);
		}
	}
}
