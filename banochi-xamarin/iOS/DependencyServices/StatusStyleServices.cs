﻿using System;
using banochixamarin.iOS;
using UIKit;

[assembly: Xamarin.Forms.Dependency(typeof(StatusStyleServices))]
namespace banochixamarin.iOS
{
	public class StatusStyleServices : IStatusStyle
	{
		public void StatusStyle(int status)
		{
			switch (status)
			{
				case 0:
					(UIApplication.SharedApplication).SetStatusBarStyle(UIStatusBarStyle.LightContent, false);
					break;
				case 1:
					(UIApplication.SharedApplication).SetStatusBarStyle(UIStatusBarStyle.Default, false);
					break;
			}
		}
	}
}
