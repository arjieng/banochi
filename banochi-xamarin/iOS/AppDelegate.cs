﻿using System;
using System.Collections.Generic;
using System.Linq;
using CarouselView.FormsPlugin.iOS;
using Facebook.CoreKit;
using FFImageLoading.Forms.Touch;
using Foundation;
using HockeyApp.iOS;
using UIKit;
using Xamarin.Forms;

namespace banochixamarin.iOS
{
    [Register("AppDelegate")]
    public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
    {
        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
			App.screenWidth = (float)UIScreen.MainScreen.Bounds.Width;
			App.screenHeight = (float)UIScreen.MainScreen.Bounds.Height;
			App.DeviceScale = (float)UIScreen.MainScreen.Scale;

            global::Xamarin.Forms.Forms.Init();

            CarouselViewRenderer.Init();
            CachedImageRenderer.Init();

////Code for starting up the Xamarin Test Cloud Agent
//#if DEBUG
//			Xamarin.Calabash.Start();
//#endif
			Facebook.CoreKit.Settings.AppID = "1772729283029100";
			Facebook.CoreKit.Settings.DisplayName = "Banochi";

            Plugin.Share.ShareImplementation.ExcludedUIActivityTypes.Clear();

            LoadApplication(new App());

			var manager = BITHockeyManager.SharedHockeyManager;
			manager.Configure("6fe472e225614f689e722f4405409cb2");
			manager.StartManager();

            UIApplication.SharedApplication.StatusBarStyle = UIStatusBarStyle.LightContent;

            return base.FinishedLaunching(app, options);
        }

		public override bool OpenUrl(UIApplication app, NSUrl url, NSDictionary options)
		{
            //facebook
            if (url.ToString().Contains("fb1772729283029100"))
            {
                return ApplicationDelegate.SharedInstance.OpenUrl(app, url, options.ValueForKey(new NSString("UIApplicationOpenURLOptionsSourceApplicationKey"))?.ToString(), options);
            }

            //reset password
			//Object[] arr = url.ToString().Split(new char[] { '=' });
			//if (url.ToString().Contains("reset"))
			//{
			//	Object[] getCode = arr[1].ToString().Split(new char[] { '&' });
			//	var email = getCode[0].ToString();
			//	var reset = arr[2].ToString();

			//	if (reset == "1")
			//	{
			//		MessagingCenter.Send<Object, string>(this, "ResetPassword", email);
			//	}
			//}


            Object[] arr = url.ToString().Split(new char[] { '&' });
            if (url.ToString().Contains("reset"))
            {
                Object[] resetObj = arr[0].ToString().Split(new char[] { '=' });
                Object[] emailObj = arr[1].ToString().Split(new char[] { '=' });

                var email = emailObj[1].ToString();
                var reset = resetObj[1].ToString();

                if (reset == "1")
                {
                    MessagingCenter.Send<Object, string>(this, "ResetPassword", email);
                }
            }

			return false;
		}

		public override void OnActivated(UIApplication uiApplication)
		{
			base.OnActivated(uiApplication);

			AppEvents.ActivateApp();
		}
    }
}
