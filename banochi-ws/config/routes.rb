Rails.application.routes.draw do
  # devise_for :users

  devise_for :users, path: 'users', controllers: {
    passwords: "api/v1/passwords"}




  root 'api/v1/decisions#root_page'
  namespace :api, path: '' do
    namespace :v1 do
      
      devise_scope :user do
        post 'register'                                 => 'registrations#create'
        post 'social-signup'                            => 'registrations#social_signup'
        post 'sign-in'                                  => 'sessions#create'
        post 'social-signin'                            => 'sessions#social_signin'
        post 'forgot-password'                          => 'sessions#forgot_password'
        post 'image-upload'                             => 'sessions#upload_image'
      end

      post 'add-option'                                 => 'options#create'
      post 'delete-option'                              => 'options#delete'
      post 'update-option'                              => 'options#update'
      post 'reload-option'                              => 'options#index'

      get 'asd'                                         => 'decisions#asd'
      post 'add-decisions'                              => 'decisions#create'
      post 'update-decision'                            => 'decisions#update'
      post 'delete-decision'                            => 'decisions#delete'
      post 'reload-decision'                            => 'decisions#index'
      post 'change-status'                              => 'decisions#change_status'
      post 'send-graph'                                  => 'decisions#send_decision_graph'
    end
  end
end
