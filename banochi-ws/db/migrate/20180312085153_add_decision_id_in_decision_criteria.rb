class AddDecisionIdInDecisionCriteria < ActiveRecord::Migration
  def up
  	drop_table :decision_criteria
  	create_table :decision_criteria do |t|
      t.belongs_to :decision
      t.string :title
      t.float :criteria_value
      t.string :image
      t.timestamps null: false
    end
  end
  
  def down
  	drop_table :decision_criteria
  	create_table :decision_criteria do |t|
      t.string :title
      t.float :criteria_value
      t.string :image
      t.timestamps null: false
    end
  end
end

	  