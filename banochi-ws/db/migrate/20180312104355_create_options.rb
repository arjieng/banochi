class CreateOptions < ActiveRecord::Migration
  def change
    create_table :options do |t|
      t.belongs_to :decision
      t.string :option_name
      t.string :mls_id
      t.string :address
      t.timestamps null: false
    end
  end
end
