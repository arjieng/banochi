class CreateDecisionCriteria < ActiveRecord::Migration
  def change
    create_table :decision_criteria do |t|
      t.string :title
      t.float :criteria_value
      t.string :image
      t.timestamps null: false
    end
  end
end
