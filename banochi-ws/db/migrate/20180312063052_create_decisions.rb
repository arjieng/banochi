class CreateDecisions < ActiveRecord::Migration
  def change
    create_table :decisions do |t|
      t.belongs_to :user
      t.string :home_type
      t.string :zipcode
      t.boolean :is_saved
      t.timestamps null: false
    end
  end
end
