class AddCriteriaIdInWeightAndDecisionCriterium < ActiveRecord::Migration
  def up
  	add_column :decision_criteria, :criteria_type_id, :int
  	add_column :weight_criteria, :criteria_type_id, :int
  end
  def down
  	remove_column :decision_criteria, :criteria_type_id
  	remove_column :weight_criteria, :criteria_type_id
  end
end
