class AddProviderIdInUser < ActiveRecord::Migration
  def up
  	add_column :users, :provider_id, :string
  end
  def down
  	remove_column :users, :provider_id
  end
end