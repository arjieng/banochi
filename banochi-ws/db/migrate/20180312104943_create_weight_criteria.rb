class CreateWeightCriteria < ActiveRecord::Migration
  def change
    create_table :weight_criteria do |t|
      t.belongs_to :option
      t.string :title
      t.float :criteria_value
      t.string :image
      t.timestamps null: false
    end
  end
end
