class GraphSender < ApplicationMailer
  def send_graph(data)
  	@graph = data[:graph]

    mail( :to => data[:email] ) do |format|

    	# format.text{ render graph_sender: 'send_graph' }
    	format.html{ render graph_sender: 'send_graph' }
    end
  end
end
