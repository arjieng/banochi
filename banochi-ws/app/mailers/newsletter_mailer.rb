class NewsletterMailer < ApplicationMailer
default from: "argie.bacomo@appstone.tech"
  def weekly(email)
    mail to: email, subject: "RailsCasts Weekly"
  end
end
