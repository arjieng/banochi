class UserUploader < CarrierWave::Uploader::Base
  # include CarrierWave::RMagick
  include CarrierWave::MiniMagick
  storage :file

  process resize_to_limit: [165, nil]
  def store_dir
    Rails.configuration.USER_IMG_UPLOAD_DIRECTORY  + "/#{model.id}"
  end

  def delete!
    remove!
    remove_versions!
  end

  def extension_whitelist
    %w(jpg jpeg png gif bmp tif tiff)
  end

  def filename
    ivar = "@#{mounted_as}_secure_token"
    token = model.instance_variable_get(ivar) or model.instance_variable_set(ivar, SecureRandom.hex(20/2))
    "#{token}.png" if original_filename
  end
end
