class Decision < ActiveRecord::Base
  belongs_to :user
  has_many :decision_criterium
  has_many :options
  self.per_page = 10
end
