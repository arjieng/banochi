class Option < ActiveRecord::Base
	belongs_to :decision
	has_many :weight_criterium
	self.per_page = 10
end
