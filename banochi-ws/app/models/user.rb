class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :recoverable, :rememberable, :trackable, :validatable
  attr_accessor :skip_email_validation
  attr_accessor :skip_password_validation


  has_many :decisions

  after_create :generate_token
  mount_uploader :image, UserUploader
  
  private
    def generate_token
      secret = Digest::SHA1.hexdigest(SecureRandom.urlsafe_base64)
      self.token = User.find_by_token(secret.to_s) ? generate_token : secret
      self.save
    end
    def password_required?
      return false if skip_password_validation
      super
    end
    def email_required?
      return false if skip_email_validation
      super
    end
end
