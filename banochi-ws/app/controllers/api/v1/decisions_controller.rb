class Api::V1::DecisionsController < ApplicationController

  def send_decision_graph
    user = User.find_by_token(params[:token])
    if user.present?
      decision = Decision.includes(:decision_criterium, options: :weight_criterium).find(params[:id])
      if decision.present?
        graph = []
        
        criteria_count = 0
        decision.decision_criterium.each do |decision_criteria|
          criteria_count = criteria_count + 1
          option_count = 1
          decision.options.each do |option|
            option_value = option.weight_criterium.select{ |x| x.criteria_type_id == decision_criteria.criteria_type_id }
            graph.push({id: option_count, option: option[:option_name], product: decision_criteria[:criteria_value].to_f * option_value[0][:criteria_value].to_f})
            option_count = option_count + 1
          end
          
        end
        graph_final = []
        (1..criteria_count).each do |count|
          graph_temp = graph.select{ |x| x[:id] == count }
          value = 0
          graph_temp.each do |temp|
            value = value + temp[:product]
          end
          graph_final.push({ name: graph_temp[0][:option], value: (((value/criteria_count)/100)*10), percentage: (value/criteria_count) })
        end
        data = { email: user.email, graph: graph_final }
        # GraphSender.send_graph(data).deliver
        render json: { graph_sent: true, status: data }, status: 200

      else
        render json: { error: "Decision was not found.", status: 300 }, status: 200
      end
    else
      render json: { error: "You are not authorize to use this service.", status: 300 }, status: 200
    end
  end

  def asd
    user = User.find_by_token(params[:token])
    if user.present?
      @decisions = []
      decisions = user.decisions.includes(:decision_criterium, options: :weight_criterium).where("is_saved = ?", params[:is_saved]).paginate(page: params[:page]).sort_by{ |h| h[:id] }.reverse

      decisions.each do |decision|
        decision_criterium = []
        options = []
        decision.decision_criterium.each do |decision_criteria|
          decision_criterium.push({ id: decision_criteria.criteria_type_id, title: decision_criteria.title, criteria_value: decision_criteria.criteria_value, image: decision_criteria.image, is_selected: 1 })
        end
        decision.options.each do |option|
          weight_criterium = []
          option.weight_criterium.each do |weight_criteria|
            weight_criterium.push({ id: weight_criteria.criteria_type_id, title: weight_criteria.title, criteria_value: weight_criteria.criteria_value, image: weight_criteria.image, is_selected: 1 })
          end
          options.push({ id: option.id, home_name: option.option_name, mls_id: option.mls_id, address: option.address, has_mls: 1, criteriaWeight: weight_criterium })
        end
        @decisions.push({ id: decision.id, home_type: decision.home_type, zip_code: decision.zipcode, date_created: decision.created_at.strftime('%m/%d/%Y'), criteriaSelected: decision_criterium , userOptions: options })
      end
      
      @pagination = { has_more: (@decisions.count < 10 ? 0 : 1), url: api_v1_asd_path+"?page="+(params[:page].to_i+1).to_s+"&is_saved="+params[:is_saved]+"&token="+user.token }
      render json: { decision: @decisions, paginations: @pagination, status: 200 }, status: 200
    else
      render json: { error: "You are not authorize to use this service.", status: 300 }, status: 200
    end
  end

  def create
    user = User.find_by_token(params[:token])
    if user.present?
      decision = user.decisions.create(decision_params)
      if decision.save
        decision_criterium = []
        params[:decision_criteria].each do |f|
          dc = DecisionCriterium.new(decision_id: decision.id, title: f[:title], criteria_value: f[:criteria_value], image: f[:image], criteria_type_id: f[:criteria_type_id])
          dc.save
          decision_criterium.push dc
        end
        @criterium = []
        decision_criterium.each do |criteria|
          @criterium.push({ id: criteria.criteria_type_id, title: criteria.title, criteria_value: criteria.criteria_value, image: criteria.image, is_selected: 1 })
        end

        render json: { decision: { id: decision.id, home_type: decision.home_type, zip_code: decision.zipcode, date_created: decision.created_at.strftime('%m/%d/%Y'), criteriaSelected: @criterium, userOptions: [] }, status: 200 }, status: 200
      else
        render json: { error: "Decision not saved.", status: 300 }, status: 200
      end
    else
      render json: { error: "You are not authorize to use this service.", status: 300 }, status: 200
    end
  end

  def change_status
    user = User.find_by_token(params[:token])
    if user.present?
      decision = Decision.find(params[:id])
      if decision.update(is_saved: params[:is_saved])
        render json: { is_saved: 1, status: 200 }, status: 200
      else
        render json: { is_saved: 0, status: 300 }, status: 200
      end
    else
      render json: { error: "You are not authorize to use this service.", status: 300 }, status: 200
    end


  end

  def index
    user = User.includes(decisions: [:decision_criterium, options: :weight_criterium]).find_by_token(params[:token])
    if user.present?
      @saved = []
      @progress = []
      descending = user.decisions.sort_by{ |h| h[:id] }.reverse
      # render json: { descending: descending, not: user.decisions, status: 200 }, status: 200

      descending.each do |decision|
        decision_criterium = []
        options = []
        decision.decision_criterium.each do |decision_criteria|
          decision_criterium.push({ id: decision_criteria.criteria_type_id, title: decision_criteria.title, criteria_value: decision_criteria.criteria_value, image: decision_criteria.image, is_selected: 1 })
        end
        decision.options.each do |option|
          weight_criterium = []
          option.weight_criterium.each do |weight_criteria|
            weight_criterium.push({ id: weight_criteria.criteria_type_id, title: weight_criteria.title, criteria_value: weight_criteria.criteria_value, image: weight_criteria.image, is_selected: 1 })
          end
          options.push({ id: option.id, home_name: option.option_name, mls_id: option.mls_id, address: option.address, has_mls: 1, criteriaWeight: weight_criterium })
        end

        if decision.is_saved
          @saved.push({ id: decision.id, home_type: decision.home_type, zip_code: decision.zipcode, date_created: decision.created_at.strftime('%m/%d/%Y'), criteriaSelected: decision_criterium , userOptions: options })
        else
          @progress.push({ id: decision.id, home_type: decision.home_type, zip_code: decision.zipcode, date_created: decision.created_at.strftime('%m/%d/%Y'), criteriaSelected: decision_criterium , userOptions: options })
        end
      end
      @user = { id: user.id, email: user.email, name: user.name, image: user.image.url, token: user.token }
      render json: { decisions: { saved: @saved, progress: @progress }, status: 200  }, status: 200
    else
      render json: { error: "You are not authorize to use this service.", status: 300 }, status: 200
    end
  end

  def root_page
    
  end

  def delete
    user = User.find_by_token(params[:token])
    if user.present?
      begin
        decision = Decision.includes(:decision_criterium, options: :weight_criterium).find(params[:id])
        decision.options.each do |option|
          option.weight_criterium.destroy_all
        end
        decision.options.destroy_all
        decision.decision_criterium.destroy_all
        decision.destroy
        render json: { is_destroyed: true, status: 200 }, status: 200
      rescue => ex
        render json: { is_destroyed: false, status: 300 }, status: 200
      end
    else
      render json: { error: "You are not authorize to use this service.", status: 300 }, status: 200
    end
  end

  def update
    user = User.find_by_token(params[:token])
    if user.present?
      decision = Decision.includes(:decision_criterium, options: :weight_criterium).find(params[:id])
      if decision.present?
        decision.update(decision_params)
        decision.decision_criterium.destroy_all
        new_option_decisions = []
        params[:decision_criteria].each do |f|
          DecisionCriterium.new(decision_id: decision.id, title: f[:title], criteria_value: f[:criteria_value], image: f[:image], criteria_type_id: f[:criteria_type_id]).save
          decision.options.each do |option|
            weight_criteria = option.weight_criterium.select{ |wc| wc.criteria_type_id == f[:criteria_type_id] }
            if weight_criteria.present?
              new_option_decisions.push({ option_id: option.id, title: weight_criteria[0][:title], criteria_value: weight_criteria[0][:criteria_value], image: weight_criteria[0][:image], criteria_type_id: weight_criteria[0][:criteria_type_id] })
            else
              new_option_decisions.push({ option_id: option.id, title: f[:title], criteria_value: 1, image: f[:image], criteria_type_id: f[:criteria_type_id] })
            end
          end
        end
        decision.options.each do |option|
          option.weight_criterium.destroy_all
        end
        new_option_decisions.each do |option_decision|
          WeightCriterium.new(option_id: option_decision[:option_id], title: option_decision[:title], criteria_value: option_decision[:criteria_value], image: option_decision[:image], criteria_type_id: option_decision[:criteria_type_id] ).save
        end
        decision = Decision.includes(:decision_criterium, options: :weight_criterium).find(params[:id])
        decision_criterium = []
        options = []
        decision.decision_criterium.each do |decision_criteria|
          decision_criterium.push({ id: decision_criteria.criteria_type_id, title: decision_criteria.title, criteria_value: decision_criteria.criteria_value, image: decision_criteria.image, is_selected: 1 })
        end
        decision.options.each do |option|
          weight_criterium = []
          option.weight_criterium.each do |weight_criteria|
            weight_criterium.push({ id: weight_criteria.criteria_type_id, title: weight_criteria.title, criteria_value: weight_criteria.criteria_value, image: weight_criteria.image, is_selected: 1 })
          end
          options.push({ id: option.id, home_name: option.option_name, mls_id: option.mls_id, address: option.address, has_mls: 1, criteriaWeight: weight_criterium })
        end
        render json: { decision: { id: decision.id, home_type: decision.home_type, zip_code: decision.zipcode, date_created: decision.created_at.strftime('%m/%d/%Y'), criteriaSelected: decision_criterium , userOptions: options }, status: 200 }, status: 200
      else
        render json: { error: "Decision cannot be found.", status: 300 }, status: 200
      end 
    else
      render json: { error: "You are not authorize to use this service.", status: 300 }, status: 200
    end

  end

  private
    def decision_params
      params.require(:decision).permit(:home_type, :zipcode, :is_saved)
    end
end
