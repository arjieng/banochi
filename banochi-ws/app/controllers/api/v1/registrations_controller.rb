class Api::V1::RegistrationsController < Devise::RegistrationsController
	def create
    user = User.find_by(email: user_params[:email])
    if user.present?
      render json: { errors: { email: "Email is already taken.", password: "" }, status: 300 }, status: 200
    else
      build_resource(user_params)
      if resource.save
        sign_in resource

        user = User.find_by_id(resource.id)

        sign_out resource

        render json: { user: { id: user.id, email: user.email, name: user.name, token: user.token }, status: 200 }, status: 200
      else
        render json: { errors: errors(resource.errors.to_a), status: 300 }, status: 200
      end
    end
  end

  def social_signup
    user = User.find_by_provider_id(params[:user][:provider_id])
    if user.present?
      render json: { errors: { email: "You are already registered", password: "" }, status: 300 }, status: 200
    else
      build_resource(social_params)
      resource.skip_password_validation = true
      resource.skip_email_validation = true
      
      if resource.save
        sign_in resource

        user = User.find_by_id(resource.id)
        
        sign_out resource
        
        render json: { user: { id: user.id, email: user.email, name: user.name, token: user.token }, status: 200 }, status: 200
      else
        render json: { errors: errors(resource.errors.to_a), status: 300 }, status: 200
      end
    end
  end

  protected
    def after_update_path_for(resource)
      root_path
    end
  private
    def social_params
      params.require(:user).permit(:provider_id, :name, :image, :email)
    end

    def user_params
      params.require(:user).permit(:email, :password)
    end

    def errors(resource_errors)
      email_errors = ""
      password_errors = ""

      resource_errors.each do |error|
        if email_error(error) == true
          email_errors = error
        elsif password_error(error)
          password_errors = error
        end
      end

      return { email: email_errors, password: password_errors }
    end

    def email_error(log_error)
      errors = ["Email is invalid", "Email has already been taken"]

      return errors.include? log_error
    end

    def password_error(log_error)
      errors = ["Password is too short (minimum is 6 characters)"]

      return errors.include? log_error
    end
end
