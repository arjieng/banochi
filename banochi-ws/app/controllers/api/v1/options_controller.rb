class Api::V1::OptionsController < ApplicationController
  def create
    user = User.find_by_token(params[:token])
    if user.present?
      decision = Decision.find(params[:decision_id])
      if decision.present?
        option = decision.options.create options_params
        if option.save
          weight_criteria = []
          params[:weight_criteria].each do |criteria|
            wc = WeightCriterium.new(option_id: option.id, title: criteria[:title], criteria_value: criteria[:criteria_value], image: criteria[:image], criteria_type_id: criteria[:criteria_type_id] )
            wc.save
            weight_criteria.push wc
          end

          @criterium = []
          weight_criteria.each do |criteria|
            @criterium.push({ id: criteria.criteria_type_id, title: criteria.title, criteria_value: criteria.criteria_value, image: criteria.image, is_selected: 1 })
          end

          render json: { option: { id: option.id, home_name: option.option_name, mls_id: option.mls_id, address: option.address, has_mls: (option.mls_id.nil? ? 0 : 1), criteriaWeight: @criterium }, status: 200 }, status: 200
        else
          render json: { error: "Option was not saved.", status: 300 }, status: 200
        end
      else
        render json: { error: "Decision cannot be found.", status: 300 }, status: 200
      end
    else
      render json: { error: "You are not authorize to use this service.", status: 300 }, status: 200
    end
  end

  def index
    user = User.find_by_token(params[:token])
    if user.present?
      decision = Decision.includes(options: :weight_criterium).find(params[:id])
      if decision.present?
        decision_option = []
        decision.options.each do |option|
          criterium = []
          option.weight_criterium.each do |criteria|
            criterium.push({ id: criteria.criteria_type_id, title: criteria.title, criteria_value: criteria.criteria_value, image: criteria.image, is_selected: 1 })
          end
          decision_option.push({ id: option.id, home_name: option.option_name, mls_id: option.mls_id, address: option.address, has_mls: (option.mls_id.nil? ? 0 : 1), criteriaWeight: criterium })
        end
        render json: { options: decision_option, status: 200 }, status: 200
      else
        render json: { error: "Decision cannot be found.", status: 300 }, status: 200
      end
    else
      render json: { error: "You are not authorize to use this service.", status: 300 }, status: 200
    end
  end


  def delete
    user = User.find_by_token(params[:token])
    if user.present?
      begin
        option = Option.includes(:weight_criterium).find(params[:id])
        option.weight_criterium.destroy_all
        option.destroy
        render json: { is_destroyed: true, status: 200 }, status: 200
      rescue => ex
        render json: { is_destroyed: false, status: 300 }, status: 200
      end
    else
      render json: { error: "You are not authorize to use this service.", status: 300 }, status: 200
    end
  end

  def update
    user = User.find_by_token(params[:token])
    if user.present?
      option = Option.includes(:weight_criterium).find(params[:id])
      if option.present?
        option.update(options_params)
        option.weight_criterium.destroy_all
        params[:weight_criteria].each do |criteria|
          WeightCriterium.new(option_id: option.id, title: criteria[:title], criteria_value: criteria[:criteria_value], image: criteria[:image], criteria_type_id: criteria[:criteria_type_id] ).save
        end

        option = Option.includes(:weight_criterium).find(params[:id])
        @criterium = []
        
        option.weight_criterium.each do |criteria|
          @criterium.push({ id: criteria.criteria_type_id, title: criteria.title, criteria_value: criteria.criteria_value, image: criteria.image, is_selected: 1 })
        end
        render json: { option: { id: option.id, home_name: option.option_name, mls_id: option.mls_id, address: option.address, has_mls: (option.mls_id.nil? ? 0 : 1), criteriaWeight: @criterium }, status: 200 }, status: 200

      else
        render json: { error: "Decision cannot be found.", status: 300 }, status: 200
      end
    else
      render json: { error: "You are not authorize to use this service.", status: 300 }, status: 200
    end
  end

  private
    def options_params
      params.require(:option).permit(:option_name, :mls_id, :address)
    end
end