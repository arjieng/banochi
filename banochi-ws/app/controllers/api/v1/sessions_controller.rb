class Api::V1::SessionsController < Devise::SessionsController
	def create
		user = User.find_by(email: params[:user][:email])
		if user.present?
      resource = warden.authenticate!(scope: resource_name, recall: "#{controller_path}#failure")
      sign_in_and_redirect(resource_name, resource)
		else
			render json: { error: "Your account is not yet registered!", status: 300 }, status: 200
		end
	end

  def upload_image
    user = User.find_by_token(params[:user][:token])
    if user.present?
      user.image = params[:user][:image]
      if user.save!
        render json: { success: user.image.url, status: 200 }, status: 200
      else
        render json: { error: "Image upload failed.", status: 300 }, status: 200
      end
    else
      render json: { error: "You are not authorize to use this service.", status: 300 }, status: 200
    end
  end

  def failure
    return render json: { error: "Error with your login or password!", status: 300 }, status: 200
  end

  def forgot_password
    @user = User.find_by_email(params[:email])
    if @user.present?
      @user.send_reset_password_instructions
      render json: { is_sent: true, status: 200 }, status: 200
    else
      render json: { error: "Email not yet registered.", status: 300 }, status: 200
    end
  end

  def social_signin
    user = User.find_by(provider_id: params[:user][:provider_id])
    if user.present?
      render json: { user: { id: user.id, email: user.email, name: user.name, token: user.token }, status: 200 }, status: 200
    else
      render json: { error: "Error with your login or password!", status: 300 }, status: 200
    end
  end

  private
    def sign_in_and_redirect(resource_or_scope, resource=nil)
      
      scope = Devise::Mapping.find_scope!(resource_or_scope)
      resource ||= resource_or_scope
      sign_in(scope, resource) unless warden.user(scope) == resource

      user = User.find_by_id(current_user.id)
      sign_out resource
      @saved = []
      @progress = []
      @pagination = []
      saved_descending = user.decisions.includes(:decision_criterium, options: :weight_criterium).where("is_saved = true").paginate(page: 1).sort_by{ |h| h[:id] }.reverse
      progress_descending = user.decisions.includes(:decision_criterium, options: :weight_criterium).where("is_saved = false").paginate(page: 1).sort_by{ |h| h[:id] }.reverse

      saved_descending.each do |decision|
        decision_criterium = []
        options = []
        decision.decision_criterium.each do |decision_criteria|
          decision_criterium.push({ id: decision_criteria.criteria_type_id, title: decision_criteria.title, criteria_value: decision_criteria.criteria_value, image: decision_criteria.image, is_selected: 1 })
        end
        decision.options.each do |option|
          weight_criterium = []
          option.weight_criterium.each do |weight_criteria|
            weight_criterium.push({ id: weight_criteria.criteria_type_id, title: weight_criteria.title, criteria_value: weight_criteria.criteria_value, image: weight_criteria.image, is_selected: 1 })
          end
          options.push({ id: option.id, home_name: option.option_name, mls_id: option.mls_id, address: option.address, has_mls: 1, criteriaWeight: weight_criterium })
        end
        @saved.push({ id: decision.id, home_type: decision.home_type, zip_code: decision.zipcode, date_created: decision.created_at.strftime('%m/%d/%Y'), criteriaSelected: decision_criterium , userOptions: options })
      end

      saved_pagination = { has_more: (@saved.count < 10 ? 0 : 1), url: api_v1_asd_path+"?page=2&is_saved=true&token="+user.token }

      progress_descending.each do |decision|
        decision_criterium = []
        options = []
        decision.decision_criterium.each do |decision_criteria|
          decision_criterium.push({ id: decision_criteria.criteria_type_id, title: decision_criteria.title, criteria_value: decision_criteria.criteria_value, image: decision_criteria.image, is_selected: 1 })
        end
        decision.options.each do |option|
          weight_criterium = []
          option.weight_criterium.each do |weight_criteria|
            weight_criterium.push({ id: weight_criteria.criteria_type_id, title: weight_criteria.title, criteria_value: weight_criteria.criteria_value, image: weight_criteria.image, is_selected: 1 })
          end
          options.push({ id: option.id, home_name: option.option_name, mls_id: option.mls_id, address: option.address, has_mls: 1, criteriaWeight: weight_criterium })
        end
        @progress.push({ id: decision.id, home_type: decision.home_type, zip_code: decision.zipcode, date_created: decision.created_at.strftime('%m/%d/%Y'), criteriaSelected: decision_criterium , userOptions: options })
      end

      progress_pagination = { has_more: (@progress.count < 10 ? 0 : 1), url: api_v1_asd_path+"?page=2&is_saved=false&token="+user.token }

      @user = { id: user.id, email: user.email, name: user.name, image: user.image.url, token: user.token }
      
      render json: { user: @user, decisions: { saved: @saved, progress: @progress }, paginations: { saved: saved_pagination, progress: progress_pagination }, status: 200  }, status: 200

    end
    def social_params
      params.require(:user).permit(:provider_id)
    end
    def user_params
      params.require(:user).permit(:email, :password)
    end
end